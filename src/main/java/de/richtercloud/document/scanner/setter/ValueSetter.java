/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.setter;

import de.richtercloud.document.scanner.gui.ocrresult.OCRResult;
import de.richtercloud.reflection.form.builder.ResetException;
import de.richtercloud.reflection.form.builder.TransformationException;
import javax.swing.JComponent;

/**
 * An interface to handle different setter methods of different
 * {@link JComponent} and pass the OCR result to them (always a {@code String})
 * (with {@link #setOCRResult(OCRResult, JComponent) })
 * as well as parsed values from {@link de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionService}s (with
 * {@link #setValue(java.lang.Object, javax.swing.JComponent) }).
 *
 * Implementations are expected to handle parsing of OCR results as well in
 * order to limit the association of field type and a handler component to one
 * which is {@code ValueSetter}.
 *
 * @author richter
 * @param <V> the type of value to set
 * @param <C> the type of the component the value is set on
 */
public interface ValueSetter<V, C extends JComponent> {

    void setOCRResult(OCRResult oCRResult, C comp);

    void setValue(V value, C comp) throws TransformationException,
            NoSuchFieldException,
            ResetException;

    /**
     * Whether or not this setter supports setting of {@link OCRResult} with
     * {@link #setOCRResult(OCRResult, JComponent) }.
     * @return {@code true} if it does support, {@code false} otherwise
     */
    boolean isSupportsOCRResultSetting();
}
