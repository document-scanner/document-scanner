/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import de.richtercloud.document.scanner.ifaces.Constants;
import de.richtercloud.message.handler.ConfirmMessageHandler;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.message.handler.Message;
import de.richtercloud.reflection.form.builder.ClassInfo;
import de.richtercloud.reflection.form.builder.ResetException;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldHandlingException;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryHistoryEntryStorage;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryPanel;
import de.richtercloud.reflection.form.builder.jpa.storage.FieldInitializer;
import de.richtercloud.reflection.form.builder.jpa.storage.PersistenceStorage;
import de.richtercloud.reflection.form.builder.storage.StorageException;
import de.richtercloud.validation.tools.FieldRetriever;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
internal implementation notes:
- There's no sense in providing a reflection form panel for editing since both
the GUI and the code logic only makes sense if there's a OCRSelectPanelPanel
present -> only provide components to query and select to be edited entities and
open them in MainPanel.
*/
/**
 * A dialog to select the class and the concrete entity to edit or delete it.
 * @author richter
 */
public class EntityEditingDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final static Logger LOGGER = LoggerFactory.getLogger(EntityEditingDialog.class);
    private final ListCellRenderer<Object> entityEditingClassComboBoxRenderer = new DefaultListCellRenderer() {
        private static final long serialVersionUID = 1L;
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            String value0;
            if(value == null) {
                value0 = null;
            }else {
                //might be null during initialization
                Class<?> valueCast = (Class<?>) value;
                ClassInfo classInfo = valueCast.getAnnotation(ClassInfo.class);
                if(classInfo != null) {
                    value0 = classInfo.name();
                }else {
                    value0 = valueCast.getSimpleName();
                }
            }
            return super.getListCellRendererComponent(list, value0, index, isSelected, cellHasFocus);
        }
    };
    private final DefaultComboBoxModel<Class<?>> entityEditingClassComboBoxModel = new DefaultComboBoxModel<>();
    private final PersistenceStorage storage;
    private final FieldRetriever fieldRetriever;
    private QueryPanel<Object> entityEditingQueryPanel;
    /**
     * A cache to keep custom queries when changing the query class (would be
     * overwritten at recreation. This also avoid extranous recreations.
     */
    /*
    internal implementation notes:
    - for necessity for instance creation see class comment
    */
    private final Map<Class<?>, QueryPanel<Object>> entityEditingQueryPanelCache = new HashMap<>();
    private final IssueHandler issueHandler;
    private final ConfirmMessageHandler confirmMessageHandler;
    private final FieldInitializer fieldInitializer;
    private final QueryHistoryEntryStorage entryStorage;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JButton cancelButton = new JButton();
    private final JButton deleteButton = new JButton();
    private final JButton editButton = new JButton();
    private final JComboBox<Class<?>> entityEditingClassComboBox = new JComboBox<>();
    private final JLabel entityEditingClassComboBoxLabel = new JLabel();
    private final JSeparator entityEditingClassSeparator = new JSeparator();
    private final JScrollPane entityEditingQueryPanelScrollPane = new JScrollPane();
    // End of variables declaration//GEN-END:variables

    public EntityEditingDialog(Window parent,
            Set<Class<?>> entityClasses,
            Class<?> primaryClassSelection,
            PersistenceStorage storage,
            IssueHandler issueHandler,
            ConfirmMessageHandler confirmMessageHandler,
            FieldInitializer fieldInitializer,
            QueryHistoryEntryStorage entryStorage,
            FieldRetriever fieldRetriever) {
        super(parent,
                ModalityType.APPLICATION_MODAL);
        this.issueHandler = issueHandler;
        this.confirmMessageHandler = confirmMessageHandler;
        this.storage = storage;
        this.fieldInitializer = fieldInitializer;
        this.entryStorage = entryStorage;
        init(entityClasses, primaryClassSelection, storage);
        this.fieldRetriever = fieldRetriever;
        init1(entityClasses, primaryClassSelection);
    }

    private void init(Set<Class<?>> entityClasses,
            Class<?> primaryClassSelection,
            PersistenceStorage storage) {
        initComponents();
        if(issueHandler == null) {
            throw new IllegalArgumentException("messageHandler mustn't be null");
        }
        if(storage == null) {
            throw new IllegalArgumentException("storage mustn't be null");
        }
        if(entityClasses == null) {
            throw new IllegalArgumentException("entityClasses mustn't be null");
        }
        if(entityClasses.isEmpty()) {
            throw new IllegalArgumentException("entityClass mustn't be empty");
        }
        if(!entityClasses.contains(primaryClassSelection)) {
            throw new IllegalArgumentException(String.format("primaryClassSelection '%s' has to be contained in entityClasses", primaryClassSelection));
        }
    }

    private void init1(Set<Class<?>> entityClasses,
            Class<?> primaryClassSelection) {
        this.entityEditingClassComboBox.addItemListener(new ItemListener() {
            @Override
            @SuppressWarnings("PMD.AvoidCatchingThrowable")
            public void itemStateChanged(ItemEvent e) {
                try {
                    //don't recreate QueryPanel instances, but cache them in order
                    //to keep custom queries managed in QueryPanel
                    if(EntityEditingDialog.this.entityEditingQueryPanel != null) {
                        Class<?> selectedEntityClass = (Class<?>) e.getItem();
                        handleEntityEditingQueryPanelUpdate(selectedEntityClass);
                    }
                }catch(Throwable ex) {
                    LOGGER.error("unexpected exception during handling of query panel update",
                            ex);
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                }
            }
        });
        List<Class<?>> entityClassesSort = DocumentScannerUtils.sortEntityClasses(entityClasses);
        for(Class<?> entityClass : entityClassesSort) {
            this.entityEditingClassComboBoxModel.addElement(entityClass);
        }
        this.entityEditingClassComboBox.setSelectedItem(primaryClassSelection);
    }

    private void handleEntityEditingQueryPanelUpdate(Class<?> selectedEntityClass) throws FieldHandlingException,
            ResetException {
        entityEditingQueryPanel = this.entityEditingQueryPanelCache.get(selectedEntityClass);
        if(entityEditingQueryPanel == null) {
            this.entityEditingQueryPanel = new QueryPanel(storage,
                    selectedEntityClass,
                    issueHandler,
                    fieldRetriever,
                    null, //initialValue
                    null, //bidirectionalControlPanel (doesn't make sense)
                    ListSelectionModel.MULTIPLE_INTERVAL_SELECTION,
                    fieldInitializer,
                    entryStorage
            );
            entityEditingQueryPanelCache.put(selectedEntityClass, entityEditingQueryPanel);
        }
        this.entityEditingQueryPanelScrollPane.setViewportView(entityEditingQueryPanel);
        this.entityEditingQueryPanelScrollPane.getVerticalScrollBar().setUnitIncrement(Constants.DEFAULT_SCROLL_INTERVAL);
        this.entityEditingQueryPanelScrollPane.getHorizontalScrollBar().setUnitIncrement(Constants.DEFAULT_SCROLL_INTERVAL);
        this.entityEditingQueryPanel.clearSelection();
    }

    public List<Object> getSelectedEntities() {
        return this.entityEditingQueryPanel.getSelectedObjects();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 1024, 768));
        setPreferredSize(new java.awt.Dimension(1024, 768));
        setSize(new java.awt.Dimension(1024, 768));

        entityEditingClassComboBox.setModel(entityEditingClassComboBoxModel);
        entityEditingClassComboBox.setRenderer(entityEditingClassComboBoxRenderer);
        entityEditingClassComboBox.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entityEditingClassComboBoxActionPerformed(evt);
            }
        });

        entityEditingClassComboBoxLabel.setText("Query class:");

        editButton.setText("Edit");
        editButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(entityEditingClassComboBoxLabel)
                        .addGap(18, 18, 18)
                        .addComponent(entityEditingClassComboBox, 0, 606, Short.MAX_VALUE))
                    .addComponent(entityEditingClassSeparator)
                    .addComponent(entityEditingQueryPanelScrollPane, GroupLayout.DEFAULT_SIZE, 714, Short.MAX_VALUE)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(cancelButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(entityEditingClassComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(entityEditingClassComboBoxLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(entityEditingClassSeparator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(entityEditingQueryPanelScrollPane, GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(editButton)
                    .addComponent(cancelButton)
                    .addComponent(deleteButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void entityEditingClassComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entityEditingClassComboBoxActionPerformed
        try {
            Class<?> selectedEntityClass = (Class<?>) entityEditingClassComboBox.getSelectedItem();
            handleEntityEditingQueryPanelUpdate(selectedEntityClass);
        } catch (FieldHandlingException
                | ResetException ex) {
            LOGGER.error("unexpected exception during resetting of component",
                    ex);
            issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        }
    }//GEN-LAST:event_entityEditingClassComboBoxActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.entityEditingQueryPanel.clearSelection(); //causes
            //getSelectedEntities to return an empty list which indicates that
            //the dialog has been canceled
        this.setVisible(false);
    }//GEN-LAST:event_cancelButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void editButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editButtonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_editButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        String answer = confirmMessageHandler.confirm(new Message(
                "Do you really want to delete all selected entities?",
                JOptionPane.QUESTION_MESSAGE,
                "Delete entities"),
                "Yes", "No");
        if("Yes".equals(answer)) {
            List<Object> selectedEntities = this.entityEditingQueryPanel.getSelectedObjects();
            for(Object selectedEntity : selectedEntities) {
                try {
                    this.storage.delete(selectedEntity);
                } catch (StorageException ex) {
                    issueHandler.handle(new Message(ex,
                            JOptionPane.ERROR_MESSAGE));
                }
            }
            this.entityEditingQueryPanel.getQueryComponent().repeatLastQuery();
        }
    }//GEN-LAST:event_deleteButtonActionPerformed
}
