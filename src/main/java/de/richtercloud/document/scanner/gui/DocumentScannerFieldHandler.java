/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import de.richtercloud.document.scanner.components.OCRResultPanel;
import de.richtercloud.document.scanner.components.OCRResultPanelFetcher;
import de.richtercloud.document.scanner.components.OCRResultPanelUpdateEvent;
import de.richtercloud.document.scanner.components.OCRResultPanelUpdateListener;
import de.richtercloud.document.scanner.components.ScanResultPanel;
import de.richtercloud.document.scanner.components.ScanResultPanelFetcher;
import de.richtercloud.document.scanner.components.ScanResultPanelUpdateEvent;
import de.richtercloud.document.scanner.components.ScanResultPanelUpdateListener;
import de.richtercloud.document.scanner.components.WorkflowItemTreePanel;
import de.richtercloud.document.scanner.components.WorkflowItemTreePanelUpdateEvent;
import de.richtercloud.document.scanner.components.WorkflowItemTreePanelUpdateListener;
import de.richtercloud.document.scanner.components.annotations.CommunicationTree;
import de.richtercloud.document.scanner.components.annotations.OCRResult;
import de.richtercloud.document.scanner.components.annotations.ScanResult;
import de.richtercloud.document.scanner.components.annotations.Tags;
import de.richtercloud.document.scanner.components.tag.TagComponent;
import de.richtercloud.document.scanner.components.tag.TagComponentUpdateEvent;
import de.richtercloud.document.scanner.components.tag.TagComponentUpdateListener;
import de.richtercloud.document.scanner.components.tag.TagStorage;
import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.ifaces.ImageWrapper;
import de.richtercloud.document.scanner.ifaces.MainPanel;
import de.richtercloud.document.scanner.model.WorkflowItem;
import de.richtercloud.message.handler.ConfirmMessageHandler;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.ComponentHandler;
import de.richtercloud.reflection.form.builder.ResetException;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorage;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetriever;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyUsageStatisticsStorage;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldHandler;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldHandlingException;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldUpdateEvent;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldUpdateListener;
import de.richtercloud.reflection.form.builder.fieldhandler.MappedFieldUpdateEvent;
import de.richtercloud.reflection.form.builder.fieldhandler.MappingFieldHandler;
import de.richtercloud.reflection.form.builder.fieldhandler.factory.AmountMoneyMappingFieldHandlerFactory;
import de.richtercloud.reflection.form.builder.jpa.JPAReflectionFormBuilder;
import de.richtercloud.reflection.form.builder.jpa.fieldhandler.JPAMappingFieldHandler;
import de.richtercloud.reflection.form.builder.jpa.fieldhandler.factory.JPAAmountMoneyMappingFieldHandlerFactory;
import de.richtercloud.reflection.form.builder.jpa.idapplier.IdApplier;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryHistoryEntryStorage;
import de.richtercloud.reflection.form.builder.jpa.panels.StringAutoCompletePanel;
import de.richtercloud.reflection.form.builder.jpa.storage.FieldInitializer;
import de.richtercloud.reflection.form.builder.jpa.storage.PersistenceStorage;
import de.richtercloud.reflection.form.builder.jpa.typehandler.ElementCollectionTypeHandler;
import de.richtercloud.reflection.form.builder.jpa.typehandler.ToManyTypeHandler;
import de.richtercloud.reflection.form.builder.jpa.typehandler.ToOneTypeHandler;
import de.richtercloud.reflection.form.builder.typehandler.TypeHandler;
import de.richtercloud.validation.tools.FieldRetriever;
import java.awt.Component;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.DocumentFilter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author richter
 */
public class DocumentScannerFieldHandler extends JPAMappingFieldHandler<Object, FieldUpdateEvent<Object>> {
    private final OCRResultPanelFetcher oCRResultPanelFetcher;
    private final ScanResultPanelFetcher scanResultPanelFetcher;
    private final DocumentScannerConf documentScannerConf;
    private final PersistenceStorage storage;
    private final Set<Class<?>> entityClasses;
    private final Class<?> primaryClassSelection;
    private final MainPanel mainPanel;
    private final TagStorage tagStorage;
    private final ConfirmMessageHandler confirmMessageHandler;
    private final FieldInitializer fieldInitializer;
    private final QueryHistoryEntryStorage entryStorage;
    private final FieldRetriever readOnlyFieldRetriever;

    private final static ComponentHandler<OCRResultPanel> OCR_RESULT_PANEL_COMPONENT_RESETTABLE = new ComponentHandler<OCRResultPanel>() {
        @Override
        public void reset(OCRResultPanel component) {
            component.reset();
        }
    };
    private final static ComponentHandler<ScanResultPanel> SCAN_RESULT_PANEL_COMPONENT_RESETTABLE = new ComponentHandler<ScanResultPanel>() {
        @Override
        public void reset(ScanResultPanel component) {
            component.reset();
        }
    };
    private final static ComponentHandler<WorkflowItemTreePanel> COMMUNICATION_TREE_PANEL_COMPONENT_HANDLER = new ComponentHandler<WorkflowItemTreePanel>() {
        @Override
        public void reset(WorkflowItemTreePanel component) throws ResetException {
            component.reset();
        }
    };
    private final static ComponentHandler<TagComponent> TAG_COMPONENT_HANDLER = new ComponentHandler<TagComponent>() {
        @Override
        public void reset(TagComponent component) throws ResetException {
            component.reset();
        }
    };

    public static DocumentScannerFieldHandler create(AmountMoneyUsageStatisticsStorage amountMoneyUsageStatisticsStorage,
            AmountMoneyCurrencyStorage amountMoneyCurrencyStorage,
            AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever,
            IssueHandler issueHandler,
            ConfirmMessageHandler confirmMessageHandler,
            Map<Type, TypeHandler<?, ?,?, ?>> typeHandlerMapping,
            PersistenceStorage storage,
            FieldRetriever readOnlyFieldRetriever,
            OCRResultPanelFetcher oCRResultPanelFetcher,
            ScanResultPanelFetcher scanResultPanelFetcher,
            DocumentScannerConf documentScannerConf,
            Set<Class<?>> entityClasses,
            Class<?> primaryClassSelection,
            MainPanel mainPanel,
            TagStorage tagStorage,
            IdApplier idApplier,
            int initialQueryLimit,
            String bidirectionalHelpDialogTitle,
            FieldInitializer fieldInitializer,
            QueryHistoryEntryStorage entryStorage) {
        AmountMoneyMappingFieldHandlerFactory embeddableFieldHandlerFactory = new AmountMoneyMappingFieldHandlerFactory(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        FieldHandler embeddableFieldHandler = new MappingFieldHandler(embeddableFieldHandlerFactory.generateClassMapping(),
                embeddableFieldHandlerFactory.generatePrimitiveMapping(),
                issueHandler);
        ElementCollectionTypeHandler elementCollectionTypeHandler = new ElementCollectionTypeHandler(typeHandlerMapping,
                typeHandlerMapping,
                issueHandler,
                embeddableFieldHandler,
                readOnlyFieldRetriever);
        JPAAmountMoneyMappingFieldHandlerFactory jPAAmountMoneyMappingFieldHandlerFactory = JPAAmountMoneyMappingFieldHandlerFactory.create(storage,
                initialQueryLimit,
                issueHandler,
                amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                readOnlyFieldRetriever);
        ToManyTypeHandler toManyTypeHandler = new ToManyTypeHandler(storage,
                issueHandler,
                typeHandlerMapping,
                typeHandlerMapping,
                bidirectionalHelpDialogTitle,
                fieldInitializer,
                entryStorage,
                readOnlyFieldRetriever);
        ToOneTypeHandler toOneTypeHandler = new ToOneTypeHandler(storage,
                issueHandler,
                bidirectionalHelpDialogTitle,
                fieldInitializer,
                entryStorage,
                readOnlyFieldRetriever);
        DocumentScannerFieldHandler retValue = new DocumentScannerFieldHandler(jPAAmountMoneyMappingFieldHandlerFactory.generateClassMapping(),
                embeddableFieldHandlerFactory.generateClassMapping(),
                embeddableFieldHandlerFactory.generatePrimitiveMapping(),
                elementCollectionTypeHandler,
                toManyTypeHandler,
                toOneTypeHandler,
                issueHandler,
                confirmMessageHandler,
                oCRResultPanelFetcher,
                scanResultPanelFetcher,
                documentScannerConf,
                storage,
                entityClasses,
                primaryClassSelection,
                mainPanel,
                tagStorage,
                idApplier,
                fieldInitializer,
                entryStorage,
                readOnlyFieldRetriever);
        return retValue;
    }

    public DocumentScannerFieldHandler(Map<Type, FieldHandler<?, ?, ?, ?>> classMapping,
            Map<Type, FieldHandler<?, ?, ?, ?>> embeddableMapping,
            Map<Class<?>, FieldHandler<?, ?, ?, ?>> primitiveMapping,
            ElementCollectionTypeHandler elementCollectionTypeHandler,
            ToManyTypeHandler toManyTypeHandler,
            ToOneTypeHandler toOneTypeHandler,
            IssueHandler issueHandler,
            ConfirmMessageHandler confirmMessageHandler,
            OCRResultPanelFetcher oCRResultPanelFetcher,
            ScanResultPanelFetcher scanResultPanelFetcher,
            DocumentScannerConf documentScannerConf,
            PersistenceStorage storage,
            Set<Class<?>> entityClasses,
            Class<?> primaryClassSelection,
            MainPanel mainPanel,
            TagStorage tagStorage,
            IdApplier idApplier,
            FieldInitializer fieldInitializer,
            QueryHistoryEntryStorage entryStorage,
            FieldRetriever readOnlyFieldRetriever) {
        super(classMapping,
                embeddableMapping,
                primitiveMapping,
                elementCollectionTypeHandler,
                toManyTypeHandler,
                toOneTypeHandler,
                issueHandler,
                idApplier);
        this.oCRResultPanelFetcher = oCRResultPanelFetcher;
        this.scanResultPanelFetcher = scanResultPanelFetcher;
        this.documentScannerConf = documentScannerConf;
        if(storage == null) {
            throw new IllegalArgumentException("storage mustn't be null");
        }
        this.storage = storage;
        this.entityClasses = entityClasses;
        this.primaryClassSelection = primaryClassSelection;
        this.mainPanel = mainPanel;
        this.tagStorage = tagStorage;
        this.confirmMessageHandler = confirmMessageHandler;
        this.fieldInitializer = fieldInitializer;
        this.entryStorage = entryStorage;
        this.readOnlyFieldRetriever = readOnlyFieldRetriever;
    }

    @Override
    protected Pair<JComponent, ComponentHandler<?>> handle0(Field field,
            Object instance,
            final FieldUpdateListener updateListener,
            JPAReflectionFormBuilder reflectionFormBuilder) throws FieldHandlingException,
            ResetException {
        if(field == null) {
            throw new IllegalArgumentException("field mustn't be null");
        }
        if(field.getAnnotation(OCRResult.class) != null) {
            String fieldValue;
            try {
                fieldValue = (String) field.get(instance);
            }catch(IllegalAccessException ex) {
                throw new FieldHandlingException(ex);
            }
            OCRResultPanel retValue = new OCRResultPanel(oCRResultPanelFetcher,
                    fieldValue,
                    getIssueHandler());
            retValue.addUpdateListener(new OCRResultPanelUpdateListener() {
                @Override
                public void onUpdate(OCRResultPanelUpdateEvent event) {
                    updateListener.onUpdate(new FieldUpdateEvent<>(event.getNewValue()));
                }
            });
            if(this.documentScannerConf.isAutoSaveOCRData()
                    && fieldValue == null) {
                //if fieldValue != null there's no need to start OCR (i.e. if
                //an invalid value has been persisted it's up to the user to
                //(re)start OCR manually
                retValue.doTask(true, //async
                        true //cancelable
                );
            }
            return new ImmutablePair<JComponent, ComponentHandler<?>>(retValue,
                    OCR_RESULT_PANEL_COMPONENT_RESETTABLE);
        }
        if(field.getAnnotation(ScanResult.class) != null) {
            List<ImageWrapper> fieldValue;
            try {
                fieldValue = (List<ImageWrapper>) field.get(instance);
            }catch(IllegalAccessException ex) {
                throw new FieldHandlingException(ex);
            }
            ScanResultPanel retValue = new ScanResultPanel(scanResultPanelFetcher,
                    fieldValue,
                    true, //async
                    true //cancelable
            );
            retValue.addUpdateListerner(new ScanResultPanelUpdateListener() {
                @Override
                public void onUpdate(ScanResultPanelUpdateEvent event) {
                    updateListener.onUpdate(new FieldUpdateEvent<>(event.getNewValue()));
                }
            });
            if(documentScannerConf.isAutoSaveImageData()
                    && (fieldValue == null || fieldValue.isEmpty())) {
                //if fieldValue != null and !fieldValue.isEmpty then there's no
                //need to save the image data
                retValue.doTask(true, //async
                        true //cancelable
                );
            }
            return new ImmutablePair<JComponent, ComponentHandler<?>>(retValue,
                    SCAN_RESULT_PANEL_COMPONENT_RESETTABLE);
        }
        if(field.getAnnotation(CommunicationTree.class) != null) {
            List<WorkflowItem> fieldValue;
            try {
                fieldValue = (List<WorkflowItem>) field.get(instance);
            }catch(IllegalAccessException ex) {
                throw new FieldHandlingException(ex);
            }
            WorkflowItemTreePanel retValue = new WorkflowItemTreePanel(storage,
                    fieldValue,
                    getIssueHandler(),
                    confirmMessageHandler,
                    readOnlyFieldRetriever,
                    entityClasses,
                    primaryClassSelection,
                    mainPanel,
                    fieldInitializer,
                    entryStorage
            );
            retValue.addUpdateListener(new WorkflowItemTreePanelUpdateListener() {
                @Override
                public void onUpdate(WorkflowItemTreePanelUpdateEvent event) {
                    updateListener.onUpdate(new MappedFieldUpdateEvent<>(event.getNewValue(),
                            event.getMappedField() //mappedField
                    ));
                }
            });
            return new ImmutablePair<JComponent, ComponentHandler<?>>(retValue,
                    COMMUNICATION_TREE_PANEL_COMPONENT_HANDLER);
        }
        if(field.getAnnotation(Tags.class) != null) {
            Set<String> fieldValue;
            try {
                fieldValue = (Set<String>) field.get(instance);
            }catch(IllegalAccessException ex) {
                throw new FieldHandlingException(ex);
            }
            TagComponent retValue = new TagComponent(tagStorage,
                    fieldValue,
                    getIssueHandler());
            retValue.addUpdateListener(new TagComponentUpdateListener() {
                @Override
                public void onUpdate(TagComponentUpdateEvent updateEvent) {
                    updateListener.onUpdate(new FieldUpdateEvent(updateEvent.getNewValue()));
                }
            });
            return new ImmutablePair<JComponent, ComponentHandler<?>>(retValue,
                    TAG_COMPONENT_HANDLER);
        }
        if(field.getType().equals(String.class)) {
            Pair<JComponent, ComponentHandler<?>> retValue = super.handle0(field,
                    instance,
                    updateListener,
                    reflectionFormBuilder);
            assert retValue.getKey() instanceof StringAutoCompletePanel;
            StringAutoCompletePanel retValueCast = (StringAutoCompletePanel) retValue.getKey();
            Component retValueCastEditorComponent = retValueCast.getComboBox().getEditor().getEditorComponent();
            assert retValueCastEditorComponent instanceof JTextField;
            JTextField retValueCastEditorTextField = (JTextField) retValueCastEditorComponent;
            AbstractDocument retValueCastEditorTextFieldDocument = (AbstractDocument)retValueCastEditorTextField.getDocument();
            DocumentFilter documentFilterGlazedLists = retValueCastEditorTextFieldDocument.getDocumentFilter();
            retValueCastEditorTextFieldDocument.setDocumentFilter(new TrimDocumentFilter(documentFilterGlazedLists,
                    mainPanel));
            return retValue;
        }
        return super.handle0(field, instance, updateListener, reflectionFormBuilder);
    }
}
