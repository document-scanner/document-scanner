/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import au.com.southsky.jfreesane.SaneDevice;
import au.com.southsky.jfreesane.SaneException;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import com.beust.jcommander.JCommander;
import com.google.common.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;
import de.richtercloud.document.scanner.components.ValueDetectionPanel;
import de.richtercloud.document.scanner.components.tag.FileTagStorage;
import de.richtercloud.document.scanner.components.tag.TagStorage;
import de.richtercloud.document.scanner.controller.DefaultValueDetectionServiceController;
import de.richtercloud.document.scanner.controller.ValueDetectionServiceController;
import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.gui.conf.DocumentScannerConfValidationException;
import de.richtercloud.document.scanner.gui.scanner.DocumentSource;
import de.richtercloud.document.scanner.gui.scanner.ScannerEditDialog;
import de.richtercloud.document.scanner.gui.scanner.ScannerPageSelectDialog;
import de.richtercloud.document.scanner.gui.scanner.ScannerSelectionDialog;
import de.richtercloud.document.scanner.gui.scanresult.DeviceOpeningAlreadyInProgressException;
import de.richtercloud.document.scanner.gui.scanresult.DocumentController;
import de.richtercloud.document.scanner.gui.scanresult.ScanJob;
import de.richtercloud.document.scanner.gui.scanresult.ScanJobFinishCallback;
import de.richtercloud.document.scanner.gui.scanresult.ScanResultDialog;
import de.richtercloud.document.scanner.gui.storageconf.StorageConfPanelCreationException;
import de.richtercloud.document.scanner.gui.storageconf.StorageSelectionDialog;
import de.richtercloud.document.scanner.ifaces.DocumentAddException;
import de.richtercloud.document.scanner.ifaces.DocumentExportFormat;
import de.richtercloud.document.scanner.ifaces.DocumentItem;
import de.richtercloud.document.scanner.ifaces.ImageWrapper;
import de.richtercloud.document.scanner.ifaces.ImageWrapperException;
import de.richtercloud.document.scanner.ifaces.MainPanel;
import de.richtercloud.document.scanner.ifaces.OCREngine;
import de.richtercloud.document.scanner.ifaces.OCREngineConf;
import de.richtercloud.document.scanner.ifaces.OCRSelectComponent;
import de.richtercloud.document.scanner.ifaces.OCRSelectPanel;
import de.richtercloud.document.scanner.model.Company;
import de.richtercloud.document.scanner.model.Document;
import de.richtercloud.document.scanner.model.imagewrapper.CachingImageWrapper;
import de.richtercloud.document.scanner.model.imagewrapper.ImageWrapperStorageDirExistsException;
import de.richtercloud.document.scanner.model.warninghandler.CompanyWarningHandler;
import de.richtercloud.document.scanner.ocr.DelegatingOCREngineFactory;
import de.richtercloud.document.scanner.ocr.OCREngineFactory;
import de.richtercloud.document.scanner.ocr.OCREngineSelectDialog;
import de.richtercloud.document.scanner.setter.AmountMoneyPanelSetter;
import de.richtercloud.document.scanner.setter.EmbeddableListPanelSetter;
import de.richtercloud.document.scanner.setter.LongIdPanelSetter;
import de.richtercloud.document.scanner.setter.QueryPanelSetter;
import de.richtercloud.document.scanner.setter.SpinnerSetter;
import de.richtercloud.document.scanner.setter.StringAutoCompletePanelSetter;
import de.richtercloud.document.scanner.setter.TextFieldSetter;
import de.richtercloud.document.scanner.setter.UtilDatePickerSetter;
import de.richtercloud.document.scanner.setter.ValueSetter;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceConf;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceConfDialog;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceCreationException;
import de.richtercloud.message.handler.BugHandler;
import de.richtercloud.message.handler.ConfirmMessageHandler;
import de.richtercloud.message.handler.DefaultIssueHandler;
import de.richtercloud.message.handler.DialogBugHandler;
import de.richtercloud.message.handler.DialogConfirmMessageHandler;
import de.richtercloud.message.handler.DialogMessageHandler;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.message.handler.JavaFXDialogIssueHandler;
import de.richtercloud.message.handler.Message;
import de.richtercloud.message.handler.MessageHandler;
import de.richtercloud.message.handler.raven.bug.handler.RavenBugHandler;
import de.richtercloud.reflection.form.builder.AnyType;
import de.richtercloud.reflection.form.builder.components.date.UtilDatePicker;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorage;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetriever;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetrieverException;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyPanel;
import de.richtercloud.reflection.form.builder.components.money.FailsafeAmountMoneyExchangeRateRetriever;
import de.richtercloud.reflection.form.builder.components.money.FileAmountMoneyCurrencyStorage;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldHandlingException;
import de.richtercloud.reflection.form.builder.jpa.IdGenerationException;
import de.richtercloud.reflection.form.builder.jpa.IdGenerator;
import de.richtercloud.reflection.form.builder.jpa.JPAFieldRetriever;
import de.richtercloud.reflection.form.builder.jpa.SequentialIdGenerator;
import de.richtercloud.reflection.form.builder.jpa.WarningHandler;
import de.richtercloud.reflection.form.builder.jpa.idapplier.IdApplier;
import de.richtercloud.reflection.form.builder.jpa.panels.EmbeddableListPanel;
import de.richtercloud.reflection.form.builder.jpa.panels.LongIdPanel;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryHistoryEntryStorage;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryHistoryEntryStorageCreationException;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryHistoryEntryStorageFactory;
import de.richtercloud.reflection.form.builder.jpa.panels.QueryPanel;
import de.richtercloud.reflection.form.builder.jpa.panels.StringAutoCompletePanel;
import de.richtercloud.reflection.form.builder.jpa.storage.AbstractPersistenceStorageConf;
import de.richtercloud.reflection.form.builder.jpa.storage.DelegatingPersistenceStorageFactory;
import de.richtercloud.reflection.form.builder.jpa.storage.FieldInitializer;
import de.richtercloud.reflection.form.builder.jpa.storage.PersistenceStorage;
import de.richtercloud.reflection.form.builder.jpa.storage.ReflectionFieldInitializer;
import de.richtercloud.reflection.form.builder.jpa.storage.copy.DelegatingStorageConfCopyFactory;
import de.richtercloud.reflection.form.builder.jpa.typehandler.JPAEntityListTypeHandler;
import de.richtercloud.reflection.form.builder.jpa.typehandler.factory.JPAAmountMoneyMappingTypeHandlerFactory;
import de.richtercloud.reflection.form.builder.retriever.FieldOrderValidationException;
import de.richtercloud.reflection.form.builder.storage.StorageConf;
import de.richtercloud.reflection.form.builder.storage.StorageConfValidationException;
import de.richtercloud.reflection.form.builder.storage.StorageCreationException;
import de.richtercloud.reflection.form.builder.storage.copy.StorageConfCopyException;
import de.richtercloud.reflection.form.builder.storage.copy.StorageConfCopyFactory;
import de.richtercloud.reflection.form.builder.typehandler.TypeHandler;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jscience.economics.money.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
internal implementation notes:
- the combobox in the storage create dialog for selection of the type of the new
storage doesn't have to be a StorageConf instance and it's misleading if it is
because such a StorageConf is about to be created -> use Class
- see DocumentScannerConf for design decisions regarding configuration file and
command line parameters
 */
/**
 * <h2>Status bar</h2>
 * In order to provide static status for a selected (and eventually
 * configurable) set of components (selected scanner, image selection mode,
 * etc.) and dynamic info, warning and error message, a status bar (at the
 * bottom of the main application window is introduced which is a container and
 * contains a subcontainer for each static information and one for the messages.
 * The latter is a popup displaying a scrollable list of message entries which
 * can be removed from it (e.g. with a close button). Static status subcontainer
 * can be sophisticated, e.g. the display for the currently selected scanner can
 * contain a button to select a scanner while none is selected an be a label
 * only while one is selected. That's why they're containers. The difference
 * between static information and messages is trivial.
 *
 * <h2>Window arrangement</h2>
 * Docking is superior in function and more flexible in programming changes than
 * split panes, so use a docking framework (see
 * <a href="https://richtercloud.de:446/doku.php?id=tasks:java:docking_framework">
 * https://richtercloud.de:446/doku.php?id=tasks:java:docking_framework</a>
 * for choices). In order to allow easy switching between
 * documents a tabbed pane should be used. In order to avoid non-constructive
 * docking/window configuration for the user which is possible if all components
 * of a document are managed in one tab and separate docking is set up for each.
 * Switch document tabs on one component of the document and make its other
 * components change - like NetBeans does with editor and navigator and others.
 *
 * <h2>Scan jobs</h2>
 * Scans ought to occur in a background task while already scanned and sorted
 * documents can be stored. It's too much work to provide extra behavior for the
 * first scan task which seems odd to be placed into a background task
 * immediately, but this can be compensated by making the scan result dialog
 * appear automatically if there're no open documents.
 *
 * @author richter
 */
@SuppressWarnings({"PMD.MoreThanOneLogger",
    "PMD.SingularField"})
    //referencing root logger in method which triggers this warning for no reason
public class DocumentScanner extends JFrame implements Managed<Exception> {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentScanner.class);
    /**
     * The Raven/sentry.io DSN. Doesn't make sense to hide from source according
     * to http://stackoverflow.com/questions/41843941/where-to-get-the-sentry-
     * raven-dsn-from-at-runtime.
     */
    private final static String RAVEN_DSN = "https://d4001abecf704dd790bb72bb5a7e0dc8:e131224c9d3e413fa194f3210ba99751@sentry.io/131229";
    private SaneDevice scannerDevice;
    private DocumentScannerConf documentScannerConf;
    private final Map<Class<? extends JComponent>, ValueSetter<?,?>> valueSetterMapping;
    private final AmountMoneyCurrencyStorage amountMoneyCurrencyStorage;
    private final AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever;
    private final TagStorage tagStorage;
    private final Map<java.lang.reflect.Type, TypeHandler<?, ?,?, ?>> typeHandlerMapping;
    private final IssueHandler issueHandler;
    private final MessageHandler messageHandler = new DialogMessageHandler(this,
            "", //titlePrefix
            generateTitleSuffix() //titleSuffix
    );
    private final JavaFXDialogIssueHandler javaFXDialogMessageHandler = new JavaFXDialogIssueHandler();
    private final ConfirmMessageHandler confirmMessageHandler = new DialogConfirmMessageHandler(this);
    private final Map<Class<?>, WarningHandler<?>> warningHandlers = new HashMap<>();
    static {
        new JFXPanel();
    }
    /*
    internal implementation notes:
    - can't be final because it's initialized in init because it depends on
    initialized entityManager
    */
    private MainPanel mainPanel;
    private final OCREngineFactory oCREngineFactory;
    private OCREngine oCREngine;
    /**
     * The default non-zero exit code.
     */
    private final static int SYSTEM_EXIT_ERROR_GENERAL = 1;
    private final IdApplier<ValueDetectionPanel> idApplier;
    private final IdGenerator<Long> idGenerator;
    /**
     * If multiple entities are selected in a {@link EntityEditingDialog} it
     * might take a long time to open documents for all of them, so a warning is
     * displayed if more documents than this value are about to be opened.
     */
    private PersistenceStorage<Long> storage;
    private final DelegatingPersistenceStorageFactory delegatingStorageFactory;
    private final FieldInitializer queryComponentFieldInitializer;
    private final StorageConfCopyFactory storageConfCopyFactory = new DelegatingStorageConfCopyFactory();
    private final QueryHistoryEntryStorageFactory entryStorageFactory;
    private final QueryHistoryEntryStorage entryStorage;
    private final JPAFieldRetriever fieldRetriever;
    /**
     * Start to fetch results and warm up the cache after start.
     */
    private final Thread amountMoneyExchangeRetrieverInitThread;
    /**
     * Allows early initialization of Apache Ignite in the background.
     */
    /*
    internal implementation notes:
    - not necessary that this is a property, but kept as such for convenience.
    */
    private final Thread cachingImageWrapperInitThread;
    private final Thread formatUtilsInitThread;
    private final DocumentController documentController;
    private final static String YES = "Yes";
    private final static String NO = "No";
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JMenuItem aboutMenuItem = new JMenuItem();
    private final JMenuItem closeMenuItem = new JMenuItem();
    private final JPopupMenu.Separator databaseMenuSeparator = new JPopupMenu.Separator();
    private final JMenuItem editEntryMenuItem = new JMenuItem();
    private final JMenuItem exitMenuItem = new JMenuItem();
    private final JPopupMenu.Separator exitMenuItemSeparator = new JPopupMenu.Separator();
    private final JMenuItem exportMenuItem = new JMenuItem();
    private final JMenu fileMenu = new JMenu();
    private final JMenu helpMenu = new JMenu();
    private final JPopupMenu.Separator knownScannersMenuItemSeparator = new JPopupMenu.Separator();
    private final JPopupMenu.Separator knownStoragesMenuItemSeparartor = new JPopupMenu.Separator();
    private final JMenuBar mainMenuBar = new JMenuBar();
    private final JPanel mainPanelPanel = new JPanel();
    private final JMenuItem oCRMenuItem = new JMenuItem();
    private final JPopupMenu.Separator oCRMenuSeparator = new JPopupMenu.Separator();
    private final JMenuItem openMenuItem = new JMenuItem();
    private final JMenuItem openSelectionMenuItem = new JMenuItem();
    private final JMenuItem optionsMenuItem = new JMenuItem();
    private final JMenuItem saveMenuItem = new JMenuItem();
    private final JMenuItem scanMenuItem = new JMenuItem();
    private final JMenuItem scanResultsMenuItem = new JMenuItem();
    private final JMenu scannerSelectionMenu = new JMenu();
    private final JMenuItem selectScannerMenuItem = new JMenuItem();
    private final JMenu storageSelectionMenu = new JMenu();
    private final JMenuItem storageSelectionMenuItem = new JMenuItem();
    private final JMenu toolsMenu = new JMenu();
    private final JMenuItem valueDetectionMenuItem = new JMenuItem();
    // End of variables declaration//GEN-END:variables
    private final ValueDetectionServiceController valueDetectionServiceController;

    public static String generateApplicationWindowTitle(String title, String applicationName, String applicationVersion) {
        return String.format("%s - %s %s", title, applicationName, applicationVersion);
    }

    /*
    internal implementation notes:
    - resources are opened in init methods only (see https://richtercloud.de:446/doku.php?id=programming:java#resource_handling for details)
    */
    public DocumentScanner(DocumentScannerConf documentScannerConf) throws IOException,
            StorageCreationException,
            ImageWrapperStorageDirExistsException,
            QueryHistoryEntryStorageCreationException,
            IdGenerationException,
            FieldOrderValidationException,
            DocumentScannerConfValidationException,
            ValueDetectionServiceCreationException {
        super();
        this.documentScannerConf = documentScannerConf;

        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger rootLogger = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);
        //configure debug level of root logger
        if (this.documentScannerConf.isDebug()) {
            rootLogger.setLevel(Level.DEBUG);
            Thread.currentThread().getContextClassLoader().setDefaultAssertionStatus(true);
        }
        //configure logging to file (from http://stackoverflow.com/questions/16910955/programmatically-configure-logback-appender)
        PatternLayoutEncoder patternLayoutEncoder = new PatternLayoutEncoder();
        patternLayoutEncoder.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n");
        patternLayoutEncoder.setContext(loggerContext);
        patternLayoutEncoder.start();
        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<ILoggingEvent>();
        fileAppender.setFile(documentScannerConf.getLogFilePath());
        fileAppender.setEncoder(patternLayoutEncoder);
        fileAppender.setContext(loggerContext);
        fileAppender.setRollingPolicy(new FixedWindowRollingPolicy());
        fileAppender.start();
        rootLogger.addAppender(fileAppender);
        LOGGER.info(String.format("logging to file '%s'", documentScannerConf.getLogFilePath()));

        //check whether user allowed automatic bug tracking
        if(!documentScannerConf.isSkipUserAllowedAutoBugTrackingQuestion()) {
            String yesOnce = "Yes, but in this session only";
            String yesAlways = "Yes, and never ask me again";
            String noOnce = "No, not this time";
            String noAlways = "No, and never ask me again";
            String answer = confirmMessageHandler.confirm(new Message(String.format("Do "
                    + "you allow %s to anonymously and automatically track "
                    + "issues and bugs?",
                    DocumentScannerUtils.APP_NAME),
                    JOptionPane.QUESTION_MESSAGE,
                    "Anoymous contribution"),
                    yesOnce,
                    yesAlways,
                    noOnce,
                    noAlways);
            if(answer.equals(yesAlways) || answer.equals(noAlways)) {
                this.documentScannerConf.setSkipUserAllowedAutoBugTrackingQuestion(true);
            }
            this.documentScannerConf.setUserAllowedAutoBugTracking(answer.equals(yesOnce) || answer.equals(yesAlways));
        }
        BugHandler bugHandler;
        if(this.documentScannerConf.isUserAllowedAutoBugTracking()) {
            bugHandler = new RavenBugHandler(RAVEN_DSN);
        }else {
            bugHandler = new DialogBugHandler(this,
                    DocumentScannerUtils.BUG_URL,
                    "", //titlePrefix
                    generateTitleSuffix() //titleSuffix
            );
        }
        this.issueHandler = new DefaultIssueHandler(messageHandler,
                bugHandler);

        //warn about difficult configuration values and validate (first warn and
        //then validate in order to allow resetting values which have been
        //changed accidentally which is much more comfortable then editing the
        //configuration file)
        documentScannerConf.warnCriticalValues(confirmMessageHandler);
        documentScannerConf.validate();

        this.fieldRetriever = new DocumentScannerFieldRetriever(documentScannerConf,
                DocumentScannerUtils.QUERYABLE_AND_EMBEDDABLE_CLASSES);

        this.delegatingStorageFactory = new DelegatingPersistenceStorageFactory("richtercloud_document-scanner_jar_1.0-SNAPSHOTPU",
                24, //@TODo: low limit no longer necessary after ImageWrapper is
                    //used for binary data storage in document
                issueHandler,
                fieldRetriever);

        this.amountMoneyExchangeRateRetriever = new FailsafeAmountMoneyExchangeRateRetriever(documentScannerConf.getAmountMoneyExchangeRateRetrieverFileCacheDir(),
                documentScannerConf.getAmountMoneyExchangeRateRetrieverExpirationMillis());
        this.amountMoneyExchangeRetrieverInitThread = new Thread(() -> {
            LOGGER.debug("Starting prefetching of currency exchange rates in "
                    + "the background");
            try {
                Set<Currency> supportedCurrencies = amountMoneyExchangeRateRetriever.getSupportedCurrencies();
                for(Currency supportedCurrency : supportedCurrencies) {
                    amountMoneyExchangeRateRetriever.retrieveExchangeRate(supportedCurrency);
                }
            } catch (AmountMoneyExchangeRateRetrieverException ex) {
                //all parts of FailsafeAmountMoneyExchangeRateRetriever failed
                LOGGER.error("unexpected exception during retrieval of exchange rate",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
            }
        },
                "amount-money-exchange-rate-retriever-init-thread"
        );
        this.amountMoneyExchangeRetrieverInitThread.start();
        this.cachingImageWrapperInitThread  = new Thread(() -> {
            try {
                Class.forName(CachingImageWrapper.class.getName());
                    //loads the static block in CachingImageWrapper in order
                    //to minimize delay when initializing the first
                    //CachingImageWrapper
            } catch (ClassNotFoundException ex) {
                LOGGER.error("unexpected exception during initialization of caching image wrapper class",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
            }
        },
                "caching-image-wrapper-init-thread"
        );
        this.cachingImageWrapperInitThread.start();
        this.formatUtilsInitThread = new Thread(() -> {
            try {
                FormatUtils.init();
            } catch (InterruptedException ex) {
                LOGGER.error("unexpected exception during initialization of caching image wrapper class",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
            }
        },
                "format-utils-init-thread"
        );
        this.formatUtilsInitThread.start();

        this.oCREngineFactory = new DelegatingOCREngineFactory(issueHandler);

        //Check that emptying image storage directory wasn't skipped at shutdown
        //due to application crash
        if(documentScannerConf.getImageWrapperStorageDir().list().length > 0) {
            int answer = confirmMessageHandler.confirm(new Message(String.format("The image "
                    + "wrapper storage directory '%s' isn't empty which can "
                    + "happen after an application crash. It needs to be "
                    + "emptied in order to make the application work "
                    + "correctly. In case there's any chance someone put file "
                    + "into that directory abort now and investigate the "
                    + "directory, otherwise confirm to delete all files in "
                    + "'%s'.",
                            documentScannerConf.getImageWrapperStorageDir().getAbsolutePath(),
                            documentScannerConf.getImageWrapperStorageDir().getAbsolutePath()),
                    JOptionPane.WARNING_MESSAGE, "Confirm emptying directory"));
            assert answer == JOptionPane.YES_OPTION || answer == JOptionPane.NO_OPTION;
            if(answer == JOptionPane.NO_OPTION) {
                throw new ImageWrapperStorageDirExistsException();
            }else {
                LOGGER.warn(String.format("cleaning image storage directory "
                        + "'%s'", documentScannerConf.getImageWrapperStorageDir().getAbsolutePath()));
                FileUtils.cleanDirectory(documentScannerConf.getImageWrapperStorageDir());
            }
        }

        this.entryStorageFactory = new DocumentScannerFileQueryHistoryEntryStorageFactory(documentScannerConf.getQueryHistoryEntryStorageFile(),
                DocumentScannerUtils.ENTITY_CLASSES,
                false,
                issueHandler);
        this.entryStorage = entryStorageFactory.create();

        StorageConf storageConf = documentScannerConf.getStorageConf();
        assert storageConf instanceof AbstractPersistenceStorageConf;
        this.storage = (PersistenceStorage) delegatingStorageFactory.create(storageConf);

        //initialize valueSetterMapping (after storage has been created)
        valueSetterMapping = generateValueSetterMapping(this.storage);

        this.idGenerator = new SequentialIdGenerator(storage);
        this.idApplier = new ValueDetectionPanelIdApplier(idGenerator);

        OCREngineConf oCREngineConf = documentScannerConf.getoCREngineConf();
        this.oCREngine = oCREngineFactory.create(oCREngineConf);
        this.documentController = new DocumentController(issueHandler,
                oCREngine);

        this.initComponents();

        validateProperties();
            //after initComponents because of afterScannerSelection involving
            //GUI components

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOGGER.info("running {} shutdown hooks", DocumentScanner.class);
            shutdownHook();
        },
                String.format("%s shutdown hook", DocumentScanner.class.getSimpleName())
        ));

        this.amountMoneyCurrencyStorage = new FileAmountMoneyCurrencyStorage(documentScannerConf.getAmountMoneyCurrencyStorageFile());
        this.tagStorage = new FileTagStorage(documentScannerConf.getTagStorageFile());
        JPAAmountMoneyMappingTypeHandlerFactory fieldHandlerFactory = new JPAAmountMoneyMappingTypeHandlerFactory(storage,
                DocumentScannerUtils.INITIAL_QUERY_LIMIT_DEFAULT,
                issueHandler,
                fieldRetriever);
        this.typeHandlerMapping = fieldHandlerFactory.generateTypeHandlerMapping();

        this.queryComponentFieldInitializer = new ReflectionFieldInitializer(fieldRetriever) {
            @Override
            protected boolean initializeField(Field field) throws FieldHandlingException {
                boolean retValue;
                try {
                    retValue = !field.equals(Document.class.getDeclaredField("scanData"));
                        //skip Document.scanData in query components
                } catch (NoSuchFieldException | SecurityException ex) {
                    LOGGER.error("unexpected exception during field initialization",
                            ex);
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                    throw new FieldHandlingException(ex);
                }
                return retValue;
            }
        };
            //Don't use DocumentScannerFieldInitializer here because it will
            //fetch all scanData of Document on all n query results resulting in
            //n*size of documents byte[]s memory consumption

        //after entity manager creation
        this.typeHandlerMapping.put(new TypeToken<List<AnyType>>() {
            }.getType(), new JPAEntityListTypeHandler(storage,
                    issueHandler,
                    DocumentScannerUtils.BIDIRECTIONAL_HELP_DIALOG_TITLE,
                    queryComponentFieldInitializer,
                    entryStorage,
                    fieldRetriever));
        //listen to window close button (x)
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close();
                shutdownHook();
            }
        });
        this.valueDetectionServiceController = new DefaultValueDetectionServiceController(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                storage,
                issueHandler,
                documentScannerConf);
    }

    /**
     * run all initialization routines which open resources which need to be
     * referenced by {@code this} in order to be closable (if everything is
     * initialized in the constructor and the initialization fails there's no
     * way to close opened resources).
     */
    @Override
    public void init() throws StorageConfValidationException, IOException, StorageConfValidationException {
        warningHandlers.put(Company.class,
                new CompanyWarningHandler(storage,
                        messageHandler,
                        confirmMessageHandler));
            //after entityManager has been initialized

        this.mainPanel = new DefaultMainPanel(DocumentScannerUtils.ENTITY_CLASSES,
                DocumentScannerUtils.PRIMARY_CLASS_SELECTION,
                valueSetterMapping,
                storage,
                amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler,
                confirmMessageHandler,
                this,
                oCREngine,
                typeHandlerMapping,
                documentScannerConf,
                tagStorage,
                idApplier,
                warningHandlers,
                queryComponentFieldInitializer,
                entryStorage,
                fieldRetriever,
                valueDetectionServiceController
        );
        mainPanelPanel.add(this.mainPanel);
    }

    /**
     * Validates  {@code conf} from configuration file.
     */
    private void validateProperties() throws IOException {
        //if a scanner address and device name is in persisted conf check if
        //it's accessible and treat it as selected scanner silently
        String scannerName = this.documentScannerConf.getScannerName();
        if(scannerName != null) {
            try {
                this.scannerDevice = documentController.getScannerDevice(scannerName,
                        this.documentScannerConf.getScannerConfMap(),
                        DocumentScannerConf.SCANNER_SANE_ADDRESS_DEFAULT,
                        documentScannerConf.getResolutionWish());
                afterScannerSelection();
            } catch (IOException | SaneException ex) {
                String text = handleSearchScannerException("An exception during the setup of "
                        + "previously selected scanner occured: ",
                        ex,
                        DocumentScannerUtils.SANED_BUG_INFO);
                messageHandler.handle(new Message(String.format("Exception during setup of previously selected scanner: %s\n%s", ExceptionUtils.getRootCauseMessage(ex), text),
                        JOptionPane.WARNING_MESSAGE,
                        "Exception occured"));
            }
        }
        if(!this.documentScannerConf.getImageWrapperStorageDir().exists()) {
            if(!this.documentScannerConf.getImageWrapperStorageDir().mkdirs()) {
                throw new IOException(String.format("Creation of image wrapper storage directory '%s' failed",
                        this.documentScannerConf.getImageWrapperStorageDir().getAbsolutePath()));
            }
        }else {
            if(!this.documentScannerConf.getImageWrapperStorageDir().isDirectory()) {
                throw new IllegalStateException(String.format("Configured image wrapper storage directory '%s' is a file",
                        this.documentScannerConf.getImageWrapperStorageDir().getAbsolutePath()));
            }
        }
    }

    private static String generateTitleSuffix() {
        return String.format("- %s %s", DocumentScannerUtils.APP_NAME, DocumentScannerUtils.APP_VERSION);
    }

    public static Map<Class<? extends JComponent>, ValueSetter<?, ?>> generateValueSetterMapping(PersistenceStorage<Long> storage) {
        Map<Class<? extends JComponent>, ValueSetter<?,?>> valueSetterMapping0 = new HashMap<>();
        valueSetterMapping0.put(JTextField.class,
                TextFieldSetter.getInstance());
        valueSetterMapping0.put(JSpinner.class,
                SpinnerSetter.getInstance());
        valueSetterMapping0.put(LongIdPanel.class,
                LongIdPanelSetter.getInstance());
        valueSetterMapping0.put(StringAutoCompletePanel.class,
                StringAutoCompletePanelSetter.getInstance());
        valueSetterMapping0.put(AmountMoneyPanel.class,
                AmountMoneyPanelSetter.getInstance());
        valueSetterMapping0.put(UtilDatePicker.class,
                UtilDatePickerSetter.getInstance());
        valueSetterMapping0.put(EmbeddableListPanel.class,
                EmbeddableListPanelSetter.getInstance());
        valueSetterMapping0.put(QueryPanel.class,
                new QueryPanelSetter(storage));
        return Collections.unmodifiableMap(valueSetterMapping0);
    }

    public static String handleSearchScannerException(String text,
            Exception ex,
            String additional) {
        String message = ex.getMessage();
        if (ex.getCause() != null) {
            message = String.format("%s (caused by '%s')", message, ex.getCause().getMessage());
        }
        return String.format("<html>%s%s%s</html>", text, message, additional);
    }

    private void afterScannerSelection() {
        this.scanMenuItem.setEnabled(true);
        this.scanMenuItem.getParent().revalidate();
    }

    /**
     * Handles all non-resource related cleanup tasks (like persistence of
     * configuration). Callers have to make sure that this is invoked only once.
     * @see #close()
     */
    @SuppressWarnings("PMD.AvoidCatchingThrowable")
    private void shutdownHook() {
        try {
            LOGGER.info(String.format("running shutdown hooks in %s", DocumentScanner.class));
            mainPanel.getDocumentSwitchingMap().values().stream()
                    .map(oCRSelectComponentPair -> oCRSelectComponentPair.getValue())
                    .collect(Collectors.toList())
                    .forEach(entityPanel -> entityPanel.getValueDetectionServiceExecutor().cancelExecute());
            if (this.documentScannerConf != null) {
                try {
                    XStream xStream = new XStream();
                    xStream.toXML(this.documentScannerConf, Files.newOutputStream(this.documentScannerConf.getConfigFile().toPath()));
                } catch (FileNotFoundException ex) {
                    LOGGER.warn("an unexpected exception occured during save of configurations into file '{}', changes most likely lost", this.documentScannerConf.getConfigFile().getAbsolutePath());
                }
            }
            this.documentController.shutdown();
                //shuts down this.scannerDevice as well
            assert !this.scannerDevice.isOpen();
            if(this.storage != null) {
                this.storage.shutdown();
            }
            LOGGER.info(String.format("emptying image wrapper storage directory '%s'", this.documentScannerConf.getImageWrapperStorageDir().getAbsolutePath()));
            try {
                FileUtils.deleteDirectory(this.documentScannerConf.getImageWrapperStorageDir());
            } catch (IOException ex) {
                LOGGER.error("removal of image wrapper storage directory failed, see nested exception for details", ex);
            }
            if(!this.documentScannerConf.getImageWrapperStorageDir().mkdirs()) {
                LOGGER.error(String.format("re-creation of image wrapper storage dir '%s' failed", this.documentScannerConf.getImageWrapperStorageDir().getAbsolutePath()));
            }
            close();
            shutdownHookThreads();
            LOGGER.info(String.format("shutdown hooks in %s finished", DocumentScanner.class));
        }catch(Throwable ex) {
            LOGGER.error("unexpected exception during shutdown hook",
                    ex);
            issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        }
        this.issueHandler.shutdown();
    }

    private static void shutdownHookThreads() {
        CachingImageWrapper.shutdown();
        Platform.exit();
            //necessary in order to prevent hanging after all shutdown hooks
            //have been processed
    }

    /**
     * Handles all resource-related shutdown tasks.
     * @see #shutdownHook()
     */
    @Override
    public void close() {
        try {
            //@TODO: this can hang a shutdown (difficult to abort initialization
            //of resources)
            if(this.amountMoneyExchangeRetrieverInitThread.isAlive()) {
                LOGGER.debug("waiting for exchange rate retriever initialization "
                        + "to finish in order to allow clean shutdown");
                this.amountMoneyExchangeRetrieverInitThread.join();
            }
            if(this.cachingImageWrapperInitThread.isAlive()) {
                LOGGER.debug("waiting for caching framework initialization "
                        + "to finish in order to allow clean shutdown");
                this.cachingImageWrapperInitThread.join();
            }
        } catch (InterruptedException ex) {
            LOGGER.error("an exception during shutdown of threads occured, see nested exception for details", ex);
        }
        this.entryStorage.shutdown();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle(String.format("%s %s", DocumentScannerUtils.APP_NAME, DocumentScannerUtils.APP_VERSION) //generateApplicationWindowTitle not applicable
        );
        setBounds(new Rectangle(0, 0, 800, 600));
        setSize(new Dimension(800, 600));

        mainPanelPanel.setLayout(new BorderLayout());

        fileMenu.setText("File");

        scannerSelectionMenu.setText("Scanner selection");

        selectScannerMenuItem.setText("Select scanner...");
        selectScannerMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                selectScannerMenuItemActionPerformed(evt);
            }
        });
        scannerSelectionMenu.add(selectScannerMenuItem);
        scannerSelectionMenu.add(knownScannersMenuItemSeparator);

        fileMenu.add(scannerSelectionMenu);

        scanMenuItem.setText("Scan");
        scanMenuItem.setEnabled(false);
        scanMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                scanMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(scanMenuItem);

        scanResultsMenuItem.setText("Scan results...");
        scanResultsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                scanResultsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(scanResultsMenuItem);

        openMenuItem.setText("Open scan...");
        openMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        openSelectionMenuItem.setText("Open scan for selection...");
        openSelectionMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                openSelectionMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openSelectionMenuItem);

        closeMenuItem.setText("Close");
        closeMenuItem.setEnabled(false);
        closeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                closeMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(closeMenuItem);

        exportMenuItem.setText("Export...");
        exportMenuItem.setEnabled(false);
        exportMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportMenuItem);

        editEntryMenuItem.setText("Edit entry...");
        editEntryMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                editEntryMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(editEntryMenuItem);

        valueDetectionMenuItem.setText("Auto OCR value detection...");
        valueDetectionMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                valueDetectionMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(valueDetectionMenuItem);
        fileMenu.add(oCRMenuSeparator);

        oCRMenuItem.setText("Configure OCR engines");
        oCRMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                oCRMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(oCRMenuItem);
        fileMenu.add(databaseMenuSeparator);

        storageSelectionMenu.setText("Storage selection");

        storageSelectionMenuItem.setText("Select storage...");
        storageSelectionMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                storageSelectionMenuItemActionPerformed(evt);
            }
        });
        storageSelectionMenu.add(storageSelectionMenuItem);
        storageSelectionMenu.add(knownStoragesMenuItemSeparartor);

        fileMenu.add(storageSelectionMenu);

        saveMenuItem.setText("Save in database");
        saveMenuItem.setEnabled(false);
        fileMenu.add(saveMenuItem);
        fileMenu.add(exitMenuItemSeparator);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        mainMenuBar.add(fileMenu);

        helpMenu.setText("Help");

        aboutMenuItem.setText("About...");
        helpMenu.add(aboutMenuItem);

        mainMenuBar.add(helpMenu);

        toolsMenu.setText("Tools");

        optionsMenuItem.setText("Options...");
        optionsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                optionsMenuItemActionPerformed(evt);
            }
        });
        toolsMenu.add(optionsMenuItem);

        mainMenuBar.add(toolsMenu);

        setJMenuBar(mainMenuBar);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(mainPanelPanel, GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(mainPanelPanel, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void exitMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        this.setVisible(false);
        this.close();
        this.shutdownHook();
        this.dispose();
    }//GEN-LAST:event_exitMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void scanMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_scanMenuItemActionPerformed
        try {
            if(documentController.getScanJobLock().tryLock()) {
                try {
                    this.scan();
                }finally {
                    documentController.getScanJobLock().unlock();
                }
            }else {
                messageHandler.handle(new Message("A scan job is already in progress",
                        JOptionPane.ERROR_MESSAGE,
                        "Scan job already in progress"));
            }
        }catch(Throwable ex) {
            handleUnexpectedException(ex,
                    "Exception occured during scanning",
                    "An unexpected exception occured during scanning: %s");
        }
    }//GEN-LAST:event_scanMenuItemActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void selectScannerMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_selectScannerMenuItemActionPerformed
        ScannerSelectionDialog scannerSelectionDialog;
        try {
            scannerSelectionDialog = new ScannerSelectionDialog(this,
                    issueHandler,
                    this.documentScannerConf,
                    documentController);
        } catch (IOException | SaneException ex) {
            handleException(ex,
                    "Exception during scanner selection",
                    "An unexpected exception during scanner selection occured: %s");
            return;
        }
        scannerSelectionDialog.pack();
        scannerSelectionDialog.setLocationRelativeTo(this);
        scannerSelectionDialog.setVisible(true);//blocks and updates
            //documentScannerConf

        if(this.documentScannerConf.getScannerName() == null) {
            //dialog has been canceled (previously selected dialog remains or
            //null
            return;
        }
        try {
            this.scannerDevice = documentController.getScannerDevice(documentScannerConf.getScannerName(),
                    documentScannerConf.getScannerConfMap(),
                    DocumentScannerConf.SCANNER_SANE_ADDRESS_DEFAULT,
                    documentScannerConf.getResolutionWish());
        } catch (IOException | SaneException ex) {
            issueHandler.handle(new ExceptionMessage(ex));
            return;
        }
        afterScannerSelection();
    }//GEN-LAST:event_selectScannerMenuItemActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageSelectionMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_storageSelectionMenuItemActionPerformed
        StorageSelectionDialog storageSelectionDialog;
        try {
            storageSelectionDialog = new StorageSelectionDialog(this,
                    documentScannerConf,
                    issueHandler,
                    confirmMessageHandler);
        } catch (IOException | StorageConfValidationException | StorageConfPanelCreationException ex) {
            messageHandler.handle(new ExceptionMessage(ex));
            return;
        }
        StorageConf storageConfCopy;
        try {
            storageConfCopy = storageConfCopyFactory.copy(documentScannerConf.getStorageConf());
        } catch (StorageConfCopyException ex) {
            messageHandler.handle(new Message(ex, JOptionPane.ERROR_MESSAGE));
            return;
        }
        storageSelectionDialog.setLocationRelativeTo(this);
        storageSelectionDialog.setVisible(true);
        StorageConf selectedStorageConf = storageSelectionDialog.getSelectedStorageConf();
        if(selectedStorageConf == null) {
            //dialog has been canceled
            return;
        }
        if(storageConfCopy.equals(selectedStorageConf)) {
            LOGGER.info("no changes made to storage configuration");
            return;
        }
        //type of StorageConf changed
        try {
            LOGGER.debug("storage configuration changes, shutting down current storage");
            this.storage.shutdown();
            LOGGER.debug("creating new storage based on changed configuration");
            this.storage = delegatingStorageFactory.create(selectedStorageConf);
            //only set references to new storage and its configuration if
            //successfully changed
            this.documentScannerConf.setStorageConf(selectedStorageConf);
            mainPanel.setStorage(this.storage);
        } catch (StorageCreationException ex) {
            LOGGER.warn("exception during creation of storage after change of "
                    + "storage configuration occured",
                    ex);
            issueHandler.handle(new ExceptionMessage(ex));
        }
    }//GEN-LAST:event_storageSelectionMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void oCRMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_oCRMenuItemActionPerformed
        try {
            DocumentScannerConf documentScannerConf0 = new DocumentScannerConf(documentScannerConf);
            OCREngineSelectDialog oCREngineSelectDialog = new OCREngineSelectDialog(this,
                    documentScannerConf0,
                    this.issueHandler);
            oCREngineSelectDialog.setLocationRelativeTo(this);
            oCREngineSelectDialog.setVisible(true);
            DocumentScannerConf documentScannerConf1 = oCREngineSelectDialog.getDocumentScannerConf();
            if(documentScannerConf1 == null) {
                //dialog canceled
                return;
            }
            this.documentScannerConf = documentScannerConf1;
            this.oCREngine = oCREngineFactory.create(documentScannerConf.getoCREngineConf());
            mainPanel.setoCREngine(oCREngine);
        }catch(Throwable ex) {
            handleUnexpectedException(ex,
                    "Exception during OCR Engine configuration",
                    "An unexpected exception during OCR engine configuration occured: %s");
        }
    }//GEN-LAST:event_oCRMenuItemActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void openMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "PDF files", "pdf");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
        }
        final File selectedFile = chooser.getSelectedFile();
        try {
            List<ImageWrapper> images = DocumentScannerUtils.retrieveImages(selectedFile,
                    this,
                    documentScannerConf.getImageWrapperStorageDir(),
                    issueHandler);
            if(images == null) {
                LOGGER.debug("image retrieval has been canceled, discontinuing adding document");
                return;
            }
            addDocument(new DocumentItem(null, //entityToEdit
                    images,
                    selectedFile));
        } catch (DocumentAddException | InterruptedException | ExecutionException | IOException ex) {
            handleException(ex,
                    "Exception during adding of new document",
                    "An unexpected exception during adding of new document occured: %s");
        }
    }//GEN-LAST:event_openMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void optionsMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_optionsMenuItemActionPerformed
        try {
            DocumentScannerConfDialog documentScannerConfDialog = new DocumentScannerConfDialog(this,
                    documentScannerConf,
                    DocumentScannerUtils.ENTITY_CLASSES,
                    fieldRetriever);
            documentScannerConfDialog.setVisible(true);
        } catch (Throwable ex) {
            handleUnexpectedException(ex, "unexpected exception during preparation of configuration dialog",
                    "An unexpected exception during the preparation of the configuration dialog occured: %s");
        }
    }//GEN-LAST:event_optionsMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void editEntryMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_editEntryMenuItemActionPerformed
        try {
            EntityEditingDialog entityEditingDialog = new EntityEditingDialog(this,
                    DocumentScannerUtils.ENTITY_CLASSES,
                    DocumentScannerUtils.PRIMARY_CLASS_SELECTION,
                    storage,
                    issueHandler,
                    confirmMessageHandler,
                    queryComponentFieldInitializer,
                    entryStorage,
                    fieldRetriever);
            entityEditingDialog.setVisible(true); //blocks
            List<Object> selectedEntities = entityEditingDialog.getSelectedEntities();
            if(selectedEntities.size() > DocumentScannerUtils.SELECTED_ENTITIES_EDIT_WARNING) {
                String answer = confirmMessageHandler.confirm(new Message(
                        String.format("More than %d entities are supposed to be opened for editing. This might take a long time. Continue?",
                                DocumentScannerUtils.SELECTED_ENTITIES_EDIT_WARNING),
                        JOptionPane.QUESTION_MESSAGE,
                        "Open documents?"),
                        YES, NO);
                if(!answer.equals(YES)) {
                    return;
                }
            }
            for(Object selectedEntity : selectedEntities) {
                try {
                    addDocument(selectedEntity);
                } catch (DocumentAddException | IOException ex) {
                    handleException(ex,
                            "Exception during adding of new document",
                            "An unexpected exception during adding of new document occured: %s");
                }
            }
        }catch(Throwable ex) {
            handleUnexpectedException(ex,
                    "Exception during editing of entry occured",
                    "An unexpected exception occured during editing entry: %s");
        }
    }//GEN-LAST:event_editEntryMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void openSelectionMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_openSelectionMenuItemActionPerformed
        try {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                    "PDF files", "pdf");
            chooser.setFileFilter(filter);
            int returnVal = chooser.showOpenDialog(this);
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                return;
            }
            final File selectedFile = chooser.getSelectedFile();
            try {
                List<ImageWrapper> images = DocumentScannerUtils.retrieveImages(selectedFile,
                        this,
                        documentScannerConf.getImageWrapperStorageDir(),
                        issueHandler);
                if(images == null) {
                    LOGGER.debug("image retrieval has been canceled, discontinuing adding document");
                    return;
                }
                if(images.isEmpty()) {
                    LOGGER.debug(String.format("selected file '%s' contained no images",
                            selectedFile.getAbsolutePath()));
                    return;
                }
                documentController.addDocumentJob(images);
                //always open dialog for pages from PDF because they don't take
                //time to open
                ScanResultDialog scanResultDialog = new ScanResultDialog(this,
                        this.documentScannerConf.getPreferredScanResultPanelWidth(),
                        documentController,
                        scannerDevice,
                        this.documentScannerConf.getImageWrapperStorageDir(),
                        javaFXDialogMessageHandler,
                        this, //openDocumentWaitDialogParent
                        new LinkedList<>(mainPanel.getDocumentItems().keySet()),
                        this.documentScannerConf
                );
                scanResultDialog.setLocationRelativeTo(this);
                scanResultDialog.setVisible(true);
                handleScannerResultDialogClosed(scanResultDialog);
            } catch (DocumentAddException | InterruptedException | ExecutionException | IOException ex) {
                handleException(ex,
                        "Exception during adding of new document",
                        "An unexpected exception during adding of new document occured: %s");
            }
        }catch(Throwable ex) {
            handleUnexpectedException(ex,
                    "Exception during opening of scan for selection",
                    "An unexpected exception during opening of scan for selection occured: %s");
        }
    }//GEN-LAST:event_openSelectionMenuItemActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void closeMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_closeMenuItemActionPerformed
        this.mainPanel.removeActiveDocumentItem();
        if(this.mainPanel.getDocumentItemCount() == 0) {
            this.closeMenuItem.setEnabled(false);
            this.exportMenuItem.setEnabled(false);
        }
    }//GEN-LAST:event_closeMenuItemActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void exportMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_exportMenuItemActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setApproveButtonText("Export");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "PDF files", "pdf");
        chooser.setFileFilter(filter);
        boolean success = false;
        File selectedFile = null;
        while(!success) {
            int returnVal = chooser.showOpenDialog(this);
            selectedFile = chooser.getSelectedFile();
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                return;
            }
            if(!selectedFile.getName().endsWith(".pdf")) {
                selectedFile = new File(String.format("%s.pdf",
                        selectedFile.getAbsolutePath()));
            }
            if(selectedFile.exists()) {
                String answer = confirmMessageHandler.confirm(new Message(
                        String.format("selected file '%s' exists. Overwrite?",
                                selectedFile.getName()),
                        JOptionPane.WARNING_MESSAGE,
                        "File exists"),
                        YES, NO);
                success = answer.equals(YES);
            }else {
                success = true;
            }
        }
        assert selectedFile != null; //if dialog is canceled, method returns
        try {
            this.mainPanel.exportActiveDocumentItem(selectedFile,
                    DocumentExportFormat.EXPORT_FORMAT_PDF);
        } catch (IOException | ImageWrapperException ex) {
            messageHandler.handle(new Message(ex,
                    JOptionPane.ERROR_MESSAGE));
        }
    }//GEN-LAST:event_exportMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void valueDetectionMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_valueDetectionMenuItemActionPerformed
        try {
            ValueDetectionServiceConfDialog serviceConfDialog = new ValueDetectionServiceConfDialog(this,
                    documentScannerConf.getAvailableValueDetectionServiceConfs(),
                    documentScannerConf.getSelectedValueDetectionServiceConfs(),
                    documentScannerConf.getValueDetectionServiceJARPathMapping(),
                    issueHandler);
            serviceConfDialog.setLocationRelativeTo(this);
            serviceConfDialog.setVisible(true);
            List<ValueDetectionServiceConf> availalbleValueDetectionServiceConfs = serviceConfDialog.getAvailableValueDetectionServiceConfs();
            if(availalbleValueDetectionServiceConfs == null) {
                //dialog canceled
                return;
            }
            this.documentScannerConf.setAvailableValueDetectionServiceConfs(availalbleValueDetectionServiceConfs);
            this.documentScannerConf.setSelectedValueDetectionServiceConfs(serviceConfDialog.getSelectedValueDetectionServiceConfs());
            this.documentScannerConf.getValueDetectionServiceJARPathMapping()
                    .putAll(serviceConfDialog.getValueDetectionServiceJARPaths());
            //delete removed 3rd-party implementations
            List<Class<? extends ValueDetectionServiceConf>> allServices = new LinkedList<>();
            allServices.addAll(serviceConfDialog.getAvailableValueDetectionServiceConfs().stream()
                    .map(serviceConf -> serviceConf.getClass())
                    .collect(Collectors.toList()));
            allServices.addAll(serviceConfDialog.getSelectedValueDetectionServiceConfs().stream()
                    .map(serviceConf -> serviceConf.getClass())
                    .collect(Collectors.toList()));
            Set<Class<? extends ValueDetectionServiceConf>> toRemoves = new HashSet<>();
            for(Class<? extends ValueDetectionServiceConf> serviceConfClass : this.documentScannerConf.getValueDetectionServiceJARPathMapping().keySet()) {
                if(!allServices.contains(serviceConfClass)) {
                    assert this.documentScannerConf.getValueDetectionServiceJARPathMapping().get(serviceConfClass) != null:
                            "built-in service ought not to be removed from runtime classloading mapping";
                    toRemoves.add(serviceConfClass);
                }
            }
            for(Class<? extends ValueDetectionServiceConf> toRemove : toRemoves) {
                this.documentScannerConf.getValueDetectionServiceJARPathMapping().remove(toRemove);
            }
            this.documentScannerConf.setValueDetectionServiceJARPaths(new HashSet<>(this.documentScannerConf.getValueDetectionServiceJARPathMapping().values()));
            //apply changes
            this.valueDetectionServiceController.applyValueDetectionServiceSelection();
        }catch(Throwable ex) {
            handleUnexpectedException(ex,
                    "Exception during value detection service configuration",
                    "An unexpected exception during value detection configuration occured: %s");
        }
    }//GEN-LAST:event_valueDetectionMenuItemActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void scanResultsMenuItemActionPerformed(ActionEvent evt) {//GEN-FIRST:event_scanResultsMenuItemActionPerformed
        try {
            try {
                ScanResultDialog scanResultDialog = new ScanResultDialog(this,
                        this.documentScannerConf.getPreferredScanResultPanelWidth(),
                        documentController,
                        scannerDevice,
                        this.documentScannerConf.getImageWrapperStorageDir(),
                        javaFXDialogMessageHandler,
                        this, //openDocumentWaitDialogParent
                        new LinkedList<>(mainPanel.getDocumentItems().keySet()),
                        this.documentScannerConf
                );
                scanResultDialog.setLocationRelativeTo(this);
                scanResultDialog.setVisible(true);
                handleScannerResultDialogClosed(scanResultDialog);
            } catch (IOException | DocumentAddException ex) {
                messageHandler.handle(new ExceptionMessage(ex));
            }
        }catch(Throwable ex) {
            handleUnexpectedException(ex,
                    "Exception during scanning",
                    "Unexpected exception during scanning occured: %s");
        }
    }//GEN-LAST:event_scanResultsMenuItemActionPerformed

    private void addDocument(DocumentItem documentItem) throws DocumentAddException, IOException {
        //wait as long as possible
        if(amountMoneyExchangeRetrieverInitThread.isAlive()) {
            try {
                amountMoneyExchangeRetrieverInitThread.join();
            } catch (InterruptedException ex) {
                LOGGER.error("unexpected exception during waiting for exchange rate retriever initialization",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                return;
            }
        }
        this.mainPanel.addDocumentItem(documentItem);
        closeMenuItem.setEnabled(true);
        exportMenuItem.setEnabled(true);
    }

    private void addDocument(Object entityToEdit) throws DocumentAddException, IOException {
        //wait as long as possible
        if(amountMoneyExchangeRetrieverInitThread.isAlive()) {
            try {
                amountMoneyExchangeRetrieverInitThread.join();
            } catch (InterruptedException ex) {
                LOGGER.error("unexpected exception during waiting for exchange rate retriever initialization",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                return;
            }
        }
        this.mainPanel.addDocumentItem(new DocumentItem(entityToEdit,
                null, //images
                null //selectedFile
        ));
        closeMenuItem.setEnabled(true);
        exportMenuItem.setEnabled(true);
    }

    /**
     * Determines the document source used on the scanner device and whether
     * everything ought to be scanned or a selection of pages.
     *
     * @param documentController the document controller handling future jobs
     * @param scannerDevice the scanner device to check
     * @param dialogParent the parent window of the dialog
     * @param documentScannerConf the document scanner configuration to read
     * @param issueHandler the issue handler to use
     * @return a pair with the selected document source and the requested page count
     * @throws IOException if an exception occurs during the communication with the scanner
     * @throws DocumentSourceOptionMissingException if the scanner doesn't support the viable document source option
     * @throws SaneException if an exception occurs during the communication with the scanner
     */
    public static Pair<DocumentSource, Integer> determineDocumentSource(DocumentController documentController,
            SaneDevice scannerDevice,
            Window dialogParent,
            DocumentScannerConf documentScannerConf,
            IssueHandler issueHandler) throws IOException,
            DocumentSourceOptionMissingException,
            SaneException {
        try {
            documentController.openScannerDevice(scannerDevice,
                    documentScannerConf.getScannerOpenWaitTime(),
                    documentScannerConf.getScannerOpenWaitTimeUnit());
        }catch(DeviceOpeningAlreadyInProgressException ex) {
            issueHandler.handle(new Message(String.format("The "
                    + "opening of device '%s' is already in progress. "
                    + "The opening might be in a loop/deadlock state "
                    + "which could be fixed by de- and reconnecting "
                    + "the device from your computer, restarting the "
                    + "device, restarting %s or rebooting your "
                    + "computer",
                            documentScannerConf.getScannerName(),
                            DocumentScannerUtils.APP_NAME),
                    JOptionPane.ERROR_MESSAGE,
                    "Opening of scanner device already in progress"));
            return null;
        }catch(InterruptedException ex) {
            LOGGER.error("Unexpected exception during scanning occured",
                    ex);
            issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        }catch(TimeoutException ex) {
            issueHandler.handle(new Message(String.format("Opening "
                    + "the scanner device '%s' timed out after %d %s, "
                    + "can't proceed. Consider increasing the timeout "
                    + "value in options",
                            documentScannerConf.getScannerName(),
                            documentScannerConf.getScannerOpenWaitTime(),
                            documentScannerConf.getScannerOpenWaitTimeUnit()),
                    JOptionPane.ERROR_MESSAGE,
                    "Opening scanner device timed out"));
            return null;
        }
        if(scannerDevice.getOption(ScannerEditDialog.DOCUMENT_SOURCE_OPTION_NAME) == null) {
            //an exception is thrown in order to allow data to be reported
            //through BugHandler
            throw new DocumentSourceOptionMissingException(scannerDevice);
        }
        DocumentSource configuredDocumentSource = documentController.getDocumentSourceEnum(scannerDevice);
        DocumentSource selectedDocumentSource;
        final ScannerPageSelectDialog scannerPageSelectDialog;
        if(configuredDocumentSource != DocumentSource.UNKNOWN) {
            scannerPageSelectDialog = new ScannerPageSelectDialog(dialogParent,
                    configuredDocumentSource);
            scannerPageSelectDialog.setVisible(true);
            selectedDocumentSource = scannerPageSelectDialog.getSelectedDocumentSource();
        }else {
            scannerPageSelectDialog = null;
            selectedDocumentSource = DocumentSource.UNKNOWN;
        }
        if(selectedDocumentSource == null) {
            return null;
        }
        return new ImmutablePair<>(selectedDocumentSource,
                scannerPageSelectDialog == null || scannerPageSelectDialog.isScanAll()
                        ? null
                        : scannerPageSelectDialog.getPageCount());
    }

    /**
     * How to select scan source? There might be inimaginable scan sources
     * besides flatbed, ADF and duplex ADF which will be configurable in the
     * scanner edit dialog. If those are configured, there can't be any dialog
     * displayed because we can't know about it's configuration parameters (and
     * we'll just call {@link SaneDevice#acquireImage() }).
     *
     * If no unknown source is configured in scanner edit dialog, we open a
     * {@link ScannerPageSelectDialog} which allows to configure the following
     * scan for flatbed, ADF and duplex ADF (with the setting configured in the
     * scanner edit dialog as initial values).
     *
     * A {@link ScanResultDialog} is always opened - even for one page
     * flatbed scan results since it allows to scan more.
     *
     * @return the retrieved images or {@code null} if the wait dialog has been
     *     canceled
     * @throws DocumentSourceOptionMissingException if the scanner doesn't support the viable document source option
     */
    private void scan() throws DocumentSourceOptionMissingException {
        assert this.scannerDevice != null;
        try {
            Pair<DocumentSource, Integer> documentSourcePair = determineDocumentSource(this.documentController,
                    scannerDevice,
                    this,
                    this.documentScannerConf,
                    this.issueHandler);
            if(documentSourcePair == null) {
                //dialog in determineDocumentSource has been aborted
                return;
            }
            ScanJob scanJob = documentController.addScanJob(documentController,
                    scannerDevice,
                    documentSourcePair.getKey(),
                    this.documentScannerConf.getImageWrapperStorageDir(),
                    documentSourcePair.getValue(),
                    issueHandler);
            ScanJobFinishCallback scanJobFinishCallback = imagesUnmodifiable -> {
                SwingUtilities.invokeLater(() -> {
                    try {
                        if (imagesUnmodifiable == null) {
                            //canceled or exception which has been handled inside retrieveImages
                            return;
                        }
                        if(imagesUnmodifiable.isEmpty()) {
                            LOGGER.debug("scanner returned no pages and no error");
                            return;
                        }
                        LOGGER.debug(String.format("scanned %d pages", imagesUnmodifiable.size()));
                        if(mainPanel.getDocumentItemCount() == 0) {
                            //Only open dialog if there's no open document in order to
                            //avoid disturbing entering of data. The new scan is
                            //available in the dialog which can be opened manually.
                            ScanResultDialog scanResultDialog = new ScanResultDialog(this,
                                    documentScannerConf.getPreferredScanResultPanelWidth(),
                                    documentController,
                                    scannerDevice,
                                    documentScannerConf.getImageWrapperStorageDir(),
                                    javaFXDialogMessageHandler,
                                    this, //openDocumentWaitDialogParent
                                    new LinkedList<>(mainPanel.getDocumentItems().keySet()),
                                    documentScannerConf
                            );
                            scanResultDialog.setLocationRelativeTo(this);
                            scanResultDialog.setVisible(true);
                            handleScannerResultDialogClosed(scanResultDialog);
                        }
                    }catch(DocumentAddException
                            | IOException
                            | ImageWrapperException ex) {
                        messageHandler.handle(new Message(ex));
                    }
                });
            };
            scanJob.setFinishCallback(scanJobFinishCallback);
                //callback can be set safely until thread or other
                //executor isn't started
            Thread scanJobThread = new Thread(scanJob,
                    String.format("scan-job-thread-%d",
                            documentController.getDocumentJobCount().intValue()+1));
            scanJobThread.start();
        } catch (SaneException | IOException | IllegalArgumentException ex) {
            this.handleException(ex,
                    "Exception during scanning",
                    "An unexpected exception during scanning occured: %s");
        }
        //Don't call device.close because it resets all options
    }

    /**
     * Handles logging and user feedback of {@code ex}.
     * @param ex the exception to handle
     * @param title the title property passed to the {@link MessageHandler}
     * @param text an optional text to explain in which situation the exception
     *     occurred (can be {@code null} in which case a not very helpful default
     *     text like "The following exception occured:" will be chosen). It should
     *     end with {@code : %s} so that adding the exception cause message makes
     *     sense.
     */
    private void handleException(Throwable ex,
            String title,
            String text) {
        LOGGER.info("The following exception occured and should be handled by the user",
                ex);
        handleException0(ex,
                title,
                text);
    }

    private void handleException0(Throwable ex,
            String title,
            String text) {
        this.messageHandler.handle(new Message(String.format(text != null ? text : "The following exception occured: %s", ExceptionUtils.getRootCauseMessage(ex)),
                JOptionPane.ERROR_MESSAGE,
                title));
    }

    private void handleUnexpectedException(Throwable ex,
            String title,
            String text) {
        LOGGER.error("The following unexpected exception occured which causes the application to shut down",
                ex);
        handleException0(ex,
                title,
                text);
        this.issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        this.dispose();
            //- there's few sense to leave the application running after an
            //unexpected exception which the user has been informed about
            //- doesn't run shutdown hooks
        this.shutdownHook();
    }

    private void handleScannerResultDialogClosed(ScanResultDialog scanResultDialog) throws DocumentAddException,
            IOException,
            ImageWrapperException {
        if(this.documentScannerConf.isRememberPreferredScanResultPanelWidth()) {
            this.documentScannerConf.setPreferredScanResultPanelWidth(scanResultDialog.getPanelWidth());
        }
        List<DocumentItem> dialogResult = scanResultDialog.getSortedDocuments();
        if(dialogResult == null) {
            //dialog canceled
            return;
        }
        for(DocumentItem scannerResult : scanResultDialog.getSortedDocuments()) {
            if(!mainPanel.getDocumentItems().keySet().contains(scannerResult)) {
                addDocument(scannerResult);
            }else {
                OCRSelectComponent oCRSelectComponent = mainPanel.getDocumentItems().get(scannerResult);
                assert oCRSelectComponent != null;
                if(!scannerResult.getImages().equals(oCRSelectComponent.getoCRSelectPanelPanel().getoCRSelectPanels().stream()
                                .map(panel -> panel.getImage())
                                .collect(Collectors.toList()))) {
                    List<OCRSelectPanel> newOCRSelectPanels = new LinkedList<>();
                        //create a new list and overwrite the property in order
                        //to allow DefaultOCRSelectPanelPanel.getoCRSelectPanels
                        //to remain unmodifiable
                    for(ImageWrapper imageWrapper : scannerResult.getImages()) {
                        newOCRSelectPanels.add(new DefaultOCRSelectPanel(imageWrapper,
                                documentScannerConf.getPreferredOCRSelectPanelWidth(),
                                issueHandler));
                    }
                    oCRSelectComponent.getoCRSelectPanelPanel().updateoCRSelectPanels(newOCRSelectPanels);
                    oCRSelectComponent.getoCRSelectPanelPanel().validate();
                }
            }
        }
        //this.validate(); //not necessary
    }

    /**
     * This methods handles the fact that before checking
     * {@link DocumentScannerConf#isSkipUserAllowedAutoBugTrackingQuestion} the
     * user needs to be asked separately whether he_she allows upload of an
     * occured exception.
     *
     * @param ex the exception to handle
     */
    private static void handleExceptionInEventQueueThread(Throwable ex) {
        ConfirmMessageHandler confirmMessageHandler = new DialogConfirmMessageHandler(null, //parent
                "", //titlePrefix
                generateTitleSuffix() //titleSuffix
        );
        String answer = confirmMessageHandler.confirm(new Message(String.format(
                "The error '%s' occured. Do you allow anonymous upload of "
                        + "information about the reason for issue to the"
                        + " developers?",
                        ExceptionUtils.getRootCauseMessage(ex)),
                JOptionPane.QUESTION_MESSAGE,
                "Anoymous contribution"),
                YES,
                NO);
        BugHandler bugHandler;
        if(answer == null //if dialog has been canceled
                || !answer.equals(YES)) {
            //still display the error message with the complete stacktrace
            bugHandler = new DialogBugHandler(null, //parent
                    DocumentScannerUtils.BUG_URL,
                    "", //titlePrefix
                    generateTitleSuffix() //titleSuffix
            );
        }else {
            bugHandler = new RavenBugHandler(RAVEN_DSN);
        }
        bugHandler.handleUnexpectedException(new ExceptionMessage(ex));
        shutdownHookThreads();
    }

    public IssueHandler getIssueHandler() {
        return issueHandler;
    }

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("PMD.DoNotCallSystemExit")
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException
                | InstantiationException
                | IllegalAccessException
                | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DocumentScanner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        EventQueue.invokeLater(new Runnable() {
            @Override
            @SuppressWarnings("PMD.AvoidCatchingThrowable")
            public void run() {
                DocumentScanner documentScanner = null;
                DocumentScannerConf documentScannerConf = null;
                MessageHandler messageHandler = new DialogMessageHandler(null //parent
                        );
                try {
                    assert DocumentScannerConf.HOME_DIR.exists();
                    documentScannerConf = new DocumentScannerConf();
                    //read once for configFile parameter...
                    new JCommander(documentScannerConf, args);

                    if(documentScannerConf.getConfigFile().exists()) {
                        //...then for value detection service JARs to load...
                        XStream xStream = new XStream();
                        xStream.ignoreUnknownElements();
                            //doesn't avoid com.thoughtworks.xstream.mapper.CannotResolveClassException
                        xStream.omitField(DocumentScannerConf.class, "availableValueDetectionServiceConfs");
                        xStream.omitField(DocumentScannerConf.class, "selectedValueDetectionServiceConfs");
                        xStream.omitField(DocumentScannerConf.class, "valueDetectionServiceJARPathMapping");
                        try {
                            documentScannerConf = (DocumentScannerConf)xStream.fromXML(Files.newInputStream(documentScannerConf.getConfigFile().toPath()));
                        } catch (FileNotFoundException ex) {
                            messageHandler.handle(new ExceptionMessage(ex));
                            return;
                        }
                        List<URL> classLoaderURLs = new LinkedList<>();
                        for(String valueDetectionServiceJARPath : documentScannerConf.getValueDetectionServiceJARPaths()) {
                            File valueDetectionServiceJARFile = new File(valueDetectionServiceJARPath);
                            classLoaderURLs.add(valueDetectionServiceJARFile.toURI().toURL());
                        }
                        URLClassLoader classLoader = new URLClassLoader(classLoaderURLs.toArray(new URL[0]),
                                Thread.currentThread().getContextClassLoader()
                                    //System.class.getClassLoader doesn't work
                        );
                        //...then read config file
                        xStream = new XStream();
                        xStream.setClassLoader(classLoader);
                        documentScannerConf = (DocumentScannerConf)xStream.fromXML(Files.newInputStream(documentScannerConf.getConfigFile().toPath()));
                    }else {
                        documentScannerConf = new DocumentScannerConf();
                        LOGGER.info("no previous configuration found in configuration directry '{}', using default values", documentScannerConf.getConfigFile().getAbsolutePath());
                        //new configuration will be persisted in shutdownHook
                    }
                    //...and override value from command line
                    new JCommander(documentScannerConf, args);

                    documentScanner = new DocumentScanner(documentScannerConf);
                    documentScanner.init();
                    documentScanner.setVisible(true); //all shutdown and
                        //resource closing routines need to be handled in event
                        //handling and shutdown hooks since this doesn't block
                        //and there's no way of acchieving that without trouble
                } catch(ImageWrapperStorageDirExistsException ex) {
                    LOGGER.warn("aborting application start because user wants "
                            + "to investigate existing files in image storage "
                            + "directory");
                    handleDocumentScannerShutdown(documentScanner);
                } catch(StorageConfValidationException | StorageCreationException ex) {
                    LOGGER.error("An unexpected exception during initialization of storage occured, see nested exception for details", ex);
                    messageHandler.handle(new Message(ex,
                            JOptionPane.ERROR_MESSAGE));
                    handleDocumentScannerShutdown(documentScanner);
                } catch(FieldOrderValidationException ex) {
                    String message = String.format("The underlying entity model "
                            + "used for storage contains errors: %s",
                            ExceptionUtils.getRootCauseMessage(ex));
                    LOGGER.error(message,
                            ex);
                    messageHandler.handle(new Message(message,
                            JOptionPane.ERROR_MESSAGE,
                            "Entity model validation error"));
                    handleDocumentScannerShutdown(documentScanner);
                } catch(DocumentScannerConfValidationException ex) {
                    LOGGER.error("An unexpected exception during validation of configurationo occured, see nested excception for details",
                            ex);
                    messageHandler.handle(new ExceptionMessage(ex));
                    handleDocumentScannerShutdown(documentScanner);
                } catch(Throwable ex) {
                    LOGGER.error("An unexpected exception occured, see nested exception for details", ex);
                    if(documentScanner != null
                            && documentScanner.getIssueHandler() != null) {
                        documentScanner.getIssueHandler().handleUnexpectedException(new ExceptionMessage(ex));
                    }else {
                        handleExceptionInEventQueueThread(ex);
                    }
                    handleDocumentScannerShutdown(documentScanner);
                    System.exit(SYSTEM_EXIT_ERROR_GENERAL);
                        //calling System.exit is the only way to be able to
                        //close DocumentScanner resources which have been
                        //initialized in the constructor after an exception in
                        //the constructor occured
                }
            }
        });
    }

    private static void handleDocumentScannerShutdown(DocumentScanner documentScanner) {
        if(documentScanner != null) {
            documentScanner.setVisible(false);
            documentScanner.close();
            documentScanner.shutdownHook();
            documentScanner.dispose();
        }else {
            //is DocumentScanner instance isn't available at least threads with
            //static shutdown methods can be shut down
            shutdownHookThreads();
        }
    }
}
