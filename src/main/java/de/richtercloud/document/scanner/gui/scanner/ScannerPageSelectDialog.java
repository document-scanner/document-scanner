/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui.scanner;

import de.richtercloud.document.scanner.gui.DocumentScanner;
import de.richtercloud.document.scanner.gui.DocumentScannerUtils;
import java.awt.Window;
import java.util.Arrays;
import javax.swing.*;

/*
internal implementation notes:
- There's no need for a function to fail if a page count has been specified
which is higher than the pages available in the ADF because there's always the
possibility to add more scanned pages.
*/
/**
 *
 * @author richter
 */
public class ScannerPageSelectDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    /**
     * The selected scan source. {@code null} indicates that the dialog has been
     * canceled.
     */
    private DocumentSource selectedDocumentSource;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final ButtonGroup aDFButtonGroup = new ButtonGroup();
    private final JRadioButton aDFRadioButton = new JRadioButton();
    private final JRadioButton allRadioButton = new JRadioButton();
    private final JButton cancelButton = new JButton();
    private final JCheckBox duplexCheckBox = new JCheckBox();
    private final JRadioButton flatbedRadioButton = new JRadioButton();
    private final JLabel jLabel1 = new JLabel();
    private final JLabel label = new JLabel();
    private final JRadioButton numberRadioButton = new JRadioButton();
    private final JSpinner pageCountSpinner = new JSpinner();
    private final JButton scanButton = new JButton();
    private final ButtonGroup scanSourceButtonGroup = new ButtonGroup();
    // End of variables declaration//GEN-END:variables

    public ScannerPageSelectDialog(Window parent,
            DocumentSource initialDocumentSource) {
        super(parent,
                DocumentScanner.generateApplicationWindowTitle("Select pages to scan",
                        DocumentScannerUtils.APP_NAME,
                        DocumentScannerUtils.APP_VERSION),
                ModalityType.APPLICATION_MODAL //modal
        );
        if(initialDocumentSource == null) {
            throw new IllegalArgumentException("initialScanSource mustn't be null");
        }
        initComponents();
        aDFButtonGroup.add(allRadioButton);
        aDFButtonGroup.add(numberRadioButton);
        scanSourceButtonGroup.add(flatbedRadioButton);
        scanSourceButtonGroup.add(aDFRadioButton);
        flatbedRadioButton.addActionListener((event) -> {
            toggleADFButtonGroupComponents(false);
            //can't use aDFRadioButton.isEnabled because it's still enabled when
            //this ActionListener is invoked
        });
        aDFRadioButton.addActionListener((event) -> {
            toggleADFButtonGroupComponents(true);
        });
        if(initialDocumentSource == DocumentSource.FLATBED) {
            flatbedRadioButton.setSelected(true);
                //doesn't invoked action listener
            toggleADFButtonGroupComponents(false);
        }else if(initialDocumentSource == DocumentSource.ADF || initialDocumentSource == DocumentSource.ADF_DUPLEX) {
            //ADF or duplex ADF
            aDFRadioButton.setSelected(true);
            toggleADFButtonGroupComponents(true);
            if(initialDocumentSource == DocumentSource.ADF_DUPLEX) {
                duplexCheckBox.setSelected(true);
            }
        }else {
            throw new IllegalArgumentException(String.format(
                    "initialDocumentSource has to be one of %s",
                    Arrays.toString(DocumentSource.values())));
        }
    }

    private void toggleADFButtonGroupComponents(boolean enabled) {
        label.setEnabled(enabled);
        allRadioButton.setEnabled(enabled);
        duplexCheckBox.setEnabled(enabled);
        numberRadioButton.setEnabled(enabled);
            //should trigger enabling of components below numberRadioButton
    }

    public DocumentSource getSelectedDocumentSource() {
        return selectedDocumentSource;
    }

    public boolean isScanAll() {
        return allRadioButton.isSelected();
    }

    public int getPageCount() {
        return (int) pageCountSpinner.getValue();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        label.setText("How many pages ought to be scanned?");
        label.setEnabled(false);

        allRadioButton.setSelected(true);
        allRadioButton.setText("All in ADF");
        allRadioButton.setEnabled(false);
        allRadioButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allRadioButtonActionPerformed(evt);
            }
        });

        numberRadioButton.setText("Number of pages:");
        numberRadioButton.setEnabled(false);
        numberRadioButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numberRadioButtonActionPerformed(evt);
            }
        });

        pageCountSpinner.setModel(new SpinnerNumberModel(1, 1, null, 1));
        pageCountSpinner.setEnabled(false);

        scanButton.setText("Scan");
        scanButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scanButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("<html><body style=\"white-space:pre-line; width=10\"><p>If more pages are available in the ADF the scanner might remove them by emptying the ADF</p></body></html>");
        jLabel1.setVerticalAlignment(SwingConstants.TOP);
        jLabel1.setEnabled(false);
        jLabel1.setVerticalTextPosition(SwingConstants.TOP);

        flatbedRadioButton.setSelected(true);
        flatbedRadioButton.setText("Scan from flatbed");

        aDFRadioButton.setText("Scan from Automated Document Feeder (ADF)");

        duplexCheckBox.setText("Duplex");
        duplexCheckBox.setEnabled(false);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(duplexCheckBox)
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(cancelButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scanButton))
                            .addComponent(flatbedRadioButton)
                            .addComponent(aDFRadioButton)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(allRadioButton)
                                    .addComponent(label)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(numberRadioButton)
                                        .addGap(18, 18, 18)
                                        .addComponent(pageCountSpinner, GroupLayout.PREFERRED_SIZE, 607, GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(flatbedRadioButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(aDFRadioButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(duplexCheckBox)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(allRadioButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(numberRadioButton)
                    .addComponent(pageCountSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(scanButton)
                    .addComponent(cancelButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void allRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allRadioButtonActionPerformed
        pageCountSpinner.setEnabled(false);
    }//GEN-LAST:event_allRadioButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void numberRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numberRadioButtonActionPerformed
        pageCountSpinner.setEnabled(true);
    }//GEN-LAST:event_numberRadioButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_cancelButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void scanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scanButtonActionPerformed
        if(flatbedRadioButton.isSelected()) {
            this.selectedDocumentSource = DocumentSource.FLATBED;
        }else {
            //aDFRadioButton selected
            if(duplexCheckBox.isSelected()) {
                this.selectedDocumentSource = DocumentSource.ADF_DUPLEX;
            }else {
                this.selectedDocumentSource = DocumentSource.ADF;
            }
        }
        this.setVisible(false);
    }//GEN-LAST:event_scanButtonActionPerformed
}
