/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui.storageconf;

import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.message.handler.ConfirmMessageHandler;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.message.handler.Message;
import de.richtercloud.reflection.form.builder.storage.StorageConf;
import de.richtercloud.reflection.form.builder.storage.StorageConfValidationException;
import java.awt.Component;
import java.awt.Window;
import java.io.IOException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Allows selection of storage implementation. Caller is responsible for
 * evaluating the selection.
 * @author richter
 */
public class StorageSelectionDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final DefaultListModel<StorageConf> storageListModel = new DefaultListModel<>();
    private final ListCellRenderer storageListCellRenderer = new DefaultListCellRenderer() {
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            assert value instanceof StorageConf;
            StorageConf valueCast = (StorageConf) value;
            return super.getListCellRendererComponent(list,
                    valueCast.getShortDescription(),
                    index,
                    isSelected,
                    cellHasFocus);
        }
    };
    private final DocumentScannerConf documentScannerConf;
    /**
     * Reference to the selected storage configuration after closing.
     * {@code null} indicates that the selection has been aborted/hasn't
     * changed. Keeping an extra reference instead of just selecting from
     * {@code storageList} avoids possible trouble after dialog has been
     * disposed.
     */
    private StorageConf selectedStorageConf;
    private final IssueHandler issueHandler;
    private final StorageConfPanelFactory storageConfPanelFactory;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JButton storageDialogCancelButton = new JButton();
    private final JButton storageDialogDeleteButton = new JButton();
    private final JButton storageDialogEditButton = new JButton();
    private final JButton storageDialogNewButton = new JButton();
    private final JButton storageDialogSelectButton = new JButton();
    private final JLabel storageLabel = new JLabel();
    private final JList<StorageConf> storageList = new JList<>();
    private final JScrollPane storageListScrollPane = new JScrollPane();
    // End of variables declaration//GEN-END:variables

    public StorageSelectionDialog(Window parent,
            DocumentScannerConf documentScannerConf,
            IssueHandler issueHandler,
            ConfirmMessageHandler confirmMessageHandler) throws IOException, StorageConfValidationException, StorageConfPanelCreationException {
        super(parent,
                ModalityType.APPLICATION_MODAL //modalityType
        );
        this.documentScannerConf = documentScannerConf;
        this.issueHandler = issueHandler;

        this.storageConfPanelFactory = new DefaultStorageConfPanelFactory(issueHandler,
                confirmMessageHandler,
                documentScannerConf.isSkipMD5SumCheck(),
                documentScannerConf);
        for(StorageConf availableStorageConf : this.documentScannerConf.getAvailableStorageConfs()) {
            this.storageListModel.addElement(availableStorageConf);
        }
        initComponents();
        this.storageList.setCellRenderer(storageListCellRenderer);
            //after initComponents
        this.storageList.setSelectedValue(this.documentScannerConf.getStorageConf(),
                true //shouldScroll
        );
        this.storageList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                StorageSelectionDialog.this.storageDialogSelectButton.setEnabled(StorageSelectionDialog.this.storageListModel.getSize() > 0
                        && StorageSelectionDialog.this.storageList.getSelectedIndices().length > 0);
            }
        });
    }

    public StorageConf getSelectedStorageConf() {
        return selectedStorageConf;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        storageLabel.setText("Storages");

        storageList.setModel(storageListModel);
        storageListScrollPane.setViewportView(storageList);

        storageDialogCancelButton.setText("Cancel");
        storageDialogCancelButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storageDialogCancelButtonActionPerformed(evt);
            }
        });

        storageDialogSelectButton.setText("Select");
        storageDialogSelectButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storageDialogSelectButtonActionPerformed(evt);
            }
        });

        storageDialogEditButton.setText("Edit");
        storageDialogEditButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storageDialogEditButtonActionPerformed(evt);
            }
        });

        storageDialogDeleteButton.setText("Delete");

        storageDialogNewButton.setText("New...");
        storageDialogNewButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storageDialogNewButtonActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(storageLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(storageListScrollPane, GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(storageDialogSelectButton)))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(storageDialogCancelButton, GroupLayout.Alignment.TRAILING)
                            .addComponent(storageDialogNewButton, GroupLayout.Alignment.TRAILING)
                            .addGroup(GroupLayout.Alignment.TRAILING, layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(storageDialogEditButton, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(storageDialogDeleteButton, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(storageLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(storageListScrollPane, GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(storageDialogCancelButton)
                            .addComponent(storageDialogSelectButton)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(storageDialogNewButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(storageDialogEditButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(storageDialogDeleteButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageDialogCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storageDialogCancelButtonActionPerformed
        this.selectedStorageConf = null;
            //is fine to set null because the value in the dialog doesn't affect
            //the configuration (has to be handled by caller)
        this.setVisible(false);
    }//GEN-LAST:event_storageDialogCancelButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageDialogSelectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storageDialogSelectButtonActionPerformed
        this.selectedStorageConf = this.storageList.getSelectedValue();
        assert this.selectedStorageConf != null;
        try {
            this.selectedStorageConf.validate();
                //some configurations, e.g. DerbyNetworkPersistenceStorageConf
                //can't be created in a valid state because they require values
                //to be set (e.g. a password)
        } catch (StorageConfValidationException ex) {
            issueHandler.handle(new Message(ex, JOptionPane.ERROR_MESSAGE));
            return;
        }
        this.setVisible(false);
    }//GEN-LAST:event_storageDialogSelectButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageDialogNewButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storageDialogNewButtonActionPerformed
        StorageCreateDialog storageCreateDialog = new StorageCreateDialog(this,
                storageConfPanelFactory,
                issueHandler);
        storageCreateDialog.setLocationRelativeTo(this);
        storageCreateDialog.setVisible(true);
        StorageConf createdStorageConf = storageCreateDialog.getCreatedStorageConf();
        this.storageListModel.addElement(createdStorageConf);
        this.documentScannerConf.getAvailableStorageConfs().add(createdStorageConf);
    }//GEN-LAST:event_storageDialogNewButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageDialogEditButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storageDialogEditButtonActionPerformed
        Object storageListSelectedValue = storageList.getSelectedValue();
        assert storageListSelectedValue instanceof StorageConf;
        StorageConf selectedStorageConf = (StorageConf) storageListSelectedValue;
        StorageConfPanel storageConfPanel;
        try {
            storageConfPanel = this.storageConfPanelFactory.create(selectedStorageConf);
        } catch (StorageConfPanelCreationException ex) {
            issueHandler.handle(new ExceptionMessage(ex));
            return;
        }
        assert storageConfPanel != null;
        StorageEditDialog storageEditDialog = new StorageEditDialog(this,
                storageConfPanel,
                issueHandler);
        storageEditDialog.setLocationRelativeTo(this);
        storageEditDialog.setVisible(true);
        StorageConf editedStorageConf = storageEditDialog.getEditedStorageConf();
        if(editedStorageConf == null) {
            //dialog canceled
            return;
        }
        //
        int index = this.storageListModel.indexOf(storageListSelectedValue);
        this.storageListModel.remove(index);
        this.storageListModel.add(index, editedStorageConf);
        this.storageList.setSelectedIndex(index);
        //documentScannerConf
        this.documentScannerConf.getAvailableStorageConfs().remove(storageListSelectedValue);
        this.documentScannerConf.getAvailableStorageConfs().add(editedStorageConf);
        this.selectedStorageConf = editedStorageConf;
    }//GEN-LAST:event_storageDialogEditButtonActionPerformed
}
