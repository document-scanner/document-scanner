/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import com.google.common.collect.ImmutableSet;
import de.richtercloud.document.scanner.ifaces.DocumentAddException;
import de.richtercloud.document.scanner.ifaces.ImageWrapper;
import de.richtercloud.document.scanner.model.APackage;
import de.richtercloud.document.scanner.model.Address;
import de.richtercloud.document.scanner.model.Bill;
import de.richtercloud.document.scanner.model.Company;
import de.richtercloud.document.scanner.model.Document;
import de.richtercloud.document.scanner.model.Email;
import de.richtercloud.document.scanner.model.EmailAddress;
import de.richtercloud.document.scanner.model.Employment;
import de.richtercloud.document.scanner.model.FinanceAccount;
import de.richtercloud.document.scanner.model.Leaflet;
import de.richtercloud.document.scanner.model.Location;
import de.richtercloud.document.scanner.model.Payment;
import de.richtercloud.document.scanner.model.Person;
import de.richtercloud.document.scanner.model.Shipping;
import de.richtercloud.document.scanner.model.TelephoneCall;
import de.richtercloud.document.scanner.model.TelephoneNumber;
import de.richtercloud.document.scanner.model.Transport;
import de.richtercloud.document.scanner.model.TransportTicket;
import de.richtercloud.document.scanner.model.Withdrawal;
import de.richtercloud.document.scanner.model.Workflow;
import de.richtercloud.document.scanner.model.WorkflowItem;
import de.richtercloud.document.scanner.model.imagewrapper.CachingImageWrapper;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.ClassInfo;
import de.richtercloud.swing.worker.get.wait.dialog.SwingWorkerCompletionWaiter;
import de.richtercloud.swing.worker.get.wait.dialog.SwingWorkerGetWaitDialog;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
@SuppressWarnings("PMD.CouplingBetweenObjects")
public final class DocumentScannerUtils {
    private final static Logger LOGGER = LoggerFactory.getLogger(DocumentScannerUtils.class);
    public static final String APP_NAME = "Document scanner";
    public static final String APP_VERSION = "1.0";
    public static final String BUG_URL = "https://github.com/krichter722/document-scanner";
    /**
     * The default value for resolution in DPI. The closest value to it might be
     * chosen if the exact resolution isn't available.
     */
    public final static int RESOLUTION_DEFAULT = 300;
    public final static Set<Class<?>> ENTITY_CLASSES = Collections.unmodifiableSet(new HashSet<Class<?>>(
            Arrays.asList(APackage.class,
                    Bill.class,
                    Company.class,
                    Document.class,
                    Email.class,
                    EmailAddress.class,
                    Employment.class,
                    FinanceAccount.class,
                    Leaflet.class,
                    Location.class,
                    Payment.class,
                    Person.class,
                    Shipping.class,
                    TelephoneCall.class,
                    TelephoneNumber.class,
                    Transport.class,
                    TransportTicket.class,
                    Withdrawal.class,
                    Workflow.class)));
    //WorkflowItem (and other abstract entity classes) can't be added here
    //because ReflectionFormBuilder will try to initialize an instance
    /**
     * All classes which need to queryablein components, but not necessarily
     * instantiable.
     */
    public final static Set<Class<?>> QUERYABLE_CLASSES = Collections.unmodifiableSet(ImmutableSet.<Class<?>>builder()
            .addAll(ENTITY_CLASSES)
            .add(WorkflowItem.class)
            .build());
    public final static Set<Class<?>> EMBEDDABLE_CLASSES = Collections.unmodifiableSet(new HashSet<Class<?>>(
            Arrays.asList(Address.class)));
    public final static Set<Class<?>> QUERYABLE_AND_EMBEDDABLE_CLASSES = Collections.unmodifiableSet(ImmutableSet.<Class<?>>builder()
            .addAll(QUERYABLE_CLASSES)
            .addAll(EMBEDDABLE_CLASSES)
            .build());
    public final static Class<?> PRIMARY_CLASS_SELECTION = Document.class;
    public final static int INITIAL_QUERY_LIMIT_DEFAULT = 20;
    public final static String BIDIRECTIONAL_HELP_DIALOG_TITLE = "Bidirectional relations help";
    public final static String SANED_BUG_INFO = "<br/>You might suffer from a "
            + "saned bug, try <tt>/usr/sbin/saned -d -s -a saned</tt> with "
            + "appropriate privileges in order to restart saned and try again"
            + "</html>";
    public final static int SELECTED_ENTITIES_EDIT_WARNING = 5;
    public final static String TO_FROM_FIELD_GROUP_NAME = "toFrom";
    public final static String ID_FIELD_GROUP_NAME = "id";
    public final static String TAGS_FIELD_GROUP_NAME = "tags";
    public final static String DATE_FIELD_GROUP_NAME = "date";
    public final static String COMMUNICATION_ITEM_DATE_FIELD_GROUP_NAME = "communication-item-date";
    public final static String WORKFLOW_ITEM_FIELD_GROUP_NAME = "workflow-item";
    public final static String WORKFLOW_FIELD_GROUP_NAME = "workflow";
    public final static String LOCATION_AND_FORM_FIELD_GROUP_NAME = "locationAndForm";
    public final static String DATA_FIELD_GROUP_NAME = "data";
    public final static String MONEY_FIELD_GROUP_NAME = "money";
    public final static String TRANSPORT_FIELD_GROUP_NAME = "transport";
    public final static String TRANSPORT_TICKET_FIELD_GROUP_NAME = "transport-ticket";
    public final static String EMAIL_FIELD_GROUP_NAME = "email";
    public final static String EMAIL_ADDRESS_FIELD_GROUP_NAME = "email-address";
    public final static String SHIPPING_FIELD_GROUP_NAME = "shipping";
    public final static String COMPANY_FIELD_GROUP_NAME = "company";
    public final static String IDENTIFIER_FIELD_GROUP_NAME = "identifier";
    public final static String EMPLOYMENT_FIELD_GROUP_NAME = "employment";
    public final static String FINANCE_ACCOUNT_FIELD_GROUP_NAME = "finance-account";
    public final static String LOCATION_FIELD_GROUP_NAME = "location";
    public final static String PACKAGE_FIELD_GROUP_NAME = "package";
    public final static String PAYMENT_FIELD_GROUP_NAME = "payment";
    public final static String TELEPHONE_NUMBER_FIELD_GROUP_NAME = "telephone-number";
    public final static String WITHDRAWAL_FIELD_GROUP_NAME = "withdrawal";
    public final static String BILL_AMOUNT_FIELD_GROUP_NAME = "bill-amount";

    public static List<Class<?>> sortEntityClasses(Set<Class<?>> entityClasses) {
        List<Class<?>> entityClassesSort = new LinkedList<>(entityClasses);
        Collections.sort(entityClassesSort, new Comparator<Class<?>>() {
            @Override
            public int compare(Class<?> o1, Class<?> o2) {
                String o1Value;
                ClassInfo o1ClassInfo = o1.getAnnotation(ClassInfo.class);
                if(o1ClassInfo != null) {
                    o1Value = o1ClassInfo.name();
                }else {
                    o1Value = o1.getSimpleName();
                }
                String o2Value;
                ClassInfo o2ClassInfo = o2.getAnnotation(ClassInfo.class);
                if(o2ClassInfo != null) {
                    o2Value = o2ClassInfo.name();
                }else {
                    o2Value = o2.getSimpleName();
                }
                return o1Value.compareTo(o2Value);
            }
        });
        return entityClassesSort;
    }

    /*
    internal implementation notes:
    - can't use ProgressMonitor without blocking EVT instead of a model dialog
    when using SwingWorker.get
    */
    /**
     * Uses a modal dialog in order to display the progress of the retrieval and
     * make the operation cancelable.
     * @param documentFile the file to retrieve from
     * @param waitDialogParent the parent window of the wait dialog
     * @param imageWrapperStorageDir the image wrapper storage directory
     * @param issueHandler the issue handler to use
     * @return the retrieved images or {@code null} if the retrieval has been
     *     canceled (in dialog)
     * @throws InterruptedException if an exception during asynchronous execution of the task occurs
     * @throws ExecutionException if an exception during asynchronous execution of the task occurs
     */
    @SuppressWarnings("PMD.AvoidCatchingThrowable")
    public static List<ImageWrapper> retrieveImages(final File documentFile,
            Window waitDialogParent,
            File imageWrapperStorageDir,
            IssueHandler issueHandler) throws InterruptedException, ExecutionException {
        if(documentFile == null) {
            throw new IllegalArgumentException("documentFile mustn't be null");
        }
        final SwingWorkerGetWaitDialog dialog = new SwingWorkerGetWaitDialog(SwingUtilities.getWindowAncestor(waitDialogParent), //owner
                DocumentScanner.generateApplicationWindowTitle("Wait",
                        APP_NAME,
                        APP_VERSION), //dialogTitle
                "Retrieving image data", //labelText
                null //progressBarText
        );
        final SwingWorker<List<ImageWrapper>, Void> worker = new SwingWorker<List<ImageWrapper>, Void>() {
            @Override
            protected List<ImageWrapper> doInBackground() throws DocumentAddException {
                try {
                    List<ImageWrapper> retValue = new LinkedList<>();
                    InputStream pdfInputStream = Files.newInputStream(documentFile.toPath());
                    try (PDDocument document = PDDocument.load(pdfInputStream)) {
                        PDFRenderer pdfRenderer = new PDFRenderer(document);
                        for(int page=0; page<document.getNumberOfPages(); page++) {
                            if(dialog.isCanceled()) {
                                document.close();
                                LOGGER.debug("tab generation aborted");
                                return null;
                            }
                            BufferedImage image = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                            ImageWrapper imageWrapper = new CachingImageWrapper(imageWrapperStorageDir,
                                    image);
                            retValue.add(imageWrapper);
                        }
                    }
                    return retValue;
                }catch(Throwable ex) {
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                    throw new DocumentAddException(ex);
                }
            }

            @Override
            protected void done() {
                //do nothing
            }
        };
        worker.addPropertyChangeListener(
            new SwingWorkerCompletionWaiter(dialog));
        worker.execute();
        //the dialog will be visible until the SwingWorker is done
        dialog.setVisible(true);
        return worker.get();
    }

    private DocumentScannerUtils() {
    }
}
