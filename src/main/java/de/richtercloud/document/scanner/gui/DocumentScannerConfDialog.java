/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.gui.conf.FieldOrderPanel;
import de.richtercloud.reflection.form.builder.ClassInfo;
import de.richtercloud.validation.tools.FieldRetriever;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.*;

/*
internal implementation notes:
- since the dialog is modal an needs to be closed with either the save or
discard button it's unnecessary to listen to value changes of components because
they can be retrieved when returning the new configuration instance
*/
/**
 * A dialog which allows to enforce instantiation of component values based on
 * values of a {@link DocumentScannerConf}. You have to display the dialog with
 *
 * @author richter
 */
public class DocumentScannerConfDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final DocumentScannerConf documentScannerConf;
    private final Map<Class<?>, List<Field>> fieldOrderMap;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JCheckBox autoGenerateIDsCheckBox = new JCheckBox();
    private final JCheckBox autoOCRValueDetectionCheckBox = new JCheckBox();
    private final JCheckBox autoSaveImageDataCheckBox = new JCheckBox();
    private final JCheckBox autoSaveOCRDataCheckBox = new JCheckBox();
    private final JButton discardButton = new JButton();
    private final JSeparator fieldOrderSeparator = new JSeparator();
    private final JTabbedPane fieldOrderTabbedPane = new JTabbedPane();
    private final JLabel fieldOrderTabbedPaneLabel = new JLabel();
    private final JCheckBox rememberTrimWhitespaceCheckBox = new JCheckBox();
    private final JButton saveButton = new JButton();
    // End of variables declaration//GEN-END:variables

    /**
     * Creates new form DocumentScannerOptionsDialog
     *
     * @param parent the dialog's parent frame
     * @param documentScannerConf a reference to a {@link DocumentScannerConf}
     *     to be updated when values of components change
     * @param entityClasses the set of supported entity classes
     * @param fieldRetriever the field retriever to use
     */
    public DocumentScannerConfDialog(Frame parent,
            DocumentScannerConf documentScannerConf,
            Set<Class<?>> entityClasses,
            FieldRetriever fieldRetriever) {
        super(parent,
                true //always modal
        );
        this.documentScannerConf = documentScannerConf;
        initComponents();
        this.autoGenerateIDsCheckBox.setSelected(documentScannerConf.isAutoGenerateIDs());
        this.autoSaveImageDataCheckBox.setSelected(documentScannerConf.isAutoSaveImageData());
        this.autoSaveOCRDataCheckBox.setSelected(documentScannerConf.isAutoSaveOCRData());
        this.autoOCRValueDetectionCheckBox.setSelected(documentScannerConf.isAutoOCRValueDetection());
        this.rememberTrimWhitespaceCheckBox.setSelected(documentScannerConf.isRememberTrimWhitespace());
        assert documentScannerConf.getFieldOrderMap() != null;
        assert documentScannerConf.getFieldOrderMap().keySet().equals(entityClasses);
        this.fieldOrderMap = documentScannerConf.getFieldOrderMap();
        for(Class<?> entityClass : entityClasses) {
            if(!fieldOrderMap.containsKey(entityClass)) {
                List<Field> entityClassRelevantFields = fieldRetriever.retrieveRelevantFields(entityClass);
                fieldOrderMap.put(entityClass,
                        entityClassRelevantFields);
            }
        }
        List<Class<?>> fieldOrderMapKeys = new LinkedList<>(fieldOrderMap.keySet());
        fieldOrderMapKeys.sort((Class<?> o1, Class<?> o2) -> {
            ClassInfo o1ClassInfo = o1.getAnnotation(ClassInfo.class);
            ClassInfo o2ClassInfo = o2.getAnnotation(ClassInfo.class);
            if(o1ClassInfo != null && o2ClassInfo != null) {
                return o1ClassInfo.name().compareTo(o2ClassInfo.name());
            }else {
                return o1.getSimpleName().compareTo(o2.getSimpleName());
            }
        });
        for(Class<?> entityClass : fieldOrderMapKeys) {
            FieldOrderPanel fieldOrderPanel = new FieldOrderPanel(fieldOrderMap.get(entityClass));
            JScrollPane fieldOrderPanelScrollPane = new JScrollPane(fieldOrderPanel);
            String title;
            ClassInfo entityClassInfo = entityClass.getAnnotation(ClassInfo.class);
            if(entityClassInfo != null) {
                title = entityClassInfo.name();
            }else {
                title = entityClass.getSimpleName();
            }
            fieldOrderTabbedPane.add(title, //title
                    fieldOrderPanelScrollPane);
        }
        pack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);

        autoGenerateIDsCheckBox.setText("Automatically generate IDs at saving");

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        discardButton.setText("Discard");
        discardButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discardButtonActionPerformed(evt);
            }
        });

        autoSaveImageDataCheckBox.setText("Automatically generate image data");

        autoSaveOCRDataCheckBox.setText("Automatically generate OCR data");

        autoOCRValueDetectionCheckBox.setText("Automatically detect OCR values (might be slow)");

        rememberTrimWhitespaceCheckBox.setText("Remember trim whitespace option in OCR copy text area");

        fieldOrderTabbedPaneLabel.setText("Field order:");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(fieldOrderTabbedPane, GroupLayout.Alignment.TRAILING)
                    .addComponent(autoGenerateIDsCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(discardButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(saveButton))
                    .addComponent(autoSaveImageDataCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(autoSaveOCRDataCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(fieldOrderSeparator)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(autoOCRValueDetectionCheckBox)
                            .addComponent(rememberTrimWhitespaceCheckBox)
                            .addComponent(fieldOrderTabbedPaneLabel))
                        .addGap(0, 306, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(autoGenerateIDsCheckBox)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(autoSaveImageDataCheckBox)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(autoSaveOCRDataCheckBox)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(autoOCRValueDetectionCheckBox)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rememberTrimWhitespaceCheckBox)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldOrderSeparator, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldOrderTabbedPaneLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fieldOrderTabbedPane, GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(saveButton)
                    .addComponent(discardButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        this.documentScannerConf.setAutoGenerateIDs(this.autoGenerateIDsCheckBox.isSelected());
        this.documentScannerConf.setAutoSaveImageData(this.autoSaveImageDataCheckBox.isSelected());
        this.documentScannerConf.setAutoSaveOCRData(this.autoSaveOCRDataCheckBox.isSelected());
        this.documentScannerConf.setAutoOCRValueDetection(this.autoOCRValueDetectionCheckBox.isSelected());
        this.documentScannerConf.setRememberTrimWhitespace(this.rememberTrimWhitespaceCheckBox.isSelected());
        this.documentScannerConf.setFieldOrderMap(this.fieldOrderMap);
        this.setVisible(false);
    }//GEN-LAST:event_saveButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void discardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discardButtonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_discardButtonActionPerformed
}
