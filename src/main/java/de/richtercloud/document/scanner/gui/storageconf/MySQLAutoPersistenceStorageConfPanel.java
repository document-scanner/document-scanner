/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui.storageconf;

import com.google.common.collect.ImmutableMap;
import de.richtercloud.document.scanner.gui.DocumentScanner;
import de.richtercloud.document.scanner.gui.DocumentScannerUtils;
import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.jhbuild.java.wrapper.ArchitectureNotRecognizedException;
import de.richtercloud.jhbuild.java.wrapper.OSNotRecognizedException;
import de.richtercloud.jhbuild.java.wrapper.SupportedOS;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadCombi;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadEmptyCallbackReation;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadException;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadFailureCallbackReation;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadUtils;
import de.richtercloud.jhbuild.java.wrapper.download.Downloader;
import de.richtercloud.jhbuild.java.wrapper.download.ExtractionException;
import de.richtercloud.jhbuild.java.wrapper.download.ExtractionMode;
import de.richtercloud.jhbuild.java.wrapper.download.GUIDownloader;
import de.richtercloud.jhbuild.java.wrapper.download.MD5SumCheckUnequalsCallbackReaction;
import de.richtercloud.message.handler.ConfirmMessageHandler;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.message.handler.Message;
import de.richtercloud.reflection.form.builder.jpa.storage.MySQLAutoPersistenceStorageConf;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import javax.swing.*;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class MySQLAutoPersistenceStorageConfPanel extends StorageConfPanel<MySQLAutoPersistenceStorageConf> {
    private static final long serialVersionUID = 1L;
    private final static Logger LOGGER = LoggerFactory.getLogger(MySQLAutoPersistenceStorageConfPanel.class);
    public final static String MYSQL_DOWNLOAD_URL_LINUX_64 = "https://cdn.mysql.com/Downloads/MySQL-5.7/mysql-5.7.19-linux-glibc2.12-x86_64.tar.gz";
    public final static String MYSQL_DOWNLOAD_URL_LINUX_32 = "https://cdn.mysql.com/Downloads/MySQL-5.7/mysql-5.7.19-linux-glibc2.12-i686.tar.gz";
    public final static String MYSQL_DOWNLOAD_URL_WINDOWS_64 = "https://cdn.mysql.com/Downloads/MySQL-5.7/mysql-5.7.19-winx64.zip";
    public final static String MYSQL_DOWNLOAD_URL_WINDOWS_32 = "https://cdn.mysql.com/Downloads/MySQL-5.7/mysql-5.7.19-win32.zip";
    public final static String MYSQL_DOWNLOAD_URL_MAC_OSX_64 = "https://cdn.mysql.com/Downloads/MySQL-5.7/mysql-5.7.19-macos10.12-x86_64.tar.gz";
        //Mac OSX doesn't have 32-bit versions
    private final MySQLAutoPersistenceStorageConf storageConf;
    private final IssueHandler issueHandler;
    private final ConfirmMessageHandler confirmMessageHandler;
    /**
     * The target of the MySQL download. Doesn't need to be exposed to the user.
     */
    public final static String MYSQL_DOWNLOAD_TARGET_LINUX_64 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-linux-glibc2.12-x86_64.tar.gz").getAbsolutePath();
    public final static String MYSQL_DOWNLOAD_TARGET_LINUX_32 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-linux-glibc2.12-i686.tar.gz").getAbsolutePath();
        //downloading same file with `i386` instead of `i686` works as well, but
        //it's unclear what it refers to (MD5 sums need to be adjusted and the
        //`i686` file proposed on the MySQL download page
    public final static String MYSQL_DOWNLOAD_TARGET_WINDOWS_64 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-winx64.zip").getAbsolutePath();
    public final static String MYSQL_DOWNLOAD_TARGET_WINDOWS_32 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-win32.zip").getAbsolutePath();
    public final static String MYSQL_DOWNLOAD_TARGET_MAC_OSX_64 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-macos10.12-x86_64.tar.gz").getAbsolutePath();
    /**
     * The resulting directory after extraction. Doesn't need to be exposed to
     * the user.
     */
    public final static String MYSQL_EXTRACTION_LOCATION_LINUX_64 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-linux-glibc2.12-x86_64").getAbsolutePath();
    public final static String MYSQL_EXTRACTION_LOCATION_LINUX_32 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-linux-glibc2.12-i686").getAbsolutePath();
    public final static String MYSQL_EXTRACTION_LOCATION_WINDOWS_64 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-winx64").getAbsolutePath();
    public final static String MYSQL_EXTRACTION_LOCATION_WINDOWS_32 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-win32").getAbsolutePath();
    public final static String MYSQL_EXTRACTION_LOCATION_MAC_OSX_64 = new File(DocumentScannerConf.CONFIG_DIR_DEFAULT, "mysql-5.7.19-macos10.12-x86_64").getAbsolutePath();
    public final static String MD5_SUM_LINUX_64 = "dbe7e5e820377c29d8681005065e5728";
    public final static String MD5_SUM_LINUX_32 = "32c4e286b7016bc1f45995849c235e41";
    public final static String MD5_SUM_WINDOWS_64 = "f4397d33052c5257c5e6cdabd8819024";
    public final static String MD5_SUM_WINDOWS_32 = "ae800b20d9e5eb3f7c0d647c3569be1e";
    public final static String MD5_SUM_MAC_OSX_64 = "48dfa875670f3c6468acc3ab72252f9e";
    public final static ExtractionMode MYSQL_EXTRACTION_MODE_LINUX_64 = ExtractionMode.EXTRACTION_MODE_TAR_GZ;
    public final static ExtractionMode MYSQL_EXTRACTION_MODE_LINUX_32 = ExtractionMode.EXTRACTION_MODE_TAR_GZ;
    public final static ExtractionMode MYSQL_EXTRACTION_MODE_WINDOWS_64 = ExtractionMode.EXTRACTION_MODE_ZIP;
    public final static ExtractionMode MYSQL_EXTRACTION_MODE_WINDOWS_32 = ExtractionMode.EXTRACTION_MODE_ZIP;
    public final static ExtractionMode MYSQL_EXTRACTION_MODE_MAC_OSX_64 = ExtractionMode.EXTRACTION_MODE_TAR_GZ;
    private final boolean skipMD5SumCheck;
    private final Map<SupportedOS, DownloadCombi> oSDownloadCombiMap = new ImmutableMap.Builder<SupportedOS, DownloadCombi>()
            .put(SupportedOS.LINUX_32, new DownloadCombi(MYSQL_DOWNLOAD_URL_LINUX_32,
                    MYSQL_DOWNLOAD_TARGET_LINUX_32,
                    MYSQL_EXTRACTION_MODE_LINUX_32,
                    MYSQL_EXTRACTION_LOCATION_LINUX_32,
                    MD5_SUM_LINUX_32))
            .put(SupportedOS.LINUX_64, new DownloadCombi(MYSQL_DOWNLOAD_URL_LINUX_64,
                    MYSQL_DOWNLOAD_TARGET_LINUX_64,
                    MYSQL_EXTRACTION_MODE_LINUX_64,
                    MYSQL_EXTRACTION_LOCATION_LINUX_64,
                    MD5_SUM_LINUX_64))
            .put(SupportedOS.WINDOWS_32, new DownloadCombi(MYSQL_DOWNLOAD_URL_WINDOWS_32,
                    MYSQL_DOWNLOAD_TARGET_WINDOWS_32,
                    MYSQL_EXTRACTION_MODE_WINDOWS_32,
                    MYSQL_EXTRACTION_LOCATION_WINDOWS_32,
                    MD5_SUM_WINDOWS_32))
            .put(SupportedOS.WINDOWS_64, new DownloadCombi(MYSQL_DOWNLOAD_URL_WINDOWS_64,
                    MYSQL_DOWNLOAD_TARGET_WINDOWS_64,
                    MYSQL_EXTRACTION_MODE_WINDOWS_64,
                    MYSQL_EXTRACTION_LOCATION_WINDOWS_64,
                    MD5_SUM_WINDOWS_64))
            .put(SupportedOS.MAC_OSX_64, new DownloadCombi(MYSQL_DOWNLOAD_URL_MAC_OSX_64,
                    MYSQL_DOWNLOAD_TARGET_MAC_OSX_64,
                    MYSQL_EXTRACTION_MODE_MAC_OSX_64,
                    MYSQL_EXTRACTION_LOCATION_MAC_OSX_64,
                    MD5_SUM_MAC_OSX_64))
            .build();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JTextField baseDirTextField = new JTextField();
    private final JLabel baseDirTextFieldLabel = new JLabel();
    private final JLabel binariesLabel = new JLabel();
    private final JTextField databaseDirTextField = new JTextField();
    private final JLabel databaseDirTextFieldLabel = new JLabel();
    private final JTextField databaseNameTextField = new JTextField();
    private final JLabel databaseNameTextFieldLabel = new JLabel();
    private final JSeparator directorySeparator = new JSeparator();
    private final JButton downloadButton = new JButton();
    private final JTextField hostnameTextField = new JTextField();
    private final JLabel hostnameTextFieldLabel = new JLabel();
    private final JTextField mysqlTextField = new JTextField();
    private final JLabel mysqlTextFieldLabel = new JLabel();
    private final JTextField mysqladminTextField = new JTextField();
    private final JLabel mysqladminTextFieldLabel = new JLabel();
    private final JTextField mysqldTextField = new JTextField();
    private final JLabel mysqldTextFieldLabel = new JLabel();
    private final JLabel passwordLabel = new JLabel();
    private final JPasswordField passwordPasswordField = new JPasswordField();
    private final JSpinner portSpinner = new JSpinner();
    private final JLabel portSpinnerLabel = new JLabel();
    private final JButton selectBaseDirectoryButton = new JButton();
    private final JLabel usernameLabel = new JLabel();
    private final JTextField usernameTextField = new JTextField();
    // End of variables declaration//GEN-END:variables

    public MySQLAutoPersistenceStorageConfPanel(MySQLAutoPersistenceStorageConf storageConf,
            IssueHandler issueHandler,
            ConfirmMessageHandler confirmMessageHandler,
            boolean skipMD5SumCheck) throws IOException {
        super();
        initComponents();
        this.storageConf = new MySQLAutoPersistenceStorageConf(storageConf.getDatabaseDir(),
                storageConf.getBaseDir(),
                storageConf.getHostname(),
                storageConf.getPort(),
                storageConf.getDatabaseDriver(),
                storageConf.getEntityClasses(),
                storageConf.getUsername(),
                storageConf.getPassword(),
                storageConf.getDatabaseName(),
                storageConf.getSchemeChecksumFile());
        this.issueHandler = issueHandler;
        this.confirmMessageHandler = confirmMessageHandler;
        this.skipMD5SumCheck = skipMD5SumCheck;
        this.baseDirTextField.setText(storageConf.getBaseDir());
        this.databaseNameTextField.setText(storageConf.getDatabaseName());
        this.databaseDirTextField.setText(storageConf.getDatabaseDir());
        this.hostnameTextField.setText(storageConf.getHostname());
        this.portSpinner.setValue(storageConf.getPort());
        this.usernameTextField.setText(storageConf.getUsername());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        passwordLabel.setText("Password");

        passwordPasswordField.setText("jPasswordField1");

        portSpinnerLabel.setText("Port");

        databaseDirTextFieldLabel.setText("Database directory");

        hostnameTextFieldLabel.setText("Hostname");

        usernameLabel.setText("Username");

        baseDirTextFieldLabel.setText("Base directory");

        databaseNameTextFieldLabel.setText("Database name");

        downloadButton.setText("Download");
        downloadButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadButtonActionPerformed(evt);
            }
        });

        mysqldTextFieldLabel.setText("mysqld path");

        mysqladminTextFieldLabel.setText("mysqladmin path");

        mysqlTextFieldLabel.setText("mysql path");

        binariesLabel.setText("MySQL binaries (resolved relative to base directory if left empty):");

        selectBaseDirectoryButton.setText("Select");
        selectBaseDirectoryButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectBaseDirectoryButtonActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(binariesLabel, GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(baseDirTextFieldLabel)
                        .addGap(18, 18, 18)
                        .addComponent(baseDirTextField)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(selectBaseDirectoryButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(mysqladminTextFieldLabel)
                            .addComponent(mysqldTextFieldLabel)
                            .addComponent(mysqlTextFieldLabel))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(mysqlTextField)
                            .addComponent(mysqldTextField)
                            .addComponent(mysqladminTextField)))
                    .addComponent(directorySeparator)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(downloadButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(databaseNameTextFieldLabel)
                            .addComponent(databaseDirTextFieldLabel)
                            .addComponent(hostnameTextFieldLabel)
                            .addComponent(portSpinnerLabel)
                            .addComponent(usernameLabel)
                            .addComponent(passwordLabel))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(passwordPasswordField)
                            .addComponent(usernameTextField)
                            .addComponent(portSpinner)
                            .addComponent(hostnameTextField)
                            .addComponent(databaseDirTextField)
                            .addComponent(databaseNameTextField))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(baseDirTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(baseDirTextFieldLabel)
                    .addComponent(selectBaseDirectoryButton))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downloadButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(binariesLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(mysqldTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(mysqldTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(mysqladminTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(mysqladminTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(mysqlTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(mysqlTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(directorySeparator, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(databaseNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(databaseNameTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(databaseDirTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(databaseDirTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(hostnameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(hostnameTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(portSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(portSpinnerLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(usernameLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordLabel))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    protected DownloadCombi getDownloadCombi() {
        SupportedOS currentOS;
        try {
            currentOS = DownloadUtils.getCurrentOS();
        } catch (OSNotRecognizedException ex) {
            this.issueHandler.handle(new Message(String.format("Operating "
                    + "system %s isn't supported for automatic download, "
                    + "please download MySQL manually and set pathes "
                    + "accordingly",
                            SystemUtils.OS_NAME),
                    JOptionPane.WARNING_MESSAGE,
                    "Operating system not supported"));
            return null;
        } catch (ArchitectureNotRecognizedException ex) {
            this.issueHandler.handle(new Message(String.format("%s "
                    + "operating system architecture %s isn't supported "
                    + "for automatic download, please download MySQL "
                    + "manually and set pathes accordingly",
                            ex.getoSName(),
                            SystemUtils.OS_ARCH),
                    JOptionPane.WARNING_MESSAGE,
                    "Linux OS architecture not supported"));
            return null;
        }
        DownloadCombi retValue = oSDownloadCombiMap.get(currentOS);
        assert retValue != null;
        return retValue;
    }

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void downloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadButtonActionPerformed
        try {
            DownloadCombi downloadCombi = getDownloadCombi();
            if(downloadCombi == null) {
                //system could not be recognized
                return;
            }
            mySQLDownload(downloadCombi);
            baseDirTextField.setText(downloadCombi.getExtractionLocation());
                //not necessary to set on storageConf because it'll be set in save
        }catch(Throwable ex) {
            LOGGER.error("unexpected exception during download of MySQL occured",
                    ex);
            issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        }
    }//GEN-LAST:event_downloadButtonActionPerformed

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void selectBaseDirectoryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectBaseDirectoryButtonActionPerformed
        try {
            File baseDir = new File("");
            if(baseDirTextField.getText() != null
                    && !baseDirTextField.getText().isEmpty()) {
                File baseDirCandidate = new File(baseDirTextField.getText());
                if(baseDirCandidate.exists()) {
                    if(baseDirCandidate.isDirectory()) {
                        baseDir = baseDirCandidate;
                    }else {
                        baseDir = baseDirCandidate.getParentFile();
                    }
                }
            }
            JFileChooser baseDirFileChooser = new JFileChooser(baseDir);
            baseDirFileChooser.setDialogTitle("Select MySQL base directory");
            baseDirFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            baseDirFileChooser.setFileHidingEnabled(false);
            int baseDirFileChooseRetValue = baseDirFileChooser.showDialog(this, "Select");
            if(baseDirFileChooseRetValue == JFileChooser.APPROVE_OPTION) {
                baseDirTextField.setText(baseDirFileChooser.getSelectedFile().getPath());
            }
                //not necessary to set on storageConf because it'll be set in save
        }catch(Throwable ex) {
            LOGGER.error("unexpected exception during download of MySQL occured",
                    ex);
            issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        }
    }//GEN-LAST:event_selectBaseDirectoryButtonActionPerformed

    protected boolean mySQLDownload(DownloadCombi downloadCombi) throws IOException,
            ExtractionException,
            DownloadException {
        Downloader downloader = new GUIDownloader(SwingUtilities.getWindowAncestor(this), //downloadDialogParent
                DocumentScanner.generateApplicationWindowTitle("Downloading MySQL",
                        DocumentScannerUtils.APP_NAME,
                        DocumentScannerUtils.APP_VERSION), //downloadDialogTitle
                "Downloading MySQL", //labelText
                "Downloading MySQL" //progressBarText
        );
        return downloader.downloadFile(downloadCombi,
                skipMD5SumCheck,
            (Exception ex, int numberOfRetries) -> {
                return confirmMessageHandler.confirm(new Message(String.format(
                        "Download failed because of the following exception: %s",
                                ex.getMessage()),
                        JOptionPane.ERROR_MESSAGE,
                        "Download failed"),
                        DownloadFailureCallbackReation.CANCEL,
                        DownloadFailureCallbackReation.RETRY);
            },
            (String md5SumExpected, String md5SumActual, int numberOfRetries) -> {
                return confirmMessageHandler.confirm(new Message(String.format(
                        "MD5 sum %s "
                        + "of download '%s' doesn't match the "
                        + "specified MD5 sum %s. This "
                        + "indicates an incomplete download, a "
                        + "wrong specified MD5 sum or an error "
                        + "during transfer.",
                                md5SumActual,
                                downloadCombi.getDownloadTarget(),
                                md5SumExpected),
                        JOptionPane.ERROR_MESSAGE,
                        "MD5 sum verification failed"),
                        MD5SumCheckUnequalsCallbackReaction.RETRY,
                        MD5SumCheckUnequalsCallbackReaction.CANCEL);
            }, //mD5SumCheckUnequalsCallback
            numberOfRetries -> {
                return confirmMessageHandler.confirm(new Message(String.format(
                        "The download '%s' is empty.",
                        downloadCombi.getDownloadTarget()),
                                JOptionPane.ERROR_MESSAGE,
                                "Download is empty"),
                        DownloadEmptyCallbackReation.RETRY,
                        DownloadEmptyCallbackReation.CANCEL);
            } //downloadEmptyCallback
        );
    }

    @Override
    public MySQLAutoPersistenceStorageConf getStorageConf() {
        return storageConf;
    }

    @Override
    public void save() {
        String baseDir = baseDirTextField.getText();
        this.storageConf.setBaseDir(baseDir);
        String mysqld = mysqldTextField.getText();
        this.storageConf.setMysqld(mysqld.isEmpty() ? null : mysqld);
        String mysqladmin = mysqladminTextField.getText();
        this.storageConf.setMysqladmin(mysqladmin.isEmpty() ? null : mysqladmin);
        String mysql = mysqlTextField.getText();
        this.storageConf.setMysql(mysql.isEmpty() ? null : mysql);
        this.storageConf.setDatabaseName(this.databaseNameTextField.getText());
        this.storageConf.setDatabaseDir(this.databaseDirTextField.getText());
        this.storageConf.setHostname(this.hostnameTextField.getText());
        this.storageConf.setPort((int) this.portSpinner.getValue());
        String username = this.usernameTextField.getText();
        this.storageConf.setUsername(username);
        String password = String.valueOf(this.passwordPasswordField.getPassword());
        this.storageConf.setPassword(password);
    }

    @Override
    public void cancel() {
        //do nothing
    }
}
