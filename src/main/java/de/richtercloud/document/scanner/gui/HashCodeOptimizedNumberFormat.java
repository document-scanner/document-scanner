/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Currency;
import java.util.Objects;

/**
 * An implementation wrapping a {@link NumberFormat} delegating all method calls to it, except for the
 * {@link #hashCode()} implementation which creates a lot less collisions than the original. This makes adding
 * 150 000 instances in a {@link java.util.HashSet}-based {@link java.util.stream.Collector} possible in a few seconds
 * rather than 30 minutes.
 *
 * Caching the hash code seems reduce the effort to one third approximately, but a proper implementation requires
 * effort which is not worth the few seconds saved.
 *
 * @author richter
 */
public class HashCodeOptimizedNumberFormat extends DecimalFormat {
    private final DecimalFormat delegate;

    public HashCodeOptimizedNumberFormat(DecimalFormat delegate) {
        super();
        this.delegate = delegate;
    }

    @Override
    public StringBuffer format(double v, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        return delegate.format(v, stringBuffer, fieldPosition);
    }

    @Override
    public StringBuffer format(long l, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        return delegate.format(l, stringBuffer, fieldPosition);
    }

    @Override
    public Number parse(String s, ParsePosition parsePosition) {
        return delegate.parse(s, parsePosition);
    }

    @Override
    public void setCurrency(Currency currency) {
        delegate.setCurrency(currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(delegate.getMinimumIntegerDigits(),
                delegate.getMaximumFractionDigits(),
                delegate.getMinimumFractionDigits(),
                delegate.isGroupingUsed(),
                delegate.isParseIntegerOnly(),
                delegate.getCurrency());
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof DecimalFormat)) {
            return false;
        }
        DecimalFormat objCast = (DecimalFormat)obj;
        return delegate.equals(objCast);
    }
}
