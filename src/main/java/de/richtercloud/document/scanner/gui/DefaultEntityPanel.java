/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import de.richtercloud.document.scanner.components.ValueDetectionReflectionFormBuilder;
import de.richtercloud.document.scanner.controller.ValueDetectionServiceController;
import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.ifaces.EntityPanel;
import de.richtercloud.document.scanner.ifaces.MainPanel;
import de.richtercloud.document.scanner.ifaces.OCREngineRecognitionException;
import de.richtercloud.document.scanner.ifaces.OCRSelectPanelPanelFetcher;
import de.richtercloud.document.scanner.valuedetectionservice.DefaultValueDetectionServiceExecutor;
import de.richtercloud.document.scanner.valuedetectionservice.ResultFetchingException;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionResult;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionService;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceExecutor;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceExecutorListener;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceUpdateEvent;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.message.handler.Message;
import de.richtercloud.reflection.form.builder.fieldhandler.FieldHandler;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class DefaultEntityPanel extends EntityPanel {
    private static final long serialVersionUID = 1L;
    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultEntityPanel.class);
    private final ValueDetectionServiceExecutor valueDetectionServiceExecutor;
    private final ValueDetectionReflectionFormBuilder reflectionFormBuilder;
    private final IssueHandler issueHandler;
    private final DocumentScannerConf documentScannerConf;
    private final ReflectionFormPanelTabbedPane entityCreationTabbedPane;
    /**
     * Store for the last results of
     * {@link #valueDetectionNonGUI(de.richtercloud.document.scanner.ifaces.OCRSelectPanelPanelFetcher, boolean) }
     * which one might display without retrieving them again from the OCR
     * result.
     */
    private Map<ValueDetectionService, List<ValueDetectionResult>> detectionResults;
    /**
     * The Apache Tika language detector used for value detection.
     */
    /*
    internal implementation notes:
    - @TODO: figure out whether it's better in terms of memory requirement and
    performance to have one instance per EntityPanel or one static instance
    whose access needs to be synchronized since there's no info about eventual
    thread-safety of LanguageDetector
    */
    private final LanguageDetector languageDetector;
    private final MainPanel mainPanel;

    /**
     * Creates new form EntityPanel.
     *
     * @param mainPanel the owning main panel
     * @param reflectionFormBuilder the reflection form builder to use
     * @param fieldHandler the field handler to use
     * @param issueHandler the issue handler to use
     * @param documentScannerConf the document scanner configuration to read
     * @param reflectionFormPanelTabbedPane the reflection form builder tabbed pane to use
     * @param valueDetectionServiceController the value detection service controller of the application
     * @throws IllegalArgumentException if {@code fieldHandler}, {@code reflectionFormBuilder}, {@code issueHandler}
     *     or {@code documentScannerConf} is {@code null}
     * @throws IOException if one happens during loading of Apache Tika language
     *     detector models
     */
    @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
    public DefaultEntityPanel(MainPanel mainPanel,
            ValueDetectionReflectionFormBuilder reflectionFormBuilder,
            FieldHandler fieldHandler,
            IssueHandler issueHandler,
            DocumentScannerConf documentScannerConf,
            ReflectionFormPanelTabbedPane reflectionFormPanelTabbedPane,
            ValueDetectionServiceController valueDetectionServiceController) throws IllegalArgumentException,
            IOException {
        super();
        if(mainPanel == null) {
            throw new IllegalArgumentException("mainPanel mustn't be null");
        }
        this.mainPanel = mainPanel;
        if(fieldHandler == null) {
            throw new IllegalArgumentException("fieldHandler mustn't be null");
        }
        entityCreationTabbedPane = reflectionFormPanelTabbedPane;
        this.initComponents();
        if(reflectionFormBuilder == null) {
            throw new IllegalArgumentException("reflectionFormBuilder mustn't be null");
        }
        this.reflectionFormBuilder = reflectionFormBuilder;
        if(issueHandler == null) {
            throw new IllegalArgumentException("messageHandler mustn't be null");
        }
        this.issueHandler = issueHandler;
        if(documentScannerConf == null) {
            throw new IllegalArgumentException("documentScannerConf mustn't be null");
        }
        this.documentScannerConf = documentScannerConf;
        this.languageDetector = LanguageDetector.getDefaultLanguageDetector();
        assert languageDetector != null;
        languageDetector.loadModels();
            //- avoids NullPointerException in
            //org.apache.tika.langdetect.OptimaizeLangDetector which is usually
            //returned by LanguageDetector.getDefaultLanguageDetector, requested
            //clarification at https://issues.apache.org/jira/browse/TIKA-2439
            //- takes a long time, so languageDetector should be initialized in
            //constructor
        this.valueDetectionServiceExecutor = new DefaultValueDetectionServiceExecutor(valueDetectionServiceController,
                issueHandler);
        valueDetectionServiceExecutor.addListener(new ValueDetectionServiceExecutorListener() {
            @Override
            public void onUpdate(ValueDetectionServiceUpdateEvent updateEvent) {
                float progress = updateEvent.getWordNumber()/(float)updateEvent.getWordCount();
                getMainPanel().getoCRSelectComponent().getValueDetectionButton().setProgress(progress);
            }

            @Override
            public void onFinished() {
                getMainPanel().getoCRSelectComponent().getValueDetectionButton().setProgress(0.0f);
                //reset progress in case the last update event has a
                //progress < 1.0f
                getMainPanel().getoCRSelectComponent().getValueDetectionButton().setEnabled(true);
                setEnabled(true);
            }
        });
    }

    public JTabbedPane getEntityCreationTabbedPane() {
        return entityCreationTabbedPane;
    }

    @Override
    public MainPanel getMainPanel() {
        return mainPanel;
    }

    @Override
    public ValueDetectionServiceExecutor getValueDetectionServiceExecutor() {
        return valueDetectionServiceExecutor;
    }

    @Override
    @SuppressWarnings("PMD.AvoidCatchingThrowable")
    public void valueDetection(OCRSelectPanelPanelFetcher oCRSelectPanelPanelFetcher,
            boolean forceRenewal) {
        for(Pair<Class, Field> pair : this.reflectionFormBuilder.getComboBoxModelMap().keySet()) {
            JComboBox<ValueDetectionResult<?>> comboBox = this.reflectionFormBuilder.getComboBoxModelMap().get(pair);
            comboBox.setEnabled(false);
        }
        Thread valueDetectionThread = new Thread(() -> {
            try {
                valueDetectionNonGUI(oCRSelectPanelPanelFetcher, forceRenewal);
                SwingUtilities.invokeLater(() -> {
                    try {
                        valueDetectionGUI();
                    }catch(Throwable ex) {
                        LOGGER.error("unexpected exception during fetching of "
                                + "auto-OCR-detection values", ex);
                        issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                    }
                });
            }catch(Throwable ex) {
                LOGGER.error("unexpected exception during value detection occured",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
            }
        },
                "auto-ocr-value-detection-thread");
        valueDetectionThread.start();
    }

    @SuppressWarnings("PMD.AvoidDeeplyNestedIfStmts")
    private void valueDetectionNonGUI(OCRSelectPanelPanelFetcher oCRSelectPanelPanelFetcher,
            boolean forceRenewal) throws ResultFetchingException {
        if(detectionResults == null || forceRenewal) {
            final String oCRResult;
            try {
                oCRResult = oCRSelectPanelPanelFetcher.fetch();
                if(oCRResult == null) {
                    //cache has been shut down
                    return;
                }
            } catch (OCREngineRecognitionException ex) {
                LOGGER.error("unexpected exception during fetching of "
                        + "auto-OCR-detection values", ex);
                issueHandler.handle(new Message(ex, JOptionPane.ERROR_MESSAGE));
                return;
            }
            if(oCRResult != null) {
                //null indicates that the recognition has been aborted
                String languageIdentifier = documentScannerConf.getTextLanguageIdentifier();
                if(languageIdentifier == null) {
                    //indicates that the text language ought to be recognized
                    //automatically
                    List<LanguageResult> languageResults = languageDetector.detectAll(oCRResult);
                    if(languageResults.size() != 1) {
                        //detection result is either empty or has more than one
                        //candidate -> need user input
                        issueHandler.handle(new Message("The language of the "
                                + "OCR result couldn't be detected "
                                + "automatically, please select the text "
                                + "language in the list",
                                JOptionPane.ERROR_MESSAGE,
                                "Language detection failed"));
                        return;
                    }
                    languageIdentifier = languageResults.get(0).getLanguage();
                    assert languageIdentifier != null && !languageIdentifier.isEmpty();
                }
                detectionResults = valueDetectionServiceExecutor.execute(oCRResult,
                        languageIdentifier);
            }
        }
    }

    @Override
    public void valueDetectionGUI() {
        if(detectionResults != null && !detectionResults.isEmpty()
            //might be null if OCREngineRecognitionException occured in
            //valueDetectionNonGUI
        ) {
            for(Pair<Class, Field> pair : this.reflectionFormBuilder.getComboBoxModelMap().keySet()) {
                JComboBox<ValueDetectionResult<?>> comboBox = this.reflectionFormBuilder.getComboBoxModelMap().get(pair);
                comboBox.setEnabled(true);
                DefaultComboBoxModel<ValueDetectionResult<?>> comboBoxModel = (DefaultComboBoxModel<ValueDetectionResult<?>>) comboBox.getModel();
                Field field = pair.getValue();
                comboBoxModel.removeAllElements();
                comboBoxModel.addElement(null);
                for(ValueDetectionService valueDetectionService : detectionResults.keySet()) {
                    if(!valueDetectionService.supportsField(field)) {
                        continue;
                    }
                    for(ValueDetectionResult detectionResult : detectionResults.get(valueDetectionService)) {
                        comboBoxModel.addElement(detectionResult);
                    }
                }
                comboBoxModel.setSelectedItem(null);
            }
        }
    }

    private void initComponents() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(entityCreationTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(entityCreationTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
        );
    }
}
