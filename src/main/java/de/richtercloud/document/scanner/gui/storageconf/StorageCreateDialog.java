/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui.storageconf;

import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.jpa.storage.DerbyEmbeddedPersistenceStorageConf;
import de.richtercloud.reflection.form.builder.storage.StorageConf;
import de.richtercloud.reflection.form.builder.storage.StorageConfValidationException;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.InvocationTargetException;
import javax.swing.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class StorageCreateDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final static Logger LOGGER = LoggerFactory.getLogger(StorageCreateDialog.class);
    private final MutableComboBoxModel<Class<? extends StorageConf>> storageCreateDialogTypeComboBoxModel = new DefaultComboBoxModel<>();
    /**
     * Reference to the created {@link StorageConf}. {@code null} indicates that
     * creation has been canceled.
     */
    private StorageConf createdStorageConf;
    private final StorageConfPanelFactory storageConfPanelFactory;
    private StorageConfPanel<?> selectedStorageConfPanel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JButton storageCreateDialogCancelDialog = new JButton();
    private final JLabel storageCreateDialogNameLabel = new JLabel();
    private final JTextField storageCreateDialogNameTextField = new JTextField();
    private final JPanel storageCreateDialogPanel = new JPanel();
    private final JButton storageCreateDialogSaveButton = new JButton();
    private final JSeparator storageCreateDialogSeparator = new JSeparator();
    private final JComboBox<Class<? extends StorageConf>> storageCreateDialogTypeComboBox = new JComboBox<>();
    private final JLabel storageCreateDialogTypeLabel = new JLabel();
    // End of variables declaration//GEN-END:variables

    public StorageCreateDialog(Window parent,
            StorageConfPanelFactory storageConfPanelFactory,
            IssueHandler issueHandler) {
        super(parent,
                ModalityType.APPLICATION_MODAL //modalityType
        );
        this.storageConfPanelFactory = storageConfPanelFactory;
        initComponents();
        this.storageCreateDialogTypeComboBoxModel.addElement(DerbyEmbeddedPersistenceStorageConf.class);
        this.storageCreateDialogTypeComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Class<? extends StorageConf> clazz = (Class<? extends StorageConf>) e.getItem();
                try {
                    clazz.getDeclaredConstructor().newInstance();
                } catch (NoSuchMethodException
                        | SecurityException
                        | InstantiationException
                        | IllegalAccessException
                        | IllegalArgumentException
                        | InvocationTargetException ex) {
                    LOGGER.error("unexpected exception during storage initialization",
                            ex);
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                    return;
                }
                StorageConfPanel<?> storageConfPanel;
                try {
                    storageConfPanel = StorageCreateDialog.this.storageConfPanelFactory.create(createdStorageConf);
                } catch (StorageConfPanelCreationException ex) {
                    LOGGER.error("unexpected exception during storage initialization",
                            ex);
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                    return;
                }
                selectedStorageConfPanel = storageConfPanel;
                StorageCreateDialog.this.storageCreateDialogPanel.removeAll();
                StorageCreateDialog.this.storageCreateDialogPanel.add(storageConfPanel);
                StorageCreateDialog.this.storageCreateDialogPanel.revalidate();
                StorageCreateDialog.this.pack();
                StorageCreateDialog.this.storageCreateDialogPanel.repaint();
            }
        });
    }

    public StorageConf getCreatedStorageConf() {
        return createdStorageConf;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        storageCreateDialogNameLabel.setText("Name");

        storageCreateDialogTypeComboBox.setModel(storageCreateDialogTypeComboBoxModel);

        storageCreateDialogTypeLabel.setText("Type");

        storageCreateDialogCancelDialog.setText("Cancel");
        storageCreateDialogCancelDialog.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storageCreateDialogCancelDialogActionPerformed(evt);
            }
        });

        storageCreateDialogSaveButton.setText("Save");
        storageCreateDialogSaveButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                storageCreateDialogSaveButtonActionPerformed(evt);
            }
        });

        GroupLayout storageCreateDialogPanelLayout = new GroupLayout(storageCreateDialogPanel);
        storageCreateDialogPanel.setLayout(storageCreateDialogPanelLayout);
        storageCreateDialogPanelLayout.setHorizontalGroup(
            storageCreateDialogPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        storageCreateDialogPanelLayout.setVerticalGroup(
            storageCreateDialogPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGap(0, 138, Short.MAX_VALUE)
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(storageCreateDialogPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(storageCreateDialogSeparator)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(storageCreateDialogNameLabel)
                            .addComponent(storageCreateDialogTypeLabel))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(storageCreateDialogNameTextField)
                            .addComponent(storageCreateDialogTypeComboBox, 0, 314, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(storageCreateDialogSaveButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(storageCreateDialogCancelDialog)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(storageCreateDialogNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(storageCreateDialogNameLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(storageCreateDialogTypeComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(storageCreateDialogTypeLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(storageCreateDialogSeparator, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(storageCreateDialogPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(storageCreateDialogCancelDialog)
                    .addComponent(storageCreateDialogSaveButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageCreateDialogCancelDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storageCreateDialogCancelDialogActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_storageCreateDialogCancelDialogActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void storageCreateDialogSaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_storageCreateDialogSaveButtonActionPerformed
        selectedStorageConfPanel.save();
        try {
            selectedStorageConfPanel.getStorageConf().validate();
        }catch(StorageConfValidationException ex) {
            //keep create dialog displayed until a valid StorageConf is created
            //or the creation has been canceled
            return;
        }
        this.createdStorageConf = selectedStorageConfPanel.getStorageConf();
        this.setVisible(false);
    }//GEN-LAST:event_storageCreateDialogSaveButtonActionPerformed
}
