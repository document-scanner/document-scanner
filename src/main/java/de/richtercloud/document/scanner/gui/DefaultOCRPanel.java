/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.ifaces.MainPanel;
import de.richtercloud.document.scanner.ifaces.OCRPanel;
import de.richtercloud.document.scanner.setter.ValueSetter;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.validation.tools.FieldRetriever;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/*
internal implementation notes:
- popup menu has a single menu "Paste into" rather than a disabled menu item
which is (mis)used as label for the following menu items because that's more
elegant even if less easy to use (means one click more)
- Since NumberFormats are hard to compare (NumberFormat.equals isn't
implemented and a lot of properties aren't suitable for comparison (comparing
groupingUsed, parseIntegerOnly, maximumFractionDigits, maximumIntegerDigits,
minimumFractionDigits and minimumIntegerDigits for equality results in 2
different number formats which doesn't make sense and doesn't match with a list
of all available formats); providing selection for all 160 available locales is
overkill and strangely results in > 20 identical format results of "-12345,987"
-> compare format result of "-12345,987"
*/
/**
 * The counterpart of every {@link de.richtercloud.document.scanner.ifaces.OCRSelectPanel} which contains a text field
 * to display OCR selection results and a toolbar with buttons and popup menus
 * to select different number formats for setting OCR selections on fields.
 *
 * If one or two of the date formats are selected and the other(s) are on
 * automatic the OCR selection is processed in the order date time, time and
 * date nevertheless.
 *
 * If nothing is selected and setting a value on a field with context menu is
 * requested, the complete content of the OCR text area is used.
 *
 * Provides a checkbox to allow quick switching between trimming whitespace from
 * copied text or not. Since there's no easy way to manipulate the source of a
 * paste action, the state of the checkbox will be read when the text is pasted.
 *
 * @author richter
 */
public class DefaultOCRPanel extends OCRPanel {
    private static final long serialVersionUID = 1L;
    private final JScrollPopupMenu currencyFormatPopup = new JScrollPopupMenu("Currency");
    private final JScrollPopupMenu dateFormatPopup = new JScrollPopupMenu("Date");
    private final JScrollPopupMenu dateTimeFormatPopup = new JScrollPopupMenu("Date and time");
    private final JScrollPopupMenu numberFormatPopup = new JScrollPopupMenu("Number");
    private final JScrollPopupMenu percentFormatPopup = new JScrollPopupMenu("Percent");
    private final JScrollPopupMenu timeFormatPopup = new JScrollPopupMenu("Time");
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JButton currencyFormatButton = new JButton();
    private final ButtonGroup currencyFormatPopupButtonGroup = new ButtonGroup();
    private final JButton dateFormatButton = new JButton();
    private final ButtonGroup dateFormatPopupButtonGroup = new ButtonGroup();
    private final JButton dateTimeFormatButton = new JButton();
    private final ButtonGroup dateTimeFormatPopupButtonGroup = new ButtonGroup();
    private final JButton numberFormatButton = new JButton();
    private final ButtonGroup numberFormatPopupButtonGroup = new ButtonGroup();
    private final JLabel oCRResultLabel = new JLabel();
    private final JPopupMenu oCRResultPopup = new JPopupMenu();
    private final JMenu oCRResultPopupPasteIntoMenu = new JMenu();
    private final JTextArea oCRResultTextArea = new JTextArea();
    private final JScrollPane oCRResultTextAreaScrollPane = new JScrollPane();
    private final JButton percentFormatButton = new JButton();
    private final ButtonGroup percentFormatPopupButtonGroup = new ButtonGroup();
    private final JButton timeFormatButton = new JButton();
    private final ButtonGroup timeFormatPopupButtonGroup = new ButtonGroup();
    private final JToolBar toolbar = new JToolBar();
    private final JCheckBox trimWhitespaceCheckBox = new JCheckBox();
    // End of variables declaration//GEN-END:variables
    private final MainPanel mainPanel;

    public DefaultOCRPanel(MainPanel mainPanel,
            Set<Class<?>> entityClasses,
            ReflectionFormPanelTabbedPane reflectionFormPanelTabbedPane,
            Map<Class<? extends JComponent>, ValueSetter<?,?>> valueSetterMapping,
            IssueHandler issueHandler,
            FieldRetriever fieldRetriever,
            DocumentScannerConf documentScannerConf) {
        super();
        if(mainPanel == null) {
            throw new IllegalArgumentException("mainPanel mustn't be null");
        }
        this.mainPanel = mainPanel;
        this.initComponents();
        if(documentScannerConf == null) {
            throw new IllegalArgumentException("documentScannerConf mustn't be "
                    + "null");
        }
        List<Class<?>> entityClassesSort = DocumentScannerUtils.sortEntityClasses(entityClasses);
        FormatOCRFieldMenuPopupFactory oCRFieldMenuPopupFactory = new FormatOCRFieldMenuPopupFactory(numberFormatPopupButtonGroup,
                percentFormatPopupButtonGroup,
                currencyFormatPopupButtonGroup,
                dateFormatPopupButtonGroup,
                timeFormatPopupButtonGroup,
                dateTimeFormatPopupButtonGroup,
                oCRResultTextArea,
                issueHandler,
                valueSetterMapping);
        List<JMenuItem> oCRResultPopupPasteIntoMenuItems = oCRFieldMenuPopupFactory.createFieldPopupMenuItems(entityClassesSort,
                reflectionFormPanelTabbedPane,
                fieldRetriever);
        for(JMenuItem oCRResultPopupPasteIntoMenuItem : oCRResultPopupPasteIntoMenuItems) {
            oCRResultPopupPasteIntoMenu.add(oCRResultPopupPasteIntoMenuItem);
        }
        /*
        Formats are compared by the formatted output, i.e. two formats are
        considered equals if the formatted output is equals
        */
        Map<String, Pair<NumberFormat, Set<Locale>>> numberFormats = new HashMap<>();
        Map<String, Pair<NumberFormat, Set<Locale>>> percentFormats = new HashMap<>();
        Map<String, Pair<NumberFormat, Set<Locale>>> currencyFormats = new HashMap<>();
        Map<String, Pair<DateFormat, Set<Locale>>> dateFormats = new HashMap<>();
        Map<String, Pair<DateFormat, Set<Locale>>> timeFormats = new HashMap<>();
        Iterator<Locale> localeIterator = new ArrayList<>(Arrays.asList(Locale.getAvailableLocales())).iterator();
        Locale firstLocale = localeIterator.next();
        String numberString = NumberFormat.getNumberInstance(firstLocale).format(FormatUtils.NUMBER_FORMAT_VALUE);
        String percentString = NumberFormat.getPercentInstance(firstLocale).format(FormatUtils.NUMBER_FORMAT_VALUE);
        String currencyString = NumberFormat.getCurrencyInstance(firstLocale).format(FormatUtils.NUMBER_FORMAT_VALUE);
        numberFormats.put(numberString,
                new ImmutablePair<NumberFormat, Set<Locale>>(NumberFormat.getNumberInstance(firstLocale),
                        new HashSet<>(Arrays.asList(firstLocale))));
        percentFormats.put(percentString,
                new ImmutablePair<NumberFormat, Set<Locale>>(NumberFormat.getPercentInstance(firstLocale),
                        new HashSet<>(Arrays.asList(firstLocale))));
        currencyFormats.put(currencyString,
                new ImmutablePair<NumberFormat, Set<Locale>>(NumberFormat.getCurrencyInstance(firstLocale),
                        new HashSet<>(Arrays.asList(firstLocale))));
        for(int formatInt : FormatUtils.DATE_FORMAT_INTS) {
            String dateString = DateFormat.getDateInstance(formatInt, firstLocale).format(FormatUtils.DATE_FORMAT_VALUE);
            dateFormats.put(dateString,
                    new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getDateInstance(formatInt, firstLocale),
                            new HashSet<>(Arrays.asList(firstLocale))));
            String timeString = DateFormat.getTimeInstance(formatInt, firstLocale).format(FormatUtils.DATE_FORMAT_VALUE);
            timeFormats.put(timeString,
                    new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getTimeInstance(formatInt, firstLocale),
                            new HashSet<>(Arrays.asList(firstLocale))));
        }
        while(localeIterator.hasNext()) {
            Locale locale = localeIterator.next();
            numberString = NumberFormat.getNumberInstance(locale).format(FormatUtils.NUMBER_FORMAT_VALUE);
            percentString = NumberFormat.getPercentInstance(locale).format(FormatUtils.NUMBER_FORMAT_VALUE);
            currencyString = NumberFormat.getCurrencyInstance(locale).format(FormatUtils.NUMBER_FORMAT_VALUE);
            Pair<NumberFormat, Set<Locale>> numberFormatsPair = numberFormats.get(numberString);
            if(numberFormatsPair == null) {
                numberFormatsPair = new ImmutablePair<NumberFormat, Set<Locale>>(NumberFormat.getNumberInstance(locale),
                        new HashSet<Locale>());
                numberFormats.put(numberString, numberFormatsPair);
            }
            Set<Locale> numberFormatsLocales = numberFormatsPair.getValue();
            numberFormatsLocales.add(locale);
            Pair<NumberFormat, Set<Locale>> percentFormatsPair = percentFormats.get(percentString);
            if(percentFormatsPair == null) {
                percentFormatsPair = new ImmutablePair<NumberFormat, Set<Locale>>(NumberFormat.getPercentInstance(locale),
                        new HashSet<Locale>());
                percentFormats.put(percentString, percentFormatsPair);
            }
            Set<Locale> percentFormatsLocales = percentFormatsPair.getValue();
            percentFormatsLocales.add(locale);
            Pair<NumberFormat, Set<Locale>> currencyFormatsPair = currencyFormats.get(currencyString);
            if(currencyFormatsPair == null) {
                currencyFormatsPair = new ImmutablePair<NumberFormat, Set<Locale>>(NumberFormat.getCurrencyInstance(locale),
                        new HashSet<Locale>());
                currencyFormats.put(currencyString, currencyFormatsPair);
            }
            Set<Locale> currencyFormatsLocales = currencyFormatsPair.getValue();
            currencyFormatsLocales.add(locale);
            for(int formatInt : FormatUtils.DATE_FORMAT_INTS) {
                String dateString = DateFormat.getDateInstance(formatInt, locale).format(FormatUtils.DATE_FORMAT_VALUE);
                Pair<DateFormat, Set<Locale>> dateFormatsPair = dateFormats.get(dateString);
                if(dateFormatsPair == null) {
                    dateFormatsPair = new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getDateInstance(formatInt, locale),
                            new HashSet<Locale>());
                    dateFormats.put(dateString, dateFormatsPair);
                }
                Set<Locale> dateFormatsLocales = dateFormatsPair.getValue();
                dateFormatsLocales.add(locale);
                String timeString = DateFormat.getTimeInstance(formatInt, locale).format(FormatUtils.DATE_FORMAT_VALUE);
                Pair<DateFormat, Set<Locale>> timeFormatsPair = timeFormats.get(timeString);
                if(timeFormatsPair == null) {
                    timeFormatsPair = new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getTimeInstance(formatInt, locale),
                            new HashSet<Locale>());
                    timeFormats.put(timeString, timeFormatsPair);
                }
                Set<Locale> timeFormatsLocales = timeFormatsPair.getValue();
                timeFormatsLocales.add(locale);
            }
        }
        //add an automatic menu item (first) and menu items for each distinct
        //format (all entries in numberFormat, percentFormat, etc. has to be
        //distinct)
        JRadioButtonMenuItem numberFormatAutomaticMenuItem = new NumberFormatMenuItem(null);
        numberFormatPopup.add(numberFormatAutomaticMenuItem);
        numberFormatPopupButtonGroup.add(numberFormatAutomaticMenuItem);
        if(documentScannerConf.isAutomaticFormatInitiallySelected()) {
            numberFormatAutomaticMenuItem.setSelected(true);
        }
        for(Map.Entry<String, Pair<NumberFormat, Set<Locale>>> numberFormat : numberFormats.entrySet()) {
            JRadioButtonMenuItem menuItem = new NumberFormatMenuItem(numberFormat.getValue().getKey());
            numberFormatPopup.add(menuItem);
            numberFormatPopupButtonGroup.add(menuItem);
            if(!documentScannerConf.isAutomaticFormatInitiallySelected()
                    && numberFormat.getValue().getValue().contains(documentScannerConf.getLocale())) {
                menuItem.setSelected(true);
            }
        }
        JRadioButtonMenuItem percentFormatAutomaticMenuItem = new NumberFormatMenuItem(null);
        percentFormatPopup.add(percentFormatAutomaticMenuItem);
        percentFormatPopupButtonGroup.add(percentFormatAutomaticMenuItem);
        if(documentScannerConf.isAutomaticFormatInitiallySelected()) {
            percentFormatAutomaticMenuItem.setSelected(true);
        }
        for(Map.Entry<String, Pair<NumberFormat, Set<Locale>>> percentFormat : percentFormats.entrySet()) {
            JRadioButtonMenuItem menuItem = new NumberFormatMenuItem(percentFormat.getValue().getKey());
            percentFormatPopup.add(menuItem);
            percentFormatPopupButtonGroup.add(menuItem);
            if(!documentScannerConf.isAutomaticFormatInitiallySelected()
                    && percentFormat.getValue().getValue().contains(documentScannerConf.getLocale())) {
                menuItem.setSelected(true);
            }
        }
        JRadioButtonMenuItem currencyFormatAutomaticMenuItem = new NumberFormatMenuItem(null);
        currencyFormatPopup.add(currencyFormatAutomaticMenuItem);
        currencyFormatPopupButtonGroup.add(currencyFormatAutomaticMenuItem);
        if(documentScannerConf.isAutomaticFormatInitiallySelected()) {
            currencyFormatAutomaticMenuItem.setSelected(true);
        }
        for(Map.Entry<String, Pair<NumberFormat, Set<Locale>>> currencyFormat : currencyFormats.entrySet()) {
            JRadioButtonMenuItem menuItem = new NumberFormatMenuItem(currencyFormat.getValue().getKey());
            currencyFormatPopup.add(menuItem);
            currencyFormatPopupButtonGroup.add(menuItem);
            if(!documentScannerConf.isAutomaticFormatInitiallySelected()
                    && currencyFormat.getValue().getValue().contains(documentScannerConf.getLocale())) {
                menuItem.setSelected(true);
            }
        }
        JRadioButtonMenuItem dateFormatAutomaticMenuItem = new DateFormatMenuItem(null);
        dateFormatPopup.add(dateFormatAutomaticMenuItem);
        dateFormatPopupButtonGroup.add(dateFormatAutomaticMenuItem);
        if(documentScannerConf.isAutomaticFormatInitiallySelected()) {
            dateFormatAutomaticMenuItem.setSelected(true);
        }
        for(Map.Entry<String, Pair<DateFormat, Set<Locale>>> dateFormat : dateFormats.entrySet()) {
            JRadioButtonMenuItem menuItem = new DateFormatMenuItem(dateFormat.getValue().getKey());
            dateFormatPopup.add(menuItem);
            dateFormatPopupButtonGroup.add(menuItem);
            if(!documentScannerConf.isAutomaticFormatInitiallySelected()
                    && dateFormat.getValue().getValue().contains(documentScannerConf.getLocale())) {
                menuItem.setSelected(true);
            }
        }
        JRadioButtonMenuItem timeFormatAutomaticMenuItem = new DateFormatMenuItem(null);
        timeFormatPopup.add(timeFormatAutomaticMenuItem);
        timeFormatPopupButtonGroup.add(timeFormatAutomaticMenuItem);
        if(documentScannerConf.isAutomaticFormatInitiallySelected()) {
            timeFormatAutomaticMenuItem.setSelected(true);
        }
        for(Map.Entry<String, Pair<DateFormat, Set<Locale>>> timeFormat : timeFormats.entrySet()) {
            JRadioButtonMenuItem menuItem = new DateFormatMenuItem(timeFormat.getValue().getKey());
            timeFormatPopup.add(menuItem);
            timeFormatPopupButtonGroup.add(menuItem);
            if(!documentScannerConf.isAutomaticFormatInitiallySelected()
                    && timeFormat.getValue().getValue().contains(documentScannerConf.getLocale())) {
                menuItem.setSelected(true);
            }
        }
        JRadioButtonMenuItem dateTimeFormatAutomaticMenuItem = new DateFormatMenuItem(null);
        dateTimeFormatPopup.add(dateTimeFormatAutomaticMenuItem);
        dateTimeFormatPopupButtonGroup.add(dateTimeFormatAutomaticMenuItem);
        if(documentScannerConf.isAutomaticFormatInitiallySelected()) {
            dateTimeFormatAutomaticMenuItem.setSelected(true);
        }
        Set<Entry<DateFormat, Set<Locale>>> dateTimeFormats = FormatUtils.getDisjointDateTimeFormats().entrySet();
        for(Entry<DateFormat, Set<Locale>> dateTimeFormat : dateTimeFormats) {
            JRadioButtonMenuItem menuItem = new DateFormatMenuItem(dateTimeFormat.getKey());
            dateTimeFormatPopup.add(menuItem);
            dateTimeFormatPopupButtonGroup.add(menuItem);
            if(!documentScannerConf.isAutomaticFormatInitiallySelected()
                    && dateTimeFormat.getValue().contains(documentScannerConf.getLocale())) {
                menuItem.setSelected(true);
            }
        }
        this.trimWhitespaceCheckBox.setSelected(documentScannerConf.isTrimWhitespace());
        this.trimWhitespaceCheckBox.addActionListener((event) -> {
            if(documentScannerConf.isRememberTrimWhitespace()) {
                documentScannerConf.setTrimWhitespace(true);
            }
        });
    }

    @Override
    public MainPanel getMainPanel() {
        return mainPanel;
    }

    @Override
    public JTextArea getoCRResultTextArea() {
        return this.oCRResultTextArea;
    }

    @Override
    public JCheckBox getTrimWhitespaceCheckBox() {
        return trimWhitespaceCheckBox;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        oCRResultPopupPasteIntoMenu.setText("Paste into");
        oCRResultPopup.add(oCRResultPopupPasteIntoMenu);

        oCRResultLabel.setText("OCR result");

        oCRResultTextArea.setColumns(20);
        oCRResultTextArea.setRows(5);
        oCRResultTextArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                oCRResultTextAreaMouseClicked(evt);
            }
        });
        oCRResultTextAreaScrollPane.setViewportView(oCRResultTextArea);

        toolbar.setRollover(true);
        toolbar.setToolTipText("formats");

        numberFormatButton.setText("Number");
        numberFormatButton.setFocusable(false);
        numberFormatButton.setHorizontalTextPosition(SwingConstants.CENTER);
        numberFormatButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        numberFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                numberFormatButtonActionPerformed(evt);
            }
        });
        toolbar.add(numberFormatButton);

        percentFormatButton.setText("Percentage");
        percentFormatButton.setFocusable(false);
        percentFormatButton.setHorizontalTextPosition(SwingConstants.CENTER);
        percentFormatButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        percentFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                percentFormatButtonActionPerformed(evt);
            }
        });
        toolbar.add(percentFormatButton);

        currencyFormatButton.setText("Currency");
        currencyFormatButton.setFocusable(false);
        currencyFormatButton.setHorizontalTextPosition(SwingConstants.CENTER);
        currencyFormatButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        currencyFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                currencyFormatButtonActionPerformed(evt);
            }
        });
        toolbar.add(currencyFormatButton);

        dateFormatButton.setText("Date");
        dateFormatButton.setFocusable(false);
        dateFormatButton.setHorizontalTextPosition(SwingConstants.CENTER);
        dateFormatButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        dateFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                dateFormatButtonActionPerformed(evt);
            }
        });
        toolbar.add(dateFormatButton);

        timeFormatButton.setText("Time");
        timeFormatButton.setFocusable(false);
        timeFormatButton.setHorizontalTextPosition(SwingConstants.CENTER);
        timeFormatButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        timeFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                timeFormatButtonActionPerformed(evt);
            }
        });
        toolbar.add(timeFormatButton);

        dateTimeFormatButton.setText("Date and Time");
        dateTimeFormatButton.setFocusable(false);
        dateTimeFormatButton.setHorizontalTextPosition(SwingConstants.CENTER);
        dateTimeFormatButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        dateTimeFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                dateTimeFormatButtonActionPerformed(evt);
            }
        });
        toolbar.add(dateTimeFormatButton);

        trimWhitespaceCheckBox.setText("Trim whitespace off copied text");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(oCRResultTextAreaScrollPane)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(oCRResultLabel)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(trimWhitespaceCheckBox)))
                .addContainerGap())
            .addComponent(toolbar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(oCRResultLabel)
                    .addComponent(trimWhitespaceCheckBox))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(oCRResultTextAreaScrollPane, GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(toolbar, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void oCRResultTextAreaMouseClicked(MouseEvent evt) {//GEN-FIRST:event_oCRResultTextAreaMouseClicked
        if(evt.getButton() == MouseEvent.BUTTON3) {
            //right click
            this.oCRResultPopup.show(this.oCRResultTextArea, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_oCRResultTextAreaMouseClicked

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void numberFormatButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_numberFormatButtonActionPerformed
        numberFormatPopup.show(toolbar, 0, 0);
    }//GEN-LAST:event_numberFormatButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void percentFormatButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_percentFormatButtonActionPerformed
        percentFormatPopup.show(toolbar, 0, 0);
    }//GEN-LAST:event_percentFormatButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void currencyFormatButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_currencyFormatButtonActionPerformed
        currencyFormatPopup.show(toolbar, 0, 0);
    }//GEN-LAST:event_currencyFormatButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void dateFormatButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_dateFormatButtonActionPerformed
        dateFormatPopup.show(toolbar, 0, 0);
    }//GEN-LAST:event_dateFormatButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void timeFormatButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_timeFormatButtonActionPerformed
        timeFormatPopup.show(toolbar, 0, 0);
    }//GEN-LAST:event_timeFormatButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void dateTimeFormatButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_dateTimeFormatButtonActionPerformed
        dateTimeFormatPopup.show(toolbar, 0, 0);
    }//GEN-LAST:event_dateTimeFormatButtonActionPerformed
}
