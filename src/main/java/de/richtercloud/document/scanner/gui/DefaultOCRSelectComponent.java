/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.ifaces.EntityPanel;
import de.richtercloud.document.scanner.ifaces.ImageWrapperException;
import de.richtercloud.document.scanner.ifaces.MainPanel;
import de.richtercloud.document.scanner.ifaces.OCREngine;
import de.richtercloud.document.scanner.ifaces.OCRSelectComponent;
import de.richtercloud.document.scanner.ifaces.OCRSelectPanelPanel;
import de.richtercloud.document.scanner.ifaces.ProgressButton;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Set;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class DefaultOCRSelectComponent extends OCRSelectComponent {
    private static final long serialVersionUID = 1L;
    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultOCRSelectComponent.class);
    private final OCRSelectPanelPanel oCRSelectPanelPanel;
    private final JToolBar toolbar = new JToolBar(SwingConstants.VERTICAL);
    private final JButton zoomInButton = new JButton("+");
    private final JButton zoomOutButton = new JButton("-");
    private final JButton valueDetectionResultsButton = new JButton("Set auto detection results on form");
    private final ProgressButton valueDetectionButton = new DefaultProgressButton("(Re-)Run auto detection");
    private final JCheckBox valueDetectionCheckBox = new JCheckBox("Show auto detection components on form");
    @SuppressWarnings("PMD.ImmutableField")
        //probably a false positive
    private float zoomLevel = 1;
    private final EntityPanel entityPanel;
    private final OCREngine oCREngine;
    private final DocumentScannerConf documentScannerConf;
    private final File file;
    private final MainPanel mainPanel;

    public DefaultOCRSelectComponent(MainPanel mainPanel,
            OCRSelectPanelPanel oCRSelectPanelPanel,
            EntityPanel entityPanel,
            OCREngine oCREngine,
            DocumentScannerConf documentScannerConf,
            final Set<JPanel> valueDetectionPanels,
            File file,
            IssueHandler issueHandler) {
        super();
        if(mainPanel == null) {
            throw new IllegalArgumentException("mainPanel mustn't be null");
        }
        this.mainPanel = mainPanel;
        this.oCRSelectPanelPanel = oCRSelectPanelPanel;
        this.entityPanel = entityPanel;
        this.oCREngine = oCREngine;
        this.documentScannerConf = documentScannerConf;
        this.file = file;

        //Simulate a multiline toolbar rather than implement something that is
        //already available in JavaFX in which the application will be ported
        //sooner or later anyway
        GroupLayout toolbarLayout = new GroupLayout(toolbar);
        toolbar.setLayout(toolbarLayout);
        JPanel toolbarPanel = new JPanel(new WrapLayout(WrapLayout.LEADING, 5, 5)
                //FlowLayout isn't sufficient
        );
        toolbarPanel.add(zoomInButton);
        toolbarPanel.add(zoomOutButton);
        toolbarPanel.add(valueDetectionResultsButton);
        toolbarPanel.add(valueDetectionButton);
        toolbarPanel.add(valueDetectionCheckBox);
        JScrollPane toolbarPanelScrollPane = new JScrollPane(toolbarPanel);
        toolbarPanelScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        toolbarPanelScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        toolbarLayout.setHorizontalGroup(toolbarLayout.createSequentialGroup().addComponent(toolbarPanelScrollPane));
        toolbarLayout.setVerticalGroup(toolbarLayout.createSequentialGroup().addComponent(toolbarPanelScrollPane));
        toolbar.setFloatable(false);

        OCRSelectPanelPanelScrollPane oCRSelectPanelPanelScrollPane =
                new OCRSelectPanelPanelScrollPane(oCRSelectPanelPanel);
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup()
                .addComponent(oCRSelectPanelPanelScrollPane,
                        0,
                        GroupLayout.PREFERRED_SIZE,
                        Short.MAX_VALUE)
                .addComponent(toolbar,
                        0, //allows toolbar to be resizable horizontally
                        GroupLayout.PREFERRED_SIZE,
                        Short.MAX_VALUE //needs to be able to grow infinitely
                ));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(oCRSelectPanelPanelScrollPane,
                        0,
                        GroupLayout.PREFERRED_SIZE,
                        Short.MAX_VALUE)
                .addComponent(toolbar,
                        GroupLayout.PREFERRED_SIZE, //DEFAULT_SIZE causes following lines to be invisible
                        GroupLayout.PREFERRED_SIZE,
                        Short.MAX_VALUE));

        zoomInButton.addActionListener(new ActionListener() {
            @Override
            @SuppressWarnings("PMD.AvoidCatchingThrowable")
            public void actionPerformed(ActionEvent e) {
                try {
                    float zoomLevelOld = DefaultOCRSelectComponent.this.zoomLevel;
                    zoomLevel /= DefaultOCRSelectComponent.this.documentScannerConf.getZoomLevelMultiplier();
                    oCRSelectPanelPanel.setZoomLevels(DefaultOCRSelectComponent.this.zoomLevel);
                    if(documentScannerConf.isRememberPreferredOCRSelectPanelWidth()) {
                        int preferredOCRSelectPanelWidthNew = (int) (documentScannerConf.getPreferredOCRSelectPanelWidth()*zoomLevel/zoomLevelOld);
                        documentScannerConf.setPreferredOCRSelectPanelWidth(preferredOCRSelectPanelWidthNew);
                    }
                } catch (Throwable ex) {
                    LOGGER.error("unexpected exception during zooming in",
                            ex);
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                }
            }
        });
        zoomOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    float zoomLevelOld = DefaultOCRSelectComponent.this.zoomLevel;
                    zoomLevel *= DefaultOCRSelectComponent.this.documentScannerConf.getZoomLevelMultiplier();
                    oCRSelectPanelPanel.setZoomLevels(DefaultOCRSelectComponent.this.zoomLevel);
                    //zooming out requires a scroll event to occur in order to
                    //paint other pages than the first only; revalidate doesn't
                    //help
                    if(documentScannerConf.isRememberPreferredOCRSelectPanelWidth()) {
                        int preferredOCRSelectPanelWidthNew = (int) (documentScannerConf.getPreferredOCRSelectPanelWidth()*zoomLevel/zoomLevelOld);
                        documentScannerConf.setPreferredOCRSelectPanelWidth(preferredOCRSelectPanelWidthNew);
                    }
                } catch (ImageWrapperException ex) {
                    LOGGER.error("unexpected exception during zooming out",
                            ex);
                    issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
                }
            }
        });
        valueDetectionResultsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultOCRSelectComponent.this.entityPanel.setEnabled(false);
                    //will be re-enabled in ValueDetectionListener
                DefaultOCRSelectComponent.this.entityPanel.valueDetection(new DefaultOCRSelectPanelPanelFetcher(DefaultOCRSelectComponent.this.getoCRSelectPanelPanel(),
                        DefaultOCRSelectComponent.this.oCREngine),
                        false //forceRenewal
                );
            }
        });
        valueDetectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultOCRSelectComponent.this.valueDetectionButton.setEnabled(false);
                DefaultOCRSelectComponent.this.entityPanel.valueDetection(new DefaultOCRSelectPanelPanelFetcher(DefaultOCRSelectComponent.this.getoCRSelectPanelPanel(),
                        DefaultOCRSelectComponent.this.oCREngine),
                        true //forceRenewal
                );
            }
        });
        valueDetectionCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(JPanel valueDetectionPanel : valueDetectionPanels) {
                    valueDetectionPanel.setVisible(valueDetectionCheckBox.isSelected());
                }
            }
        });
        valueDetectionCheckBox.setSelected(true); //should trigger
            //action listener above
    }

    @Override
    public MainPanel getMainPanel() {
        return mainPanel;
    }

    @Override
    public ProgressButton getValueDetectionButton() {
        return valueDetectionButton;
    }

    @Override
    public OCRSelectPanelPanel getoCRSelectPanelPanel() {
        return oCRSelectPanelPanel;
    }

    @Override
    public File getFile() {
        return file;
    }
}
