/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui.storageconf;

import de.richtercloud.document.scanner.gui.DocumentScanner;
import de.richtercloud.document.scanner.gui.DocumentScannerUtils;
import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.jhbuild.java.wrapper.ActionOnMissingBinary;
import de.richtercloud.jhbuild.java.wrapper.BuildFailureException;
import de.richtercloud.jhbuild.java.wrapper.JHBuildJavaWrapper;
import de.richtercloud.jhbuild.java.wrapper.MissingSystemBinaryException;
import de.richtercloud.jhbuild.java.wrapper.ModuleBuildFailureException;
import de.richtercloud.jhbuild.java.wrapper.OutputMode;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadException;
import de.richtercloud.jhbuild.java.wrapper.download.Downloader;
import de.richtercloud.jhbuild.java.wrapper.download.ExtractionException;
import de.richtercloud.jhbuild.java.wrapper.download.GUIDownloader;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.jpa.storage.PostgresqlAutoPersistenceStorageConf;
import de.richtercloud.swing.worker.get.wait.dialog.SwingWorkerGetWaitDialog;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import javax.swing.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class PostgresqlAutoPersistenceStorageConfPanel extends StorageConfPanel<PostgresqlAutoPersistenceStorageConf> {
    private static final long serialVersionUID = 1L;
    private final static Logger LOGGER = LoggerFactory.getLogger(PostgresqlAutoPersistenceStorageConfPanel.class);
    private final static String DOWNLOAD_DIR_NAME = "postgresql";
    private final static String INSTALL_PREFIX_DIR_NAME = "postgresql-install";
    private final PostgresqlAutoPersistenceStorageConf storageConf;
    private final DocumentScannerConf documentScannerConf;
    private final IssueHandler issueHandler;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JTextField baseDirTextField = new JTextField();
    private final JLabel baseDirTextFieldLabel = new JLabel();
    private final JTextField createdbBinaryPathTextField = new JTextField();
    private final JLabel createdbBinaryPathTextFieldLabel = new JLabel();
    private final JTextField databaseDirTextField = new JTextField();
    private final JLabel databaseDirTextFieldLabel = new JLabel();
    private final JTextField databaseNameTextField = new JTextField();
    private final JLabel databaseNameTextFieldLabel = new JLabel();
    private final JSeparator directorySeparator = new JSeparator();
    private final JButton downloadButton = new JButton();
    private final JTextField hostnameTextField = new JTextField();
    private final JLabel hostnameTextFieldLabel = new JLabel();
    private final JTextField initdbBinaryPathTextField = new JTextField();
    private final JLabel initdbBinaryPathTextFieldLabel = new JLabel();
    private final JLabel passwordLabel = new JLabel();
    private final JPasswordField passwordPasswordField = new JPasswordField();
    private final JSpinner portSpinner = new JSpinner();
    private final JLabel portSpinnerLabel = new JLabel();
    private final JTextField postgresBinaryPathTextField = new JTextField();
    private final JLabel postgresBinaryPathTextFieldLabel = new JLabel();
    private final JLabel usernameLabel = new JLabel();
    private final JTextField usernameTextField = new JTextField();
    // End of variables declaration//GEN-END:variables

    public PostgresqlAutoPersistenceStorageConfPanel(PostgresqlAutoPersistenceStorageConf storageConf,
            DocumentScannerConf documentScannerConf,
            IssueHandler issueHandler) {
        super();
        initComponents();
        if(storageConf == null) {
            throw new IllegalArgumentException("storageConf mustn't be null");
        }
        this.storageConf = storageConf;
        this.documentScannerConf = documentScannerConf;
        this.issueHandler = issueHandler;
        this.databaseNameTextField.setText(storageConf.getDatabaseName());
        this.databaseDirTextField.setText(storageConf.getDatabaseDir());
        this.hostnameTextField.setText(storageConf.getHostname());
        this.portSpinner.setValue(storageConf.getPort());
        this.usernameTextField.setText(storageConf.getUsername());
        this.initdbBinaryPathTextField.setText(storageConf.getInitdbBinaryPath());
        this.postgresBinaryPathTextField.setText(storageConf.getPostgresBinaryPath());
        this.createdbBinaryPathTextField.setText(storageConf.getCreatedbBinaryPath());
    }

    @Override
    public PostgresqlAutoPersistenceStorageConf getStorageConf() {
        return this.storageConf;
    }

    @Override
    public void save() {
        this.storageConf.setDatabaseName(this.databaseNameTextField.getText());
        this.storageConf.setDatabaseDir(this.databaseDirTextField.getText());
        this.storageConf.setHostname(this.hostnameTextField.getText());
        this.storageConf.setPort((int) this.portSpinner.getValue());
        String username = this.usernameTextField.getText();
        this.storageConf.setUsername(username);
        String password = String.valueOf(this.passwordPasswordField.getPassword());
        this.storageConf.setPassword(password);
        this.storageConf.setInitdbBinaryPath(this.initdbBinaryPathTextField.getText());
        this.storageConf.setPostgresBinaryPath(this.postgresBinaryPathTextField.getText());
        this.storageConf.setCreatedbBinaryPath(this.createdbBinaryPathTextField.getText());
    }

    @Override
    public void cancel() {
        //do nothing
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        databaseNameTextFieldLabel.setText("Database name");

        hostnameTextFieldLabel.setText("Hostname");

        usernameLabel.setText("Username");

        passwordLabel.setText("Password");

        passwordPasswordField.setText("jPasswordField1");

        portSpinnerLabel.setText("Port");

        databaseDirTextFieldLabel.setText("Database directory");

        initdbBinaryPathTextFieldLabel.setText("initdb binary path");

        postgresBinaryPathTextFieldLabel.setText("postgres binary path");

        downloadButton.setText("Download");
        downloadButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadButtonActionPerformed(evt);
            }
        });

        baseDirTextFieldLabel.setText("Base directory");

        createdbBinaryPathTextFieldLabel.setText("createdb binary path");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(portSpinnerLabel)
                            .addComponent(hostnameTextFieldLabel)
                            .addComponent(databaseDirTextFieldLabel)
                            .addComponent(databaseNameTextFieldLabel)
                            .addComponent(usernameLabel)
                            .addComponent(passwordLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(databaseNameTextField)
                            .addComponent(databaseDirTextField)
                            .addComponent(hostnameTextField)
                            .addComponent(portSpinner)
                            .addComponent(usernameTextField)
                            .addComponent(passwordPasswordField, GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(directorySeparator)
                        .addContainerGap())
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(downloadButton)
                        .addGap(12, 12, 12))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(postgresBinaryPathTextFieldLabel)
                            .addComponent(initdbBinaryPathTextFieldLabel)
                            .addComponent(baseDirTextFieldLabel)
                            .addComponent(createdbBinaryPathTextFieldLabel))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(createdbBinaryPathTextField, GroupLayout.Alignment.TRAILING)
                            .addComponent(initdbBinaryPathTextField)
                            .addComponent(postgresBinaryPathTextField)
                            .addComponent(baseDirTextField))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(baseDirTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(baseDirTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downloadButton)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(initdbBinaryPathTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(initdbBinaryPathTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(postgresBinaryPathTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(postgresBinaryPathTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(createdbBinaryPathTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(createdbBinaryPathTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(directorySeparator, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(databaseNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(databaseNameTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(databaseDirTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(databaseDirTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(hostnameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(hostnameTextFieldLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(portSpinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(portSpinnerLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(usernameLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordLabel))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.AvoidCatchingThrowable"})
    private void downloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadButtonActionPerformed
        try {
            assert documentScannerConf.getBinaryDownloadDir() != null;
            File installationPrefixDir = new File(documentScannerConf.getBinaryDownloadDir(),
                        //points to installation prefixes as well
                    INSTALL_PREFIX_DIR_NAME);
            File downloadDir = new File(documentScannerConf.getBinaryDownloadDir(),
                    DOWNLOAD_DIR_NAME);
            Downloader downloader = new GUIDownloader(SwingUtilities.getWindowAncestor(this),
                DocumentScanner.generateApplicationWindowTitle("Downloading MySQL",
                        DocumentScannerUtils.APP_NAME,
                        DocumentScannerUtils.APP_VERSION), //downloadDialogTitle
                "Downloading MySQL", //labelText
                "Downloading MySQL" //progressBarText
            );
            JHBuildJavaWrapper jHBuildJavaWrapper = new JHBuildJavaWrapper(installationPrefixDir,
                    downloadDir,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    ActionOnMissingBinary.DOWNLOAD,
                    downloader, //downloader
                    false, //skipMD5Check
                    System.out,
                    System.err,
                    OutputMode.JOIN_STDOUT_STDERR,
                    -1, //outputLimit
                    true //exitOnError
            );
            final SwingWorkerGetWaitDialog dialog = new SwingWorkerGetWaitDialog(SwingUtilities.getWindowAncestor(this), //parent
                    "Building PostgreSQL", //dialogTitle
                    "Building PostgreSQL", //labelText,
                    "Building PostgreSQL" //progressBarText
            );
            SwingWorker<Boolean, Void> downloadWorker = new SwingWorker<Boolean, Void>() {
                @Override
                protected Boolean doInBackground() {
                    try {
                        jHBuildJavaWrapper.installModuleset("postgresql-10.5" //moduleName
                        );
                        return true;
                    } catch (IOException
                            | ExtractionException
                            | InterruptedException
                            | MissingSystemBinaryException
                            | BuildFailureException
                            | ModuleBuildFailureException
                            | DownloadException ex) {
                        //all not critical
                        issueHandler.handle(new ExceptionMessage(ex));
                        return false;
                    }
                }

                @Override
                protected void done() {
                    dialog.setVisible(false);
                }
            };
            downloadWorker.execute();
            dialog.setVisible(true);
            if(dialog.isCanceled()) {
                jHBuildJavaWrapper.cancelInstallModuleset();
                return;
            }
            try {
                downloadWorker.get();
            } catch (InterruptedException | ExecutionException ex) {
                LOGGER.error("unexpected exception during download of PostgreSQL occured",
                        ex);
                issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
            }
            baseDirTextField.setText(installationPrefixDir.getAbsolutePath());
            initdbBinaryPathTextField.setText(new File(installationPrefixDir, String.join(File.separator, "bin", "initdb")).getAbsolutePath());
            postgresBinaryPathTextField.setText(new File(installationPrefixDir, String.join(File.separator, "bin", "postgres")).getAbsolutePath());
            createdbBinaryPathTextField.setText(new File(installationPrefixDir, String.join(File.separator, "bin", "createdb")).getAbsolutePath());
        }catch(Throwable ex) {
            LOGGER.error("unexpected exception during download of PostgreSQL occured",
                    ex);
            issueHandler.handleUnexpectedException(new ExceptionMessage(ex));
        }
    }//GEN-LAST:event_downloadButtonActionPerformed
}
