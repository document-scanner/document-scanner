/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import com.google.common.collect.ImmutableSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
internal implementation notes:
- originally methods have been implemented to initialize return values lazily
which slows down the application during operations instead of slowing it down at
start -> keep implementation, but invoke all methods in static block in order to
trigger cache filling
*/
/**
 * Provides different {@link NumberFormat}s and {@link DateFormat}s in order to
 * avoid handling this provision in static methods and variables in classes.
 *
 * Note that since Java doesn't have a useful way to manage currencies and
 * JScience is used instead (which has a quite limited set of currencies),
 * available currencies ought to be retrieved from a
 * {@link de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorage}.
 *
 * @author richter
 */
@SuppressWarnings("PMD.NonThreadSafeSingleton")
public final class FormatUtils {
    private final static Logger LOGGER = LoggerFactory.getLogger(FormatUtils.class);
    public final static double NUMBER_FORMAT_VALUE = -12345.987;
    public final static Date DATE_FORMAT_VALUE = new Date();
    public final static Set<Integer> DATE_FORMAT_INTS = new HashSet<>(Arrays.asList(DateFormat.FULL, DateFormat.LONG, DateFormat.MEDIUM, DateFormat.SHORT));
    private final static String RE_WHITESPACE = "[\\s]+";
    private static Set<DateFormat> allDateFormats;
    private static Set<DateFormat> allTimeFormats;
    private static Set<DateFormat> allDateTimeFormats;
    private static Set<DateFormat> allDateRelatedFormats;
    private static Map<DateFormat, Set<Locale>> disjointDateTimeFormats;
    private static Map<DateFormat, Set<Locale>> disjointDateFormats;
    private static Map<DateFormat, Set<Locale>> disjointTimeFormats;
    private static Map<DateFormat, Set<Locale>> disjointDateRelatedFormats;
    private static Set<NumberFormat> allCurrencyFormats;
    private static Map<NumberFormat, Set<Locale>> disjointCurrencyFormats;
    private final static Lock ALL_DATE_RELATED_FORMATS_LOCK = new ReentrantLock();
    private final static Lock ALL_DATE_TIME_FORMATS_LOCK = new ReentrantLock();
    private final static Lock ALL_DATE_FORMATS_LOCK = new ReentrantLock();
    private final static Lock ALL_TIME_FORMATS_LOCK = new ReentrantLock();
    private final static Lock DISJOINT_DATE_TIME_FORMATS_LOCK = new ReentrantLock();
    private final static Lock DISJOINT_DATE_FORMATS_LOCK = new ReentrantLock();
    private final static Lock DISJOINT_TIME_FORMATS_LOCK = new ReentrantLock();
    private final static Lock DISJOINT_DATE_RELATED_FORMATS_LOCK = new ReentrantLock();
    private final static Lock ALL_CURRENCY_FORMATS_LOCK = new ReentrantLock();
    private final static Lock DISJOINT_CURRENCY_FORMATS_LOCK = new ReentrantLock();

    /**
     * Initialized cached values before first access to speed up the latter.
     *
     * This should not be called from within a {@code static} block in this or another class because parallel execution
     * will block forever (see
     * https://stackoverflow.com/questions/54060398/why-does-executorservice-not-process-submitted-tasks-when-started-from-class-loa
     * for an explanation).
     *
     * @throws InterruptedException if such an exception occurs
     */
    public static void init() throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(() -> getAllDateRelatedFormats());
        executorService.submit(() -> getAllDateTimeFormats());
        executorService.submit(() -> getAllDateFormats());
        executorService.submit(() -> getAllTimeFormats());
        executorService.submit(() -> getDisjointDateTimeFormats());
        executorService.submit(() -> getDisjointDateFormats());
        executorService.submit(() -> getDisjointTimeFormats());
        executorService.submit(() -> getDisjointDateRelatedFormats());
        executorService.submit(() -> getAllCurrencyFormats());
        executorService.submit(() -> getDisjointCurrencyFormats());
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
    }

    public static Set<DateFormat> getAllDateRelatedFormats() {
        ALL_DATE_RELATED_FORMATS_LOCK.lock();
        try {
            if (allDateRelatedFormats == null) {
                allDateRelatedFormats = new HashSet<>();
                allDateRelatedFormats.addAll(getAllDateFormats());
                allDateRelatedFormats.addAll(getAllTimeFormats());
                allDateRelatedFormats.addAll(getAllDateTimeFormats());
                allDateRelatedFormats = Collections.unmodifiableSet(allDateRelatedFormats);
            }
            return allDateRelatedFormats;
        }finally {
            ALL_DATE_RELATED_FORMATS_LOCK.unlock();
        }
    }

    /**
     * Lazily creates all available date-time formats (all combinations of
     * available {@link Locale}s and combination of two times the set of
     * {@link #DATE_FORMAT_INTS}. The set in only generated once and then
     * cached.
     * @return all available date-time formats
     */
    public static Set<DateFormat> getAllDateTimeFormats() {
        ALL_DATE_TIME_FORMATS_LOCK.lock();
        try {
            if (allDateTimeFormats == null) {
                allDateTimeFormats = new HashSet<>();
                for (Locale locale : Locale.getAvailableLocales()) {
                    for (int formatInt : DATE_FORMAT_INTS) {
                        for (int formatInt1 : DATE_FORMAT_INTS) {
                            DateFormat dateFormat = DateFormat.getDateTimeInstance(formatInt, formatInt1, locale);
                            allDateTimeFormats.add(dateFormat);
                        }
                    }
                }
                allDateTimeFormats = Collections.unmodifiableSet(allDateTimeFormats);
            }
            return allDateTimeFormats;
        }finally {
            ALL_DATE_TIME_FORMATS_LOCK.unlock();
        }
    }

    public static Set<DateFormat> getAllDateFormats() {
        ALL_DATE_FORMATS_LOCK.lock();
        try {
            if (allDateFormats == null) {
                allDateFormats = new HashSet<>();
                for (Locale locale : Locale.getAvailableLocales()) {
                    for (int formatInt : DATE_FORMAT_INTS) {
                        DateFormat dateFormat = DateFormat.getDateInstance(formatInt, locale);
                        allDateFormats.add(dateFormat);
                    }
                }
                allDateFormats = Collections.unmodifiableSet(allDateFormats);
            }
            return allDateFormats;
        }finally {
            ALL_DATE_FORMATS_LOCK.unlock();
        }
    }

    public static Set<DateFormat> getAllTimeFormats() {
        ALL_TIME_FORMATS_LOCK.lock();
        try {
            if (allTimeFormats == null) {
                allTimeFormats = new HashSet<>();
                for (Locale locale : Locale.getAvailableLocales()) {
                    for (int formatInt : DATE_FORMAT_INTS) {
                        DateFormat dateFormat = DateFormat.getTimeInstance(formatInt, locale);
                        allTimeFormats.add(dateFormat);
                    }
                }
                allTimeFormats = Collections.unmodifiableSet(allTimeFormats);
            }
            return allTimeFormats;
        }finally {
            ALL_TIME_FORMATS_LOCK.unlock();
        }
    }

    /**
     * Get all disjoint date, time and date-time formats. Note that some date
     * (sub)strings aren't recognized by date-time formats, i.e. date and time
     * formats should be used as well for recognizing date and time in random
     * input.
     *
     * @return the disjoint mapping
     * @see #getDisjointDateTimeFormats()
     */
    public static Map<DateFormat, Set<Locale>> getDisjointDateRelatedFormats() {
        DISJOINT_DATE_RELATED_FORMATS_LOCK.lock();
        try {
            if (disjointDateRelatedFormats == null) {
                disjointDateRelatedFormats = new HashMap<>();
                disjointDateRelatedFormats.putAll(getDisjointTimeFormats());
                disjointDateRelatedFormats.putAll(getDisjointDateFormats());
                disjointDateRelatedFormats.putAll(getDisjointDateTimeFormats());
                disjointDateRelatedFormats = Collections.unmodifiableMap(disjointDateRelatedFormats);
            }
            return disjointDateRelatedFormats;
        }finally {
            DISJOINT_DATE_RELATED_FORMATS_LOCK.unlock();
        }
    }

    public static Map<DateFormat, Set<Locale>> getDisjointDateFormats() {
        DISJOINT_DATE_FORMATS_LOCK.lock();
        try {
            if (disjointDateFormats == null) {
                Map<String, Pair<DateFormat, Set<Locale>>> dateFormats = new HashMap<>();
                Iterator<Locale> localeIterator = new ArrayList<>(Arrays.asList(Locale.getAvailableLocales())).iterator();
                Locale firstLocale = localeIterator.next();
                for (int formatInt : DATE_FORMAT_INTS) {
                    String dateString = DateFormat.getDateInstance(formatInt, firstLocale).format(DATE_FORMAT_VALUE);
                    dateFormats.put(dateString,
                            new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getDateInstance(formatInt, firstLocale),
                                    new HashSet<>(Arrays.asList(firstLocale))));
                }
                while (localeIterator.hasNext()) {
                    Locale locale = localeIterator.next();
                    for (int formatInt : DATE_FORMAT_INTS) {
                        String dateString = DateFormat.getDateInstance(formatInt, locale).format(DATE_FORMAT_VALUE);
                        Pair<DateFormat, Set<Locale>> dateFormatsPair = dateFormats.get(dateString);
                        if (dateFormatsPair == null) {
                            dateFormatsPair = new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getDateInstance(formatInt, locale),
                                    new HashSet<Locale>());
                            dateFormats.put(dateString, dateFormatsPair);
                        }
                        Set<Locale> dateFormatsLocales = dateFormatsPair.getValue();
                        dateFormatsLocales.add(locale);
                    }
                }
                disjointDateFormats = new HashMap<>();
                for (Pair<DateFormat, Set<Locale>> dateFormatsPair : dateFormats.values()) {
                    disjointDateFormats.put(dateFormatsPair.getKey(),
                            dateFormatsPair.getValue());
                }
                disjointDateFormats = Collections.unmodifiableMap(disjointDateFormats);
            }
            return disjointDateFormats;
        }finally {
            DISJOINT_DATE_FORMATS_LOCK.unlock();
        }
    }

    public static Map<DateFormat, Set<Locale>> getDisjointTimeFormats() {
        DISJOINT_TIME_FORMATS_LOCK.lock();
        try {
            if (disjointTimeFormats == null) {
                Map<String, Pair<DateFormat, Set<Locale>>> timeFormats = new HashMap<>();
                Iterator<Locale> localeIterator = new ArrayList<>(Arrays.asList(Locale.getAvailableLocales())).iterator();
                Locale firstLocale = localeIterator.next();
                for (int formatInt : DATE_FORMAT_INTS) {
                    String timeString = DateFormat.getTimeInstance(formatInt, firstLocale).format(DATE_FORMAT_VALUE);
                    timeFormats.put(timeString,
                            new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getTimeInstance(formatInt, firstLocale),
                                    new HashSet<>(Arrays.asList(firstLocale))));
                }
                while (localeIterator.hasNext()) {
                    Locale locale = localeIterator.next();
                    for (int formatInt : DATE_FORMAT_INTS) {
                        String timeString = DateFormat.getTimeInstance(formatInt, locale).format(DATE_FORMAT_VALUE);
                        Pair<DateFormat, Set<Locale>> timeFormatsPair = timeFormats.get(timeString);
                        if (timeFormatsPair == null) {
                            timeFormatsPair = new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getTimeInstance(formatInt, locale),
                                    new HashSet<Locale>());
                            timeFormats.put(timeString, timeFormatsPair);
                        }
                        Set<Locale> timeFormatsLocales = timeFormatsPair.getValue();
                        timeFormatsLocales.add(locale);
                    }
                }
                disjointTimeFormats = new HashMap<>();
                for (Pair<DateFormat, Set<Locale>> timeFormatsPair : timeFormats.values()) {
                    disjointTimeFormats.put(timeFormatsPair.getKey(),
                            timeFormatsPair.getValue());
                }
                disjointTimeFormats = Collections.unmodifiableMap(disjointTimeFormats);
            }
            return disjointTimeFormats;
        }finally {
            DISJOINT_TIME_FORMATS_LOCK.unlock();
        }
    }

    public static Map<DateFormat, Set<Locale>> getDisjointDateTimeFormats() {
        DISJOINT_DATE_TIME_FORMATS_LOCK.lock();
        try {
            if (disjointDateTimeFormats == null) {
                Map<String, Pair<DateFormat, Set<Locale>>> dateTimeFormats = new HashMap<>();
                Iterator<Locale> localeIterator = new ArrayList<>(Arrays.asList(Locale.getAvailableLocales())).iterator();
                Locale firstLocale = localeIterator.next();
                for (int formatInt : DATE_FORMAT_INTS) {
                    for (int formatInt1 : DATE_FORMAT_INTS) {
                        String dateTimeString = DateFormat.getDateTimeInstance(formatInt, formatInt1, firstLocale).format(DATE_FORMAT_VALUE);
                        dateTimeFormats.put(dateTimeString,
                                new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getDateTimeInstance(formatInt, formatInt1, firstLocale),
                                        new HashSet<>(Arrays.asList(firstLocale))));
                    }
                }
                while (localeIterator.hasNext()) {
                    Locale locale = localeIterator.next();
                    for (int formatInt : DATE_FORMAT_INTS) {
                        for (int formatInt1 : DATE_FORMAT_INTS) {
                            String dateTimeString = DateFormat.getDateTimeInstance(formatInt, formatInt1, locale).format(DATE_FORMAT_VALUE);
                            Pair<DateFormat, Set<Locale>> dateTimeFormatsPair = dateTimeFormats.get(dateTimeString);
                            if (dateTimeFormatsPair == null) {
                                dateTimeFormatsPair = new ImmutablePair<DateFormat, Set<Locale>>(DateFormat.getDateTimeInstance(formatInt, formatInt1, locale),
                                        new HashSet<Locale>());
                                dateTimeFormats.put(dateTimeString, dateTimeFormatsPair);
                            }
                            Set<Locale> dateTimeFormatsLocales = dateTimeFormatsPair.getValue();
                            dateTimeFormatsLocales.add(locale);
                        }
                    }
                }
                disjointDateTimeFormats = new HashMap<>();
                for (Pair<DateFormat, Set<Locale>> dateTimeFormatsPair : dateTimeFormats.values()) {
                    disjointDateTimeFormats.put(dateTimeFormatsPair.getKey(),
                            dateTimeFormatsPair.getValue());
                }
                disjointDateTimeFormats = Collections.unmodifiableMap(disjointDateTimeFormats);
            }
            return disjointDateTimeFormats;
        }finally {
            DISJOINT_DATE_TIME_FORMATS_LOCK.unlock();
        }
    }

    public static Set<NumberFormat> getAllCurrencyFormats() {
        ALL_CURRENCY_FORMATS_LOCK.lock();
        try {
            if (allCurrencyFormats == null) {
                List<Locale> availableLocales = Arrays.asList(Locale.getAvailableLocales());
                allCurrencyFormats = availableLocales
                        .parallelStream()
                        .map(locale -> Currency.getAvailableCurrencies()
                                .parallelStream()
                                .map(currency -> {
                                    NumberFormat currencyFormat = new HashCodeOptimizedNumberFormat((DecimalFormat)NumberFormat.getCurrencyInstance(locale));
                                    treatCurrencyFormat(currencyFormat,
                                            currency);
                                    return currencyFormat;
                                })
                                .collect(Collectors.toList()))
                        .flatMap(Collection::parallelStream)
                        .collect(ImmutableSet.toImmutableSet());
            }
            return allCurrencyFormats;
        }finally {
            ALL_CURRENCY_FORMATS_LOCK.unlock();
        }
    }

    public static Map<NumberFormat, Set<Locale>> getDisjointCurrencyFormats() {
        DISJOINT_CURRENCY_FORMATS_LOCK.lock();
        try {
            if (disjointCurrencyFormats == null) {
                Map<String, Pair<NumberFormat, Set<Locale>>> currencyFormats = new HashMap<>();
                Iterator<Locale> localeIterator = new ArrayList<>(Arrays.asList(Locale.getAvailableLocales())).iterator();
                Locale firstLocale = localeIterator.next();
                for (Currency currency : Currency.getAvailableCurrencies()) {
                    NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(firstLocale);
                    treatCurrencyFormat(currencyFormat,
                            currency);
                    String currencyString = currencyFormat.format(NUMBER_FORMAT_VALUE);
                    currencyFormats.put(currencyString,
                            new ImmutablePair<NumberFormat, Set<Locale>>(currencyFormat,
                                    new HashSet<>(Arrays.asList(firstLocale))));
                }
                while (localeIterator.hasNext()) {
                    Locale locale = localeIterator.next();
                    for (Currency currency : Currency.getAvailableCurrencies()) {
                        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
                        currencyFormat.setCurrency(currency);
                        treatCurrencyFormat(currencyFormat,
                                currency);
                        String currencyString = currencyFormat.format(NUMBER_FORMAT_VALUE);
                        Pair<NumberFormat, Set<Locale>> currencyFormatsPair = currencyFormats.get(currencyString);
                        if (currencyFormatsPair == null) {
                            currencyFormatsPair = new ImmutablePair<NumberFormat, Set<Locale>>(currencyFormat,
                                    new HashSet<Locale>());
                            currencyFormats.put(currencyString, currencyFormatsPair);
                        }
                        Set<Locale> currencyFormatsLocales = currencyFormatsPair.getValue();
                        currencyFormatsLocales.add(locale);
                    }
                }
                LOGGER.debug(Arrays.toString(currencyFormats.entrySet().toArray()));
                disjointCurrencyFormats = new HashMap<>();
                for (Pair<NumberFormat, Set<Locale>> currencyFormatsPair : currencyFormats.values()) {
                    disjointCurrencyFormats.put(currencyFormatsPair.getKey(),
                            currencyFormatsPair.getValue());
                }
                disjointCurrencyFormats = Collections.unmodifiableMap(disjointCurrencyFormats);
            }
            return disjointCurrencyFormats;
        }finally {
            DISJOINT_CURRENCY_FORMATS_LOCK.unlock();
        }
    }

    private static void treatCurrencyFormat(NumberFormat currencyFormat, Currency currency) {
        currencyFormat.setCurrency(currency);
        DecimalFormat numberFormatCast = (DecimalFormat)currencyFormat;
        numberFormatCast.setPositivePrefix(numberFormatCast.getPositivePrefix().replaceAll(RE_WHITESPACE, " "));
        numberFormatCast.setPositiveSuffix(numberFormatCast.getPositiveSuffix().replaceAll(RE_WHITESPACE, " "));
        numberFormatCast.setNegativePrefix(numberFormatCast.getNegativePrefix().replaceAll(RE_WHITESPACE, " "));
        numberFormatCast.setNegativeSuffix(numberFormatCast.getNegativeSuffix().replaceAll(RE_WHITESPACE, " "));
        //handle the case of protected space which is not whitespace
        numberFormatCast.setPositivePrefix(numberFormatCast.getPositivePrefix().replace((char)160, ' '));
        numberFormatCast.setPositiveSuffix(numberFormatCast.getPositiveSuffix().replace((char)160, ' '));
        numberFormatCast.setNegativePrefix(numberFormatCast.getNegativePrefix().replace((char)160, ' '));
        numberFormatCast.setNegativeSuffix(numberFormatCast.getNegativeSuffix().replace((char)160, ' '));
    }

    private FormatUtils() {
    }
}
