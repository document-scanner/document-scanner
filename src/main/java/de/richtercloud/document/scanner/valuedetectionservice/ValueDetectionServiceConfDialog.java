/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.valuedetectionservice;

import de.richtercloud.message.handler.IssueHandler;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.*;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author richter
 */
public class ValueDetectionServiceConfDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final DefaultListModel<ValueDetectionServiceConf> availableListModel = new DefaultListModel<>();
    private final DefaultListModel<ValueDetectionServiceConf> selectedListModel = new DefaultListModel<>();
    private final IssueHandler issueHandler;
    /**
     * The values changed by dialog operations. {@code null} indicates that the
     * dialog has been canceled.
     */
    private List<ValueDetectionServiceConf> availableValueDetectionServiceConfs = new LinkedList<>();
    private List<ValueDetectionServiceConf> selectedValueDetectionServiceConfs = new LinkedList<>();
    private Map<Class<? extends ValueDetectionServiceConf>, String> valueDetectionServiceJARPaths = new HashMap<>();
    private final ListCellRenderer valueDetectionServiceListCellRenderer = new DefaultListCellRenderer() {
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            assert value != null;
            assert value instanceof ValueDetectionServiceConf;
            ValueDetectionServiceConf valueCast = (ValueDetectionServiceConf) value;
            return super.getListCellRendererComponent(list,
                    valueCast.getDescription(),
                    index,
                    isSelected,
                    cellHasFocus);
        }
    };
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private final JButton availableAddButton = new JButton();
    private final JList<ValueDetectionServiceConf> availableList = new JList<>();
    private final JLabel availableListLabel = new JLabel();
    private final JScrollPane availableListScrollPane = new JScrollPane();
    private final JPanel availablePanel = new JPanel();
    private final JButton availableRemoveButton = new JButton();
    private final JButton closeButton = new JButton();
    private final JButton deselectButton = new JButton();
    private final JButton saveButton = new JButton();
    private final JButton selectButton = new JButton();
    private final JList<ValueDetectionServiceConf> selectedList = new JList<>();
    private final JLabel selectedListLabel = new JLabel();
    private final JScrollPane selectedListScrollPane = new JScrollPane();
    private final JPanel selectedPanel = new JPanel();
    private final JSplitPane splitPane = new JSplitPane();
    // End of variables declaration//GEN-END:variables

    public ValueDetectionServiceConfDialog(Window parent,
            List<ValueDetectionServiceConf> availableValueDetectionServiceConfs,
            List<ValueDetectionServiceConf> selectedValueDetectionServiceConfs,
            Map<Class<? extends ValueDetectionServiceConf>, String> valueDetectionServiceJARPaths,
            IssueHandler issueHandler) {
        super(parent,
                ModalityType.APPLICATION_MODAL);
        this.issueHandler = issueHandler;
        initComponents();
        this.splitPane.setDividerLocation(splitPane.getPreferredSize().width/2);
        assert Collections.disjoint(availableValueDetectionServiceConfs,
                selectedValueDetectionServiceConfs);
        this.availableValueDetectionServiceConfs = availableValueDetectionServiceConfs;
        this.selectedValueDetectionServiceConfs = selectedValueDetectionServiceConfs;
        this.valueDetectionServiceJARPaths = valueDetectionServiceJARPaths;
        for(ValueDetectionServiceConf availableService : availableValueDetectionServiceConfs) {
            assert availableService != null;
            availableListModel.addElement(availableService);
        }
        for(ValueDetectionServiceConf selectedService : selectedValueDetectionServiceConfs) {
            assert selectedService != null;
            selectedListModel.addElement(selectedService);
        }
        this.availableList.setCellRenderer(valueDetectionServiceListCellRenderer);
        this.selectedList.setCellRenderer(valueDetectionServiceListCellRenderer);
        this.availableList.addListSelectionListener((listSelectionEvent) -> {
            this.selectButton.setEnabled(this.availableList.getSelectedIndex() != -1);
        });
        this.selectedList.addListSelectionListener((listSelectionEvent) -> {
            this.deselectButton.setEnabled(this.selectedList.getSelectedIndex() != -1);
        });
    }

    public List<ValueDetectionServiceConf> getAvailableValueDetectionServiceConfs() {
        return availableValueDetectionServiceConfs;
    }

    public List<ValueDetectionServiceConf> getSelectedValueDetectionServiceConfs() {
        return selectedValueDetectionServiceConfs;
    }

    public Map<Class<? extends ValueDetectionServiceConf>, String> getValueDetectionServiceJARPaths() {
        return valueDetectionServiceJARPaths;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        availableListLabel.setText("Available Auto OCR detection services:");

        availableList.setModel(availableListModel);
        availableListScrollPane.setViewportView(availableList);

        availableAddButton.setText("Add");
        availableAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                availableAddButtonActionPerformed(evt);
            }
        });

        selectButton.setText("Select");
        selectButton.setEnabled(false);
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                selectButtonActionPerformed(evt);
            }
        });

        availableRemoveButton.setText("Remove");
        availableRemoveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                availableRemoveButtonActionPerformed(evt);
            }
        });

        GroupLayout availablePanelLayout = new GroupLayout(availablePanel);
        availablePanel.setLayout(availablePanelLayout);
        availablePanelLayout.setHorizontalGroup(
            availablePanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(availablePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(availablePanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(availableListScrollPane)
                    .addGroup(availablePanelLayout.createSequentialGroup()
                        .addComponent(availableListLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(availablePanelLayout.createSequentialGroup()
                        .addComponent(availableRemoveButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(availableAddButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(selectButton)))
                .addContainerGap())
        );
        availablePanelLayout.setVerticalGroup(
            availablePanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(availablePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(availableListLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(availableListScrollPane, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(availablePanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(availableAddButton)
                    .addComponent(availableRemoveButton)
                    .addComponent(selectButton))
                .addContainerGap())
        );

        splitPane.setLeftComponent(availablePanel);

        selectedList.setModel(selectedListModel);
        selectedListScrollPane.setViewportView(selectedList);

        selectedListLabel.setText("Selected auto OCR value detection services:");

        deselectButton.setText("Deselect");
        deselectButton.setEnabled(false);
        deselectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                deselectButtonActionPerformed(evt);
            }
        });

        GroupLayout selectedPanelLayout = new GroupLayout(selectedPanel);
        selectedPanel.setLayout(selectedPanelLayout);
        selectedPanelLayout.setHorizontalGroup(
            selectedPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(selectedPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(selectedPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(selectedListScrollPane)
                    .addGroup(selectedPanelLayout.createSequentialGroup()
                        .addGroup(selectedPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(selectedListLabel)
                            .addComponent(deselectButton))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        selectedPanelLayout.setVerticalGroup(
            selectedPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(selectedPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectedListLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(selectedListScrollPane, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(deselectButton)
                .addContainerGap())
        );

        splitPane.setRightComponent(selectedPanel);

        closeButton.setText("Close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        saveButton.setText("Save");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(splitPane, GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(closeButton)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saveButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(splitPane)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(closeButton)
                    .addComponent(saveButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void availableAddButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_availableAddButtonActionPerformed
        ValueDetectionServiceAddDialog addDialog = new ValueDetectionServiceAddDialog(this,
                issueHandler);
        addDialog.setLocationRelativeTo(this);
        addDialog.setVisible(true);
        Pair<String, ValueDetectionServiceConf> createdConf = addDialog.getCreatedConf();
            //already validated in addDialog
        if(createdConf == null) {
            //dialog has been canceled
            return;
        }
        this.valueDetectionServiceJARPaths.put(createdConf.getValue().getClass(),
                createdConf.getKey());
        this.availableValueDetectionServiceConfs.add(createdConf.getValue());
        this.availableListModel.addElement(createdConf.getValue());
    }//GEN-LAST:event_availableAddButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void closeButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        this.availableValueDetectionServiceConfs = null;
        this.selectedValueDetectionServiceConfs = null;
        this.valueDetectionServiceJARPaths = null;
        this.setVisible(false);
    }//GEN-LAST:event_closeButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void selectButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_selectButtonActionPerformed
        ValueDetectionServiceConf serviceConf = this.availableList.getSelectedValue();
        this.availableListModel.removeElement(serviceConf);
        this.selectedListModel.addElement(serviceConf);
        this.availableValueDetectionServiceConfs.remove(serviceConf);
        this.selectedValueDetectionServiceConfs.add(serviceConf);
    }//GEN-LAST:event_selectButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void deselectButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_deselectButtonActionPerformed
        ValueDetectionServiceConf serviceConf = this.selectedList.getSelectedValue();
        this.selectedListModel.removeElement(serviceConf);
        this.availableListModel.addElement(serviceConf);
        this.selectedValueDetectionServiceConfs.remove(serviceConf);
        this.availableValueDetectionServiceConfs.add(serviceConf);
    }//GEN-LAST:event_deselectButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void saveButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_saveButtonActionPerformed

    @SuppressWarnings("PMD.UnusedFormalParameter")
    private void availableRemoveButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_availableRemoveButtonActionPerformed
        ValueDetectionServiceConf serviceConf = this.availableList.getSelectedValue();
        this.valueDetectionServiceJARPaths.remove(serviceConf.getClass());
        this.availableValueDetectionServiceConfs.remove(serviceConf);
        this.availableListModel.removeElement(serviceConf);
    }//GEN-LAST:event_availableRemoveButtonActionPerformed
}
