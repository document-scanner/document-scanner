/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.controller;

import de.richtercloud.document.scanner.gui.conf.DocumentScannerConf;
import de.richtercloud.document.scanner.valuedetectionservice.DelegatingValueDetectionServiceFactory;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionService;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceConf;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceCreationException;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceFactory;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorage;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetriever;
import de.richtercloud.reflection.form.builder.jpa.storage.PersistenceStorage;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author richter
 */
public class DefaultValueDetectionServiceController implements ValueDetectionServiceController {
    private final AmountMoneyCurrencyStorage amountMoneyAdditionalCurrencyStorage;
    private final AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever;
    private final PersistenceStorage<Long> storage;
    private final IssueHandler issueHandler;
    private final DocumentScannerConf documentScannerConf;
    private final Set<ValueDetectionService<?>> configuredValueDetectionServices = new HashSet<>();

    public DefaultValueDetectionServiceController(AmountMoneyCurrencyStorage amountMoneyAdditionalCurrencyStorage,
            AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever,
            PersistenceStorage<Long> storage,
            IssueHandler issueHandler,
            DocumentScannerConf documentScannerConf) throws ValueDetectionServiceCreationException {
        this.amountMoneyAdditionalCurrencyStorage = amountMoneyAdditionalCurrencyStorage;
        this.amountMoneyExchangeRateRetriever = amountMoneyExchangeRateRetriever;
        this.storage = storage;
        this.issueHandler = issueHandler;
        this.documentScannerConf = documentScannerConf;
        applyValueDetectionServiceSelection0();
    }

    private void applyValueDetectionServiceSelection0() throws ValueDetectionServiceCreationException {
        ValueDetectionServiceFactory valueDetectionServiceConfFactory = new DelegatingValueDetectionServiceFactory(amountMoneyAdditionalCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler,
                storage);
        for(ValueDetectionServiceConf serviceConf : documentScannerConf.getSelectedValueDetectionServiceConfs()) {
            ValueDetectionService<?> valueDetectionService = valueDetectionServiceConfFactory.createService(serviceConf);
            configuredValueDetectionServices.add(valueDetectionService);
        }
    }

    /**
     * Re-creates value detection services from {@code documentScannerConf}.
     */
    @Override
    public void applyValueDetectionServiceSelection() throws ValueDetectionServiceCreationException {
        applyValueDetectionServiceSelection0();
    }

    @Override
    public Set<ValueDetectionService<?>> getConfiguredValueDetectionServices() {
        return configuredValueDetectionServices;
    }
}
