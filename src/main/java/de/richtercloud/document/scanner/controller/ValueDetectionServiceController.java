/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.controller;

import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionService;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionServiceCreationException;
import java.util.Set;

/**
 *
 * @author richter
 */
public interface ValueDetectionServiceController {

    Set<ValueDetectionService<?>> getConfiguredValueDetectionServices();

    /**
     * Create new {@link ValueDetectionService}s after a change of available and
     * selected {@link ValueDetectionService}s in configuration.
     *
     * @throws ValueDetectionServiceCreationException if the creation of the service fails
     */
    void applyValueDetectionServiceSelection() throws ValueDetectionServiceCreationException;
}
