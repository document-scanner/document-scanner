/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.gui;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class FormatUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FormatUtilsTest.class);

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testInit() throws InterruptedException {
        FormatUtils.init();
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetAllDateRelatedFormats() {
        Set<DateFormat> result = FormatUtils.getAllDateRelatedFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetAllDateTimeFormats() {
        Set<DateFormat> result = FormatUtils.getAllDateTimeFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetAllDateFormats() {
        Set<DateFormat> result = FormatUtils.getAllDateFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetAllTimeFormats() {
        Set<DateFormat> result = FormatUtils.getAllTimeFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetDisjointDateRelatedFormats() {
        Map<DateFormat, Set<Locale>> result = FormatUtils.getDisjointDateRelatedFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetDisjointDateFormats() {
        Map<DateFormat, Set<Locale>> result = FormatUtils.getDisjointDateFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetDisjointTimeFormats() {
        Map<DateFormat, Set<Locale>> result = FormatUtils.getDisjointTimeFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetDisjointDateTimeFormats() {
        Map<DateFormat, Set<Locale>> result = FormatUtils.getDisjointDateTimeFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetAllCurrencyFormats() {
        List<Locale> availableLocales = Arrays.asList(Locale.getAvailableLocales());
        int expectedSize = availableLocales.size()* Currency.getAvailableCurrencies().size();
        LOGGER.debug(String.format("expected number of items to process: %d",
                expectedSize));
        Set<NumberFormat> result = FormatUtils.getAllCurrencyFormats();
        assertEquals(expectedSize,
                result.size());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testGetDisjointCurrencyFormats() {
        Map<NumberFormat, Set<Locale>> result = FormatUtils.getDisjointCurrencyFormats();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }
}
