/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.valuedetectionservice;

import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorage;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorageException;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetriever;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetrieverException;
import de.richtercloud.reflection.form.builder.components.money.MemoryAmountMoneyCurrencyStorage;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;
import org.jscience.economics.money.Currency;
import org.jscience.economics.money.Money;
import org.jscience.physics.amount.Amount;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class CurrencyFormatValueDetectionServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyFormatValueDetectionServiceTest.class);

    /**
     * Test of getMaxWords method, of class CurrencyFormatValueDetectionService.
     */
    @Test
    public void testGetMaxWords() {
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = mock(AmountMoneyCurrencyStorage.class);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        CurrencyFormatValueDetectionService instance = new CurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug("Is Java version at least 10: {}",
                SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_10));
        int expResult = SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_10)
                ? 3
                : 2; // OpenJDK 10 introduces new currency formats with 3 words, e.g. `n € NZ` for New Zealand Dollars
        int result = instance.getMaxWords();
        assertEquals("expected max. 2 words for the maximum of words in OpenJDK < 10 and max. 3 in " +
                        ">= OpenJDK 10 (has introduced more detailed currency codes)",
                expResult,
                result);
    }

    @Test
    public void testCheckResult() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String inputSub = "1,2 €";
        List<String> inputSplits = new LinkedList<>(Arrays.asList(inputSub));
        int i = 0;
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        CurrencyFormatValueDetectionService instance = new CurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        List<ValueDetectionResult<Amount<Money>>> results = instance.checkResult(inputSub, inputSplits, i);
        assertTrue(String.format("results should contain a value detection result for 1.2 €, but was %s",
                        results),
                results.contains(new ValueDetectionResult<>(inputSub, Amount.valueOf(1.2d, Currency.EUR))));
    }
}
