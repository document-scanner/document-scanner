/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.valuedetectionservice;

import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorage;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyCurrencyStorageException;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetriever;
import de.richtercloud.reflection.form.builder.components.money.AmountMoneyExchangeRateRetrieverException;
import de.richtercloud.reflection.form.builder.components.money.MemoryAmountMoneyCurrencyStorage;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import org.jscience.economics.money.Currency;
import org.jscience.economics.money.Money;
import org.jscience.physics.amount.Amount;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class TrieCurrencyFormatValueDetectionServiceTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(TrieCurrencyFormatValueDetectionServiceTest.class);
    private final static String ASSERTION_TEMPLATE = "result should contain a value detection result for 5 €, but was %s";

    @Test
    public void testFetchResults0WithoutSpace() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss fdjskl f jklfds fkd 5€ jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency symbol without space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format(ASSERTION_TEMPLATE,
                result),
                result.contains(new ValueDetectionResult<>("5€",
                        Amount.valueOf(5.0d, Currency.EUR)
                )));
    }

    @Test
    public void testFetchResults0CurrencySymbolWithSpace() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss fdjskl f jklfds fkd 5 € jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency symbol with space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format(ASSERTION_TEMPLATE,
                result),
                result.contains(new ValueDetectionResult<>("5 €",
                        Amount.valueOf(5.0d, Currency.EUR))));
    }

    @Test
    public void testFetchResults0CurrencyNameWithoutSpace() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss fdjskl f jklfds fkd 5EUR jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency name without space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format(ASSERTION_TEMPLATE,
                result),
                result.contains(new ValueDetectionResult<>("5EUR",
                        Amount.valueOf(5.0d, Currency.EUR)
                )));
    }

    @Test
    public void testFetchResults0CurrencyNameWithSpace() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss fdjskl f jklfds fkd 5 EUR jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency name with space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format(ASSERTION_TEMPLATE,
                result),
                result.contains(new ValueDetectionResult<>("5 EUR",
                        Amount.valueOf(5.0d, Currency.EUR)
                )));
    }

    @Test
    public void testFetchResults0CurrencyNameBeforeValueWithoutSpace() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss fdjskl f jklfds fkd EUR5 jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency name before value without space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format(ASSERTION_TEMPLATE,
                result),
                result.contains(new ValueDetectionResult<>("EUR5",
                        Amount.valueOf(5.0d, Currency.EUR)
                )));
    }

    @Test
    public void testFetchResults0CurrencyNameBeforeValueWithSpace() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss fdjskl f jklfds fkd EUR 5 jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency name before value with space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format(ASSERTION_TEMPLATE,
                result),
                result.contains(new ValueDetectionResult<>("EUR 5",
                        Amount.valueOf(5.0d, Currency.EUR)
                )));
    }

    @Test
    public void testFetchResults0TwoOccurances() throws AmountMoneyCurrencyStorageException,
            AmountMoneyExchangeRateRetrieverException,
            ResultFetchingException {
        String input = "jfklds jklfd jklds jkldfs fjkdls jkdflss 4€ fdjskl f jklfds fkd EUR 5 jkfdls fkldfsjklf  fdjklf sjklfds f jkldslskd ";
        AmountMoneyCurrencyStorage amountMoneyCurrencyStorage = new MemoryAmountMoneyCurrencyStorage();
        amountMoneyCurrencyStorage.saveCurrency(Currency.EUR);
        AmountMoneyExchangeRateRetriever amountMoneyExchangeRateRetriever = mock(AmountMoneyExchangeRateRetriever.class);
        IssueHandler issueHandler = mock(IssueHandler.class);
        when(amountMoneyExchangeRateRetriever.getSupportedCurrencies()).thenReturn(new HashSet<>(Arrays.asList(Currency.EUR)));
        TrieCurrencyFormatValueDetectionService instance = new TrieCurrencyFormatValueDetectionService(amountMoneyCurrencyStorage,
                amountMoneyExchangeRateRetriever,
                issueHandler);
        LOGGER.debug(String.format("running with currency name before value with space with input '%s'", input));
        LinkedHashSet<ValueDetectionResult<Amount<Money>>> result = instance.fetchResults0(input,
                null //languageIdentifier
        );
        assertTrue(String.format("result should contain a value detection result for 5 € and 4 €, but was %s",
                result),
                result.contains(new ValueDetectionResult<>("EUR 5",
                                Amount.valueOf(5.0d, Currency.EUR)))
                        && result.contains(new ValueDetectionResult<>("4€",
                                Amount.valueOf(4.0d, Currency.EUR))));
    }
}
