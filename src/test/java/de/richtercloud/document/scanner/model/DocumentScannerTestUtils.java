/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.model;

import de.richtercloud.test.tools.TestRandomUtils;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author richter
 */
public final class DocumentScannerTestUtils {

    /**
     * Creates a contact with valid random properties.
     * @return the created contact
     */
    public static Company createContact() {
        List<String> allNames = new LinkedList<>();
        int nameCount = TestRandomUtils.getInsecureTestRandom().nextInt(5);
        for(int i=0; i<nameCount; i++) {
            allNames.add("name"+TestRandomUtils.getInsecureTestRandom().nextInt());
        }
        List<Address> addresses = new LinkedList<>();
        int addressCount = TestRandomUtils.getInsecureTestRandom().nextInt(3);
        for(int i=0; i<addressCount; i++) {
            addresses.add(new Address("street"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                    "number"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                    "postofficebox"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                    "zipcode"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                    "region"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                    "city"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                    "country"+TestRandomUtils.getInsecureTestRandom().nextInt()));
        }
        List<EmailAddress> emails = new LinkedList<>();
        int emailCount = TestRandomUtils.getInsecureTestRandom().nextInt(3);
        for(int i=0; i<emailCount; i++) {
            emails.add(new EmailAddress(TestRandomUtils.getInsecureTestRandom().nextInt()+"@bla.com",
                    new LinkedList<>(Arrays.asList(String.valueOf(TestRandomUtils.getInsecureTestRandom().nextInt())))));
        }
        List<TelephoneNumber> telephoneNumbers = new LinkedList<>();
        int telephoneNumberCount = TestRandomUtils.getInsecureTestRandom().nextInt(3);
        for(int i=0; i<telephoneNumberCount; i++) {
            telephoneNumbers.add(new TelephoneNumber(TestRandomUtils.getInsecureTestRandom().nextInt(100),
                    TestRandomUtils.getInsecureTestRandom().nextInt(999),
                    TestRandomUtils.getInsecureTestRandom().nextInt(10000),
                    null,
                    TelephoneNumber.TYPE_LANDLINE));
        }
        Company retValue = new Company("name"+TestRandomUtils.getInsecureTestRandom().nextInt(),
                allNames,
                addresses, emails, telephoneNumbers);
        return retValue;
    }

    private DocumentScannerTestUtils() {
    }
}
